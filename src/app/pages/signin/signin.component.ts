import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { ScdFileService } from 'src/app/services/scdfile.service';
import { FormControl, FormGroup } from '@angular/forms';
import { MessageService } from "src/app/services/message.service";
import { Subscription } from "rxjs";
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SigninComponent {
  subscription: Subscription;

  profileForm = new FormGroup({
    username: new FormControl('testdemouser'),
    password: new FormControl('12345678'),
  });
  constructor(private toastr: ToastrService, public MS: MessageService, private router: Router, private loadingBar: LoadingBarService, private gService: ScdFileService) {

  }

  signin_clickHandler() {

    let username = this.profileForm.value.username;
    let password = this.profileForm.value.password;

    if (username != "" || password != "") {
      this.MS.basicAuthToken.BASIC_AUTH_TOKEN = "Basic " + btoa(username + ":" + password);

      let credUserPass = { Username: username, Password: password };
      this.gService.signInVersions(credUserPass).subscribe((data: any) => {
        this.MS.signIn_user.userName = username;
        
        this.uploadOfflineData();
        this.gService.getUserSettings();
        
        this.toastr.success('You are logged in!', "Success!");
        this.router.navigate(['open-project']);
      },
        (error: HttpErrorResponse) => {
          this.toastr.warning('Username and password may not working!', "Authrization Failure!");
        });
    } else {
      this.toastr.error('Username & Password required', "");
    }

  }

  uploadOfflineData() {
    for (var i = 0; i < localStorage.length; i++) {
      const splitProjectInfo = localStorage.key(i).split("_");
      if(splitProjectInfo[0] == "projectSavedInfo" && splitProjectInfo[1] !="") {
          const orderDetails = JSON.parse(localStorage.getItem("projectSavedInfo_"+ splitProjectInfo[1]));
          this.gService.addBasicProjectData(orderDetails).subscribe((res) => {
            this.toastr.success('Offline data saved successfully!', "Success!");
            localStorage.removeItem("projectSavedInfo_" + splitProjectInfo[1]);
          },
          (error: HttpErrorResponse) => {
            this.toastr.warning('Offline data error!', "Warning!");
          });
        }
      }
    
  }

}
