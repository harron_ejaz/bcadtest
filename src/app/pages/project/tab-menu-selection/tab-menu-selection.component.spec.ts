import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabMenuSelectionComponent } from './tab-menu-selection.component';

describe('TabMenuSelectionComponent', () => {
  let component: TabMenuSelectionComponent;
  let fixture: ComponentFixture<TabMenuSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabMenuSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabMenuSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
