import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
  selector: "app-tab-menu-selection",
  templateUrl: "./tab-menu-selection.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ["./tab-menu-selection.component.scss"],
})
export class TabMenuSelectionComponent {
  tabs: any[] = [{ title: "Measure & Align" }];
  constructor() {}

  addNewTab(): void {
    const newTabIndex = this.tabs.length + 1;
    this.tabs.push({
      title: `Dynamic Title ${newTabIndex}`,
      content: `Dynamic content ${newTabIndex}`,
      disabled: false,
      removable: true,
    });
  }

  removeTabHandler(tab: any): void {
    this.tabs.splice(this.tabs.indexOf(tab), 1);
    console.log("Remove Tab handler");
  }
  
}
