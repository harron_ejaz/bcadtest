import { Component, ViewEncapsulation } from "@angular/core";
import * as turf from '@turf/turf';
import { faSortDown } from "@fortawesome/free-solid-svg-icons";
import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from "src/app/services/message.service";
import { AccordionConfig } from "ngx-bootstrap/accordion";
import {
  trigger,
  transition,
  state,
  animate,
  style,
} from "@angular/animations";
import { FunctionService } from 'src/app/services/function.service';
import { Vector3, Group, WebGLRenderer, Mesh, MeshBasicMaterial, TextGeometry, FontLoader } from 'three';
import { Subscription } from 'rxjs';


export function getAccordionConfig(): AccordionConfig {
  return Object.assign(new AccordionConfig(), { closeOthers: false });
}

@Component({
  selector: "app-area-editor",
  templateUrl: "./area-editor.component.html",
  styleUrls: ["./area-editor.component.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger("flyInOut", [
      state("in", style({ transform: "translateX(0)" })),
      transition("void => *", [
        style({ transform: "translateX(-100%)" }),
        animate(500),
      ]),
      transition("* => void", [
        animate(500, style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
  providers: [{ provide: AccordionConfig, useFactory: getAccordionConfig }],
})
export class AreaEditorComponent {

  faSortDown = faSortDown;
  subscription: Subscription;
  allHigh: boolean = false;
  polygonData = new Array();
  alphabetsArray = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
  alphabetIndex : number = 0;
  polygonInAccordionDrop : boolean = false;
  private font:any;

  constructor( public engServ: EngineService, public MS: MessageService, private FS: FunctionService ) {

    this.subscription = this.MS.getMessage().subscribe((message) => {
      if( message == "DRAG_DROP_POLYGON" ) {
        this.polygon_dropHandler();
      }
      //input line length changes in measure and align page, so re-calculate area of all polygon if they are already drawn
      else if( message == "INPUT_LENGTH_CHANGE" ) {
        this.reCalculatePolygonArea();
      }
      else if( message == "SHOW_POLYGON_SVG" ) {
        let scope = this;
        setTimeout(() => {
          scope.showPolygonSVG();
        }, 500);
      }
    });

    this.MS.area_editor.POLYGON_DATA = this.polygonData;
    document.getElementById("renderCanvas").style.cursor = "default";
	this.loadFont();
  }

  polygonDropArea_moveHandler(e: MouseEvent) {
    if (this.MS.area_editor.DRAGGING_POLYGON) {
      document.body.style.cursor = "pointer";
    }
  }

  mouseup(e: MouseEvent) {
    if (this.MS.area_editor.DRAGGING_POLYGON)
      this.MS.area_editor.DRAGGING_POLYGON = false;
  }

  polygon_moveHandler(e: MouseEvent) {
    if( this.MS.area_editor.DRAGGING_POLYGON ) {
      document.body.style.cursor = "pointer";
    }
  }



  polygonInAccordion_dropHandler(index:number) {

    this.polygonInAccordionDrop = true;
    //if polygon is dragging and polygons are dropped in accordion
    if( this.MS.area_editor.DRAGGING_POLYGON && this.polygonInAccordionDrop == true ) {

      //Array of polygon's ids
      let polygonID:any = new Array();
      polygonID = this.MS.area_editor.POLYGON_ID;

      let data_object:any;

      let alphabet:string = this.alphabetsArray[ index ];

      for( let i:number=0; i < polygonID.length; i++ ) {

        let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", polygonID[i] );

        let polygonSVG:any = this.convert_polygon_to_svg( polygon.userData.shape );
        let polygonArea:number = this.getPolygonArea( polygon.userData.shape );

        data_object = new Object();
        data_object.svg = polygonSVG;
        data_object.area = polygonArea; // area with rise 0 or slope factor 1
        data_object.selectedSVG = false;
        data_object.slopeArea = null;
        data_object.id = polygonID[i];
        data_object.name = "Polygon";
        data_object.addArea = true; // add or subtract

        this.polygonData[ index ].shape.push( data_object );
        
        //show alphabet inside polygin in drawing canvas
        this.showAlphabetInPolygon( polygonID[i], alphabet );

        this.showPolygonHolesInAccordion( polygon, alphabet, index );

      }

      this.MS.area_editor.DRAGGING_POLYGON = false;
      document.body.style.cursor = "default";

      let scope = this;
      setTimeout(function() { scope.showPolygonSVG(); }, 200);  

    }

  }

  resequence_clickHandler() {
    let selectedBuildingGroup:number = this.MS.menu_line_editor.BUILDING_GROUP_SELECTED;
    let alphabetCounter = 0;
    for (let i = 0; i < this.polygonData.length; i++) {
      let matchSelectedGroup  = this.polygonData[ i ].selectedBuildingGroup;
      if(matchSelectedGroup == selectedBuildingGroup) {
        let alphabet:string = this.alphabetsArray[ alphabetCounter ];
        this.polygonData[ i ].alphabet = alphabet; 
        alphabetCounter++;
      } 
    } 
    this.MS.menu_line_editor.STRUCTURE_COUNT[this.MS.menu_line_editor.BUILDING_GROUP_SELECTED] = alphabetCounter; 
  }

  showPolygonHolesInAccordion( polygon:any, alphabet:string, index:number ) {

    //array of hole ids
    let polygonID:any = polygon.userData.holeID;

    if( polygonID == undefined ) return;

    if( polygonID.length > 0 ) {

      let data_object:any;

      for( let i:number=0; i < polygonID.length; i++ ) {

        let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", polygonID[i] );

        let polygonSVG:any = this.convert_polygon_to_svg( polygon.userData.shape );
        let polygonArea:number = this.getPolygonArea( polygon.userData.shape );

        data_object = new Object();
        data_object.svg = polygonSVG;
        data_object.area = polygonArea * 1; // area with rise 0 or slope factor 1
        data_object.selectedSVG = false;
        data_object.slopeArea = null;
        data_object.id = polygonID[i];
        data_object.name = "Polygon";
        data_object.addArea = false; // add or subtract

        this.polygonData[ index ].shape.push( data_object );
        
        //show alphabet inside polygin in drawing canvas
        this.showAlphabetInPolygon( polygonID[i], alphabet );

      }

    }

  }

  polygon_dropHandler() {

    //if polygon is dragging and polygons are not dropped in accordion
    if( this.MS.area_editor.DRAGGING_POLYGON && this.polygonInAccordionDrop == false ) {

      //Array of polygon's ids
      let polygonID:any = new Array();
      polygonID = this.MS.area_editor.POLYGON_ID;

      // Alphabets by Structure
      let structureGroupCount:number;
      let structureGroup : string = "STR_" + this.MS.menu_line_editor.BUILDING_GROUP_SELECTED;
        structureGroupCount = this.MS.menu_line_editor.STRUCTURE_COUNT[structureGroup];
        if (  typeof structureGroupCount != "undefined") {
          this.MS.menu_line_editor.STRUCTURE_COUNT[structureGroup] = (structureGroupCount+1);
        } else {
          this.MS.menu_line_editor.STRUCTURE_COUNT[structureGroup] = 1;
          structureGroupCount = 0;
        }
    

      this.flyPolygon( polygonID );

      let data_object:any;
      let data_array = new Array();

      let rise:number = 0;
      let iwb:number = 0;

      let alphabet:string = this.alphabetsArray[ structureGroupCount ];
      
      for( let i:number=0; i < polygonID.length; i++ ) {

        let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", polygonID[i] );

        let polygonSVG:any = this.convert_polygon_to_svg( polygon.userData.shape );
        let polygonArea:number = this.getPolygonArea( polygon.userData.shape );

        data_object = new Object();
        data_object.svg = polygonSVG;
        data_object.area = polygonArea; // area with rise 0 or slope factor 1
        data_object.selectedSVG = false;
        data_object.slopeArea = null;
        data_object.id = polygonID[i];
        data_object.name = "Polygon";
        data_object.addArea = true; // add or subtract

        data_array.push( data_object );

        //show alphabet inside polygon in drawing canvas
        this.showAlphabetInPolygon( polygonID[i], alphabet );

        data_array = this.showPolygonHoles( polygon, data_array, alphabet );

      }

      let buildingsGroup:any = this.engServ.getSelectedBuildingsGroup();
      
      this.polygonData.push({
        alphabet: this.alphabetsArray[ structureGroupCount ],
        rise: rise,
        iwb: iwb,
        totalSlopeArea: 0,
        shape: data_array,
        buildingsGroupUUID: buildingsGroup.uuid,
        isOpen: true,
        selectedBuildingGroup : this.MS.menu_line_editor.BUILDING_GROUP_SELECTED,
      });
      
      this.MS.area_editor.DRAGGING_POLYGON = false;
      document.body.style.cursor = "default";

      let scope = this;
      setTimeout(function() { scope.showPolygonSVG(); }, 200);  
        
    }

    this.polygonInAccordionDrop = false;

  }

  flyPolygon( POLYGON_ID:any ) {

    let flyingAreasGroup:any = new Group();

    for( let i:number=0; i < POLYGON_ID.length; i++ ) {
      
      let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", POLYGON_ID[i] );

      flyingAreasGroup.add( polygon.clone() );
    }

    flyingAreasGroup.name = "FlyingAreasGroup";
    this.engServ.commonGroup.add(flyingAreasGroup);

    let count = 0;
    let scope = this;

    let renderer:WebGLRenderer = this.engServ.renderer;
    
    renderer.setAnimationLoop( function () {
      if ( count < 50) {
        flyingAreasGroup.position.x -= 10;
        count++;
      } 
      else if ( count == 50 ) {
        scope.engServ.commonGroup.remove( flyingAreasGroup );
        count = 0;
        renderer.setAnimationLoop(null);
      }
    } );
  }
  

  showPolygonHoles( polygon:any, data_array:any, alphabet:string ) {

    //array of hole ids
    let polygonID:any = polygon.userData.holeID;

    if( polygonID == undefined ) return data_array;

    if( polygonID.length > 0 ) {

      let data_object:any;
      
      for( let i:number=0; i < polygonID.length; i++ ) {

        let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", polygonID[i] );

        let polygonSVG:any = this.convert_polygon_to_svg( polygon.userData.shape );
        let polygonArea:number = this.getPolygonArea( polygon.userData.shape );

        data_object = new Object();
        data_object.svg = polygonSVG;
        data_object.area = polygonArea * 1; // area with rise 0 or slope factor 1
        data_object.selectedSVG = false;
        data_object.slopeArea = null;
        data_object.id = polygonID[i];
        data_object.addArea = false; // add or subtract

        data_array.push( data_object );

        //show alphabet inside polygin in drawing canvas
        this.showAlphabetInPolygon( polygonID[i], alphabet );

      }

    }
    
    return data_array;
    
  }

  addArea(poloygon_index, shape_index, shape_area) {

    this.polygonData[ poloygon_index ].shape[ shape_index ].addArea = true;
    this.reCalculatePolygonArea();
    
  }

  subtractArea(poloygon_index, shape_index, shape_area) {

    this.polygonData[ poloygon_index ].shape[ shape_index ].addArea = false;
    this.reCalculatePolygonArea();

  }


  // show Alphabet In Polygon
  private showAlphabetInPolygon( polygonID:number, alphabet:string ){

    let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", polygonID );

    let pointsArray = new Array();
    let vertex:any = polygon.geometry.vertices;
    for( let i = 0; i < vertex.length; i++ ) {
      let x:number = vertex[i].x;
      let y:number = vertex[i].y;
      pointsArray.push( [x, y] );
    }
    pointsArray.push( [ pointsArray[0][0], pointsArray[0][1] ] );

    let turnPolygon = turf.polygon( [ pointsArray ] );
    let center = turf.centerOfMass(turnPolygon);

    let x = center.geometry.coordinates[0];
    let y = center.geometry.coordinates[1];
    let relativeCenter:Vector3 = this.engServ.get2DPosition(x, y);

    let position:Vector3 = new Vector3( x, y, 0 );

    let geometry:TextGeometry = new TextGeometry( alphabet, {
        font: this.font,
        size: 50
    } );

    geometry.computeBoundingBox();

    let lineType:any = this.FS.get_line_type_selected();

    let material:MeshBasicMaterial = new MeshBasicMaterial( { color : lineType.COLOR } );

    let text:Mesh = new Mesh( geometry, material );

    text.name = "PolygonAlphabet";
    text.renderOrder = 2;

    text.position.copy( position );
    
    //polygon uuid is same as its dimensions id,
    //so while modify this polygon, only this polygon's dimensions are updated
    text.uuid = "PolygonAlphabet-" + polygon.uuid;

    this.engServ.addToScene( text );

  }

  loadFont() {
    let scope = this;
    const loader = new FontLoader();
    loader.load( 'assets/fonts/helvetiker_regular.typeface.json', function ( font ) {
        scope.font = font;
    } );
  }

  

  private showPolygonSVG() {
    
    for( let i:number = 0; i < this.polygonData.length; i++ ) {

      if( this.polygonData[i].buildingsGroupUUID == this.engServ.getSelectedBuildingsGroup().uuid ) {

        let alphabet:string = this.polygonData[i].alphabet;
        let rise:number = this.polygonData[i].rise;
        let iwb:number = this.polygonData[i].iwb;
        let shape:any = this.polygonData[i].shape //array
        let totalSlopeArea:number = 0;
        let slopeArea:number;

        //if all svg in an accordions are deleted, then refresh total area to nill
        if( shape.length == 0 ) {
          let totalAreaID:string = 'totalArea-' + alphabet;
          document.getElementById( totalAreaID ).innerHTML = '';
        }

        for( let j:number = 0; j < shape.length; j++ ) {

          //area is undefined if there is an emptly accordion in left plane
          if( shape[j].area != undefined ) {

           
            let svgID:string = 'svg-' + alphabet + j;

            try{

              let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", shape[j].id );
              let iwbArea:number = this.getPolygonEaveArea( polygon.userData.shape, iwb );

              let polygonIWBArea:number = shape[j].area + iwbArea;

              slopeArea = this.FS.get_length_or_area_with_slope_factor( polygonIWBArea ,rise );
              slopeArea = parseFloat( slopeArea.toFixed(2) );
              shape[j].slopeArea = slopeArea;
              //total area of all svg polygons for each row (accordion)
              if( shape[j].addArea )
                totalSlopeArea = totalSlopeArea + slopeArea;
              else  
                totalSlopeArea = totalSlopeArea - slopeArea;  

              document.getElementById( svgID ).innerHTML = shape[j].svg;

              //when last shape, then show the total area
              if( j == shape.length-1 ) {
                this.polygonData[i].totalSlopeArea = totalSlopeArea.toFixed(2);                
              }

            }
            catch(e){
              console.log("Error showing svg:" + e);
            }
            
          }

        }
      }

            

    }

  }


  //re calculate all polygons area and slope area in left plane, after changing input scale length
  reCalculatePolygonArea() {
   

    for( let i:number = 0; i < this.polygonData.length; i++ ) {

      
      let rise:number = this.polygonData[i].rise;
      let iwb:number = this.polygonData[i].iwb;
      let shape:any = this.polygonData[i].shape //array
      let totalSlopeArea:number = 0;
      let slopeArea:number;

      for( let j:number = 0; j < shape.length; j++ ) {

        let polygonID = shape[j].id;
        let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", polygonID ).userData.shape;
        let polygonArea:number = this.getPolygonArea( polygon );

        shape[j].area = polygonArea;

        slopeArea = this.FS.get_length_or_area_with_slope_factor( shape[j].area, rise );
        slopeArea = parseFloat( slopeArea.toFixed(2) );
        shape[j].slopeArea = slopeArea;
        //total area of all svg polygons for each row (accordion)
        if( shape[j].addArea )
          totalSlopeArea = totalSlopeArea + slopeArea;
        else  
          totalSlopeArea = totalSlopeArea - slopeArea;  
        this.polygonData[i].totalSlopeArea = totalSlopeArea.toFixed(2);
        
      }
      
    }

  }


  pitch_changeHandler( index:number, rise:number ) {
    this.polygonData[ index ].rise = rise;
    this.showPolygonSVG();
  }

  iwb_changeHandler(index:number, iwb:number ) {
    this.polygonData[index].iwb = iwb; 
    this.showPolygonSVG();
  }

  pitchSelectedRows_clickHandler(rise:number) {
  
    for( let i:number = 0; i < this.polygonData.length; i++ ) {
      if(this.polygonData[i].selected == true) {
       this.polygonData[ i ].rise = rise;
      }
    }

    this.showPolygonSVG();
  }  


  iwbSelectedRows_clickHandler(iwb:number) {
  
    for( let i:number = 0; i < this.polygonData.length; i++ ) {
      if(this.polygonData[i].selected == true) {
       this.polygonData[ i ].iwb = iwb;
      }
    }

    this.showPolygonSVG();
  }  

  

  expandAll_clickHandler() {
    for (let i = 0; i < this.polygonData.length; i++) {
      this.polygonData[i].isOpen = true;
    }
  }

  collapseAll_clickHandler () {
    for (let i = 0; i < this.polygonData.length; i++) {
      this.polygonData[i].isOpen = false;
    }
  }

  add_clickHandler() {
    
    let rise:number = 0;
    let iwb:number = 0;

    this.polygonData.push({
      alphabet: this.alphabetsArray[ this.alphabetIndex++ ],
      rise: rise,
      iwb: iwb,
      totalSlopeArea: null,
      shape: [],
      buildingsGroupUUID: this.engServ.getSelectedBuildingsGroup().uuid,
      isOpen: true
    });

 }

 deleteSelectedRows_clickHandler() {

  //delete accordions 
  for( let i:number = 0; i < this.polygonData.length; i++ ) {

    if( this.polygonData[i].selected == true ) {
      this.polygonData = this.FS.remove_from_array( this.polygonData, this.polygonData[i] );
      i--;
    }
    //delete svg(inner rows)
    else {
      for( let j:number = 0; j < this.polygonData[i].shape.length; j++ ) {
        
        if( this.polygonData[i].shape[j].selectedSVG == true ) {

          this.polygonData[i].shape = this.FS.remove_from_array_by_index( this.polygonData[i].shape, j );
          j--;

        }

      }
      this.showPolygonSVG();
    }
  }

 }
 
 
 
 convert_polygon_to_svg( polygonShape:any ) { 

  var svg: string = '<polyline points="';
  var offset: number = this.get_smallest_negative_number(polygonShape);

  let curves:any = polygonShape.curves;

  let xMin:number; 
  let xMax:number;  
  let yMin:number;
  let yMax:number;

  for( let i = 0; i < curves.length; i++ ) {

    let x:number = curves[i].v1.x + offset;
    let y:number = ( curves[i].v1.y * -1 ) + offset;

    if( i == 0 ) {
      xMin=x;
      xMax=x;
      yMin=y;
      yMax=y;
    }
    else {

      if(x < xMin) xMin = x;
      if(x > xMax) xMax = x;
      if(y < yMin) yMin = y;
      if(y > yMax) yMax = y;

    } 

    svg = svg + x + "," + y + " ";

  }

  var svgViewBox: string = '<svg viewBox="'+xMin+' '+yMin+' '+xMax+' '+yMax+'" width="50" height="50" preserveAspectRatio="xMidYMid meet" x="'+xMin+'" y="'+yMin+'">';
  svg = svgViewBox + svg;
  svg = svg + ( curves[0].v1.x + offset ) + "," + ( ( curves[0].v1.y * -1 ) + offset );
  svg = svg + '" style="fill:black;stroke:black;stroke-width:10" />';

  var holes:any = polygonShape.holes;
  if( holes.length > 0 ) {

    svg = svg + '<polyline points="';

    for( let i = 0; i < holes.length; i++ ) {

      curves = holes[i].curves;

      for( let j = 0; j < curves.length; j++ ) {
        let x:number = curves[j].v1.x + offset;
        let y:number = ( curves[j].v1.y * -1 ) + offset;
        svg = svg + x + "," + y + " ";
      }

      svg = svg + ( curves[0].v1.x + offset ) + "," + ( ( curves[0].v1.y * -1 ) + offset );
      svg = svg + '" style="fill:none;stroke:rgb(20, 20, 20);stroke-width:1" />';

    }

  }

  svg = svg + 'Sorry, your browser does not support inline SVG.</svg>';
  return svg;

}

  get_smallest_negative_number( polygonShape:any):number {
    var curves:any = polygonShape.curves;
    var smallest:number = 0;
    for( let i = 0; i < curves.length; i++ ) {
      let x:number = curves[i].v1.x;
      let y:number = curves[i].v1.y * -1;
      if(x < smallest) smallest = x;
      if(y < smallest) smallest = y;
    }
    return Math.abs( smallest );
  }

  private getPolygonArea( polygonShape:any ):number {
    let area:number = null;
    let polygon:any = this.getPolygon(polygonShape);
    let pathArea:number = this.getPolygonAreaFromCoordArray( polygon.path );
    area = pathArea;
    //if hole exists
    if( polygon.hole.length > 0 ) {
      let holeArea:number = this.getPolygonAreaFromCoordArray( polygon.hole );
      area = pathArea - holeArea;
    }

    return area;
  }

  //eaveLengths = polygon.eaveLength
  private getPolygonEaveArea( polygonShape:any, iwb:number ):number {
    if( iwb == 0 ) return 0;

    let polygon:any = this.getPolygon(polygonShape);
    let eaveArea:number = 0;
    for(let i=0;i<polygon.eaveLength.length;i++) {
      let eaveLength:number = polygon.eaveLength[i];
      eaveArea = eaveArea + ( eaveLength * iwb );
    }
    return eaveArea;
  }



  //https://stackoverflow.com/questions/24793288/calculating-the-area-of-an-irregular-polygon-using-javascript
    //If you're just using Simpson's rule to calculate area, 
    //the following function will do the job. 
    //Just make sure the polygon is closed. 
    //If not, just repeat the first coordinate pair at the end.
    //This function uses a single array of values, 
    //assuming they are in pairs (even indexes are x, odd are y). 
    //It can be converted to using an array of arrays containing coordinate pairs.
    //The function doesn't do any out of bounds or other tests on the input values.
    private getPolygonAreaFromCoordArray(coordArray:any):number {
      var x = coordArray;
      var a = 0;
      // Must have even number of elements
      if (x.length % 2) return;
      // Process pairs, increment by 2 and stop at length - 2
      for (var i=0, iLen=x.length-2; i<iLen; i+=2) {
         a += x[i]*x[i+3] - x[i+2]*x[i+1]; 
      }
      return Math.abs(a/2);
  }

  private getPolygon( polygonShape:any ) {

    const inputScaleRatio:number = this.FS.get_input_scale_ratio( this.MS.input_scaleLength );

    let polygonObject = { path:null, hole:[], eaveLength:[] }

    let polygonArray:any = new Array();
    let eaveLength:any = new Array();

    //get polygon path
    let curves:any = polygonShape.curves;

    for( let i = 0; i < curves.length; i++ ) {

      let x1:number = curves[i].v1.x;
      let y1:number = curves[i].v1.y;

      x1 = x1 / inputScaleRatio;
      y1 = y1 / inputScaleRatio;

      polygonArray.push( x1 );
      polygonArray.push( y1 );

      //if line is eave, then length is easured for line, to get multiplied by iwb value
      if( curves[i].properties.LineTypesRef == 1 ) {
        let x2:number = curves[i].v2.x;
        let y2:number = curves[i].v2.y;
  
        x2 = x2 / inputScaleRatio;
        y2 = y2/ inputScaleRatio;
  
        let lineLength:number = this.FS.get_distance_between_points( x1, y1, x2, y2 );
        eaveLength.push(lineLength);
      }
      

    }
    //push first point at end of array for closed polygon
    polygonArray.push( polygonArray[0] );
    polygonArray.push( polygonArray[1] );

    polygonObject.path = polygonArray;

    polygonObject.eaveLength = eaveLength;

    //if hole exists
    if( polygonShape.holes.length > 0 ) {

      polygonArray = new Array();
      //get polygon hole path
      curves = polygonShape.holes[0].curves;

      for( let i = 0; i < curves.length; i++ ) {

        let x1:number = curves[i].v1.x;
        let y1:number = curves[i].v1.y;

        x1 = x1 / inputScaleRatio;
        y1 = y1 / inputScaleRatio;

        polygonArray.push( x1 );
        polygonArray.push( y1 );

        //if line is eave, then length is easured for line, to get multiplied by iwb value
        if( curves[i].properties.LineTypesRef == 1 ) {
          let x2:number = curves[i].v2.x;
          let y2:number = curves[i].v2.y;
    
          x2 = x2 / inputScaleRatio;
          y2 = y2/ inputScaleRatio;
    
          let lineLength:number = this.FS.get_distance_between_points( x1, y1, x2, y2 );
          eaveLength.push(lineLength);
        }

      }
      //push first point at end of array for closed polygon
      polygonArray.push( polygonArray[0] );
      polygonArray.push( polygonArray[1] );

      polygonObject.hole = polygonArray;

      polygonObject.eaveLength = eaveLength;
      
    }

    return polygonObject;
    
  }
  
  openAccordion ( index:number ) {
    this.polygonData[index].isOpen = !this.polygonData[index].isOpen;
  }

  text_inputHandler( value:any ) {
    if(value != "") {
      if(this.MS.area_editor.LAST_SELECTED_LABEL_SHAPE == 'LABEL') {
        this.polygonData[this.MS.area_editor.LAST_SELECT_LABEL_INDEX].alphabet = value;
      } else if (this.MS.area_editor.LAST_SELECTED_LABEL_SHAPE == 'SHAPE') {
        
      }
    }
  }

  selectUnSelect (index) {
    
    this.MS.area_editor.LAST_SELECTED_LABEL_SHAPE = 'LABEL';
    this.MS.area_editor.LAST_SELECT_LABEL_INDEX = index;

    if(typeof this.polygonData[index].selected === 'undefined') {
      this.polygonData[index].selected = true;
    } else if( this.polygonData[index].selected == false ) {
      this.polygonData[index].selected = true;
    } else {
      this.polygonData[index].selected = false;
    }
  }
  
  selectUnSelectInner (parentIndex, index) {

    let polygonID = this.polygonData[parentIndex].shape[index].id;

    this.MS.area_editor.LAST_SELECTED_LABEL_SHAPE = 'SHAPE';
    this.MS.area_editor.LAST_SELECT_SHAPE_PARENT_INDEX = parentIndex;
    this.MS.area_editor.LAST_SELECT_SHAPE_INDEX = index;

    if( this.polygonData[parentIndex].shape[index].selectedSVG == false ) {
      this.polygonData[parentIndex].shape[index].selectedSVG = true;
      this.MS.sendMessage( "AREA_EDITOR_SELECT_SVG:" + polygonID );
    }
    else {
      this.polygonData[parentIndex].shape[index].selectedSVG = false;  
      this.MS.sendMessage( "AREA_EDITOR_UNSELECT_SVG:" + polygonID );
    }

  }

  swap(input, index_A, index_B) {
    let temp = input[index_A];
    input[index_A] = input[index_B];
    input[index_B] = temp;
  }

  insertAndShift(arr, from, to) {
    let cutOut = arr.splice(from, 1)[0]; // cut the element at index 'from'
    arr.splice(to, 0, cutOut);            // insert it at index 'to'
  }

  moveUp_clickHandler() {

    for (let i = 0; i < this.polygonData.length; i++) {
      let polygon = this.polygonData[i];
      if (polygon.selected == true) {
        if (i > 0) {
          this.insertAndShift(this.polygonData, i, (i - 1));
          break;
        }
      }
    }

  }

  moveDown_clickHandler() {

    for (let i = 0; i < this.polygonData.length; i++) {
      let polygon = this.polygonData[i];
      if (polygon.selected == true) {
        if (i < this.polygonData.length - 1) {
          this.insertAndShift(this.polygonData, i, (i + 1));
          break;
        }
      }
    }

  }

  allHigh_clickHandler() {
    this.allHigh = !this.allHigh;
  }
  

}
