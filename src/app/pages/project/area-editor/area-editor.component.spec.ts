import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaEditorComponent } from './area-editor.component';

describe('AreaEditorComponent', () => {
  let component: AreaEditorComponent;
  let fixture: ComponentFixture<AreaEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
