import {
  Component,
  OnInit,
  ViewChild,
  ComponentFactoryResolver,
  ViewEncapsulation
} from "@angular/core";
import { Subscription } from "rxjs";
import { MessageService } from "src/app/services/message.service";
import { ScdFileService } from 'src/app/services/scdfile.service';

import { EngineComponentDirective } from "src/app/engine/engine-component.directive";
import { EngineComponent } from "src/app/engine/engine.component";
import { ProjectComponentDirective } from "./project-component.directive";
import { ProjectInformationComponent } from "./project-information/project-information.component";
import {
  trigger,
  transition,
  state,
  animate,
  style,
} from "@angular/animations";

@Component({
  selector: "app-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger("flyInOut", [
      state("in", style({ transform: "translateX(0)" })),
      transition("void => *", [
        style({ transform: "translateX(-100%)" }),
        animate(500),
      ]),
      transition("* => void", [
        animate(500, style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
})
export class ProjectComponent implements OnInit {
  
  mainHeight: number = 400;
  subscription: Subscription;
  componentID: number = this.MS.COMPONENT_ID;
  componentFactory: any[] = new Array();
  componentRef: any[] = new Array();
  viewContainerRef: any;
  engineFactory: any;
  engineRef: any;
  engine_viewContainerRef: any;
  menuGotoComponent: any;

  public showHideMenu: boolean = false;

  @ViewChild(EngineComponentDirective, { static: true })
  engineComponentHost: EngineComponentDirective;
  @ViewChild(ProjectComponentDirective, { static: true })
  projectComponentHost: ProjectComponentDirective;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private MS: MessageService,
    private gService: ScdFileService
  ) {

    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe((message) => {
      if (message) { 

        const msg: string = message.split(":")[0];
        const componentID: number = message.split(":")[1];

        if (msg == "MENU_GOTO_NAVIGATE_CHANGE_COMPONENT_ID") {
          this.componentID = componentID;
        }

        if (message == "MENU_GOTO_NEXT_CLICK" 
          || message == "MENU_GOTO_PREVIOUS_CLICK" 
          || msg == "MENU_GOTO_NAVIGATE_CHANGE_COMPONENT_ID") {

          if (message == "MENU_GOTO_NEXT_CLICK") {
            if (this.componentID < 5) {
              ++this.componentID;
            }
          } 
          else if (message == "MENU_GOTO_PREVIOUS_CLICK") {
            if (this.componentID > 0) {
              --this.componentID;
            }
          }

            if (this.componentID == 0) {

              this.loadComponent(this.componentID, ProjectInformationComponent);
              this.engine_viewContainerRef.detach();
            } 
            else if (this.componentID == 1) {
                     
              import("./image-acquisition/image-acquisition.component").then(
                (m) => {
                  this.loadComponent(
                    this.componentID,
                    m.ImageAcquisitionComponent
                  );
                  this.engine_viewContainerRef.detach();
                }
              );
            } 
            else if (this.componentID == 2) {
              import("./measure-align/measure-align.component").then((m) => {
                this.loadComponent(this.componentID, m.MeasureAlignComponent);
                this.loadEngine();
              });
            } 
            else if (this.componentID == 3) {
              import("./line-editor/line-editor.component").then((m) => {
                this.loadComponent(this.componentID, m.LineEditorComponent);
                this.loadEngine();
              });

            } 
            else if (this.componentID == 4) {
              import("./area-editor/area-editor.component").then((m) => {
                this.loadComponent(this.componentID, m.AreaEditorComponent);
                this.loadEngine();
              });
            } 
            else if (this.componentID == 5) {

              import("./reports/reports.component").then((m) => {
                this.loadComponent(this.componentID, m.ReportsComponent);
                this.engine_viewContainerRef.detach();
                  
                this.MS.sendMessage("PROJECT_REPORTS_INIT");              
              });
            }
            this.MS.COMPONENT_ID = this.componentID;
            this.MS.sendMessage("ComponentForEngine:" + this.componentID);
          	this.MS.sendMessage("Menu_FooterForComponent:" + this.componentID);
          
          }       
        }
      });
  }

  ngOnInit() {

    this.disableRefresh();

    this.engine_viewContainerRef = this.engineComponentHost.viewContainerRef;
    this.viewContainerRef = this.projectComponentHost.viewContainerRef;
    this.loadComponent(this.componentID, ProjectInformationComponent);

    //load threejs canvas when project is loaded and hide, so that canvas variables are available to save/import
    this.loadEngine();
  }

  ngAfterContentChecked() {
    this.onResize();
  }

  ngAfterViewInit() {
    //when Engine Service Loaded, the detach canvas view at Project Information page
    this.engine_viewContainerRef.detach();
  }

  loadComponent(componentID: number, component: any) {
    this.viewContainerRef.detach();

    if (this.componentFactory[componentID] == undefined) {
      this.componentFactory[componentID] = this.componentFactoryResolver.resolveComponentFactory(component);
      this.componentRef[componentID] = this.viewContainerRef.createComponent( this.componentFactory[componentID] );
    } 
    else {
      this.viewContainerRef.insert(this.componentRef[componentID].hostView);
    }

  }

  loadEngine() {
    this.engine_viewContainerRef.detach();

    if (this.engineFactory == undefined) {
      this.engineFactory = this.componentFactoryResolver.resolveComponentFactory(
        EngineComponent
      );
      this.engineRef = this.engine_viewContainerRef.createComponent(
        this.engineFactory
      );
    } else {
      this.engine_viewContainerRef.insert(this.engineRef.hostView);
    }
  }

  onResize() {
    var windowHeight: number = document.documentElement.clientHeight;
    var headerHeight: number = document.getElementsByTagName("header")[0].clientHeight;
    var footerHeight: number = document.getElementsByTagName("footer")[0].clientHeight;
    this.mainHeight = windowHeight - (headerHeight + footerHeight);
  }

  disableRefresh() {
    window.addEventListener("keyup", disableF5);

    window.addEventListener("keydown", disableF5);

    function disableF5(e) {

       if ((e.which || e.keyCode) == 116) e.preventDefault(); 

    };
  }


}
