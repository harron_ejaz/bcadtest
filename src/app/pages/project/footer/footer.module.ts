import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { FooterComponent } from './footer.component';
import { FooterProjectInformationComponent } from './footer-project-information/footer-project-information.component';
import { FooterImageAcquisitionComponent } from './footer-image-acquisition/footer-image-acquisition.component';
import { FooterLineEditorComponent } from './footer-line-editor/footer-line-editor.component';
import { FooterMeasureAlignComponent } from './footer-measure-align/footer-measure-align.component';
import { FooterAreaEditorComponent } from './footer-area-editor/footer-area-editor.component';
import { FooterReportsComponent } from './footer-reports/footer-reports.component';




@NgModule({
  declarations: [FooterComponent, FooterProjectInformationComponent, FooterImageAcquisitionComponent, FooterLineEditorComponent, FooterMeasureAlignComponent, FooterAreaEditorComponent, FooterReportsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ButtonsModule.forRoot()
  ],
  exports: [FooterComponent]
})
export class FooterModule { }
