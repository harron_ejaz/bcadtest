import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterLineEditorComponent } from './footer-line-editor.component';

describe('FooterLineEditorComponent', () => {
  let component: FooterLineEditorComponent;
  let fixture: ComponentFixture<FooterLineEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterLineEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterLineEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
