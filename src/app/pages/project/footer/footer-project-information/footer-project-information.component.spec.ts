import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterProjectInformationComponent } from './footer-project-information.component';

describe('FooterProjectInformationComponent', () => {
  let component: FooterProjectInformationComponent;
  let fixture: ComponentFixture<FooterProjectInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterProjectInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterProjectInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
