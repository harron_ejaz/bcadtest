import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterImageAcquisitionComponent } from './footer-image-acquisition.component';

describe('FooterImageAcquisitionComponent', () => {
  let component: FooterImageAcquisitionComponent;
  let fixture: ComponentFixture<FooterImageAcquisitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterImageAcquisitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterImageAcquisitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
