import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']  
})
export class FooterComponent {

  subscription: Subscription; 

  constructor(public MS: MessageService) {
    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe(message => {


    })
  }

  areaEditorFooter_moveHandler(e: MouseEvent) {
    if( this.MS.area_editor.DRAGGING_POLYGON ) {
      document.body.style.cursor = "grabbing";
    }
  }

  areaEditorFooter_upHandler(e: MouseEvent) {
    if( this.MS.area_editor.DRAGGING_POLYGON ) {
      this.MS.area_editor.DRAGGING_POLYGON = false;
      document.body.style.cursor = "default";
    }
  }

}
