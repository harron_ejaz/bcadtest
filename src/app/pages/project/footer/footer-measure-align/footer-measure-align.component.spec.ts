import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterMeasureAlignComponent } from './footer-measure-align.component';

describe('FooterMeasureAlignComponent', () => {
  let component: FooterMeasureAlignComponent;
  let fixture: ComponentFixture<FooterMeasureAlignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterMeasureAlignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterMeasureAlignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
