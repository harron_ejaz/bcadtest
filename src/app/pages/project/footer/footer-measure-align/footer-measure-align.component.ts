import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-footer-measure-align',
  templateUrl: './footer-measure-align.component.html',
  styleUrls: ['./footer-measure-align.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterMeasureAlignComponent {

  constructor() { }

}
