import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterAreaEditorComponent } from './footer-area-editor.component';

describe('FooterAreaEditorComponent', () => {
  let component: FooterAreaEditorComponent;
  let fixture: ComponentFixture<FooterAreaEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterAreaEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterAreaEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
