import { Component } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal";
import { MessageService } from "src/app/services/message.service";
import { EngineService } from "src/app/engine/engine.service";
import { ScdFileService } from 'src/app/services/scdfile.service';

@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"]
})
export class SettingsComponent {

  backgroundColor:any;
 
  constructor(public bsModalRef: BsModalRef, public MS: MessageService, private engServ: EngineService,  private gService: ScdFileService) {
    this.backgroundColor = this.MS.color.EXTENT;
  }

  crosshair_changeHandler(value: number) {
    this.MS.settings.CROSSHAIR_SIZE = Number( value );
  }

  backgroundColor_clickHandler(value: number) {
    this.MS.settings.SELECTED_INDEX_BACKGROUND_COLOR = value;
    if (value == 1) {
        this.backgroundColor = "#FFFFFF"; 
        this.MS.settings.CROSSHAIR_COLOR = "#000000";
        this.MS.settings.BACKGROUND_COLOR = "#FFFFFF";
    }
    else if (value == 2) { 
        this.backgroundColor = "#CCCCCC"; 
        this.MS.settings.CROSSHAIR_COLOR = "#000000";
        this.MS.settings.BACKGROUND_COLOR = "#CCCCCC";
 
    }
    else if (value == 3) { 
        this.backgroundColor = "#999999"; 
        this.MS.settings.CROSSHAIR_COLOR = "#FFFFFF";
        this.MS.settings.BACKGROUND_COLOR = "#999999";
 
    }
    else if (value == 4) { 
        this.backgroundColor = "#666666"; 
        this.MS.settings.CROSSHAIR_COLOR = "#000000";
        this.MS.settings.BACKGROUND_COLOR = "#666666";
 
    }
    else if (value == 5) { 
        this.backgroundColor = "#333333"; 
        this.MS.settings.CROSSHAIR_COLOR = "#0000FF";
        this.MS.settings.BACKGROUND_COLOR = "#333333";
 
    }
    else if (value == 6) { 
        this.backgroundColor = "#000000"; 
        this.MS.settings.CROSSHAIR_COLOR = "#FFFFFF";
        this.MS.settings.BACKGROUND_COLOR = "#000000";
 
    }

    this.MS.sendMessage("CROSSHAIR_COLOR_CHANGED");
    console.log(this.MS.settings);
  }

  ok_clickHandler() {
console.log(this.MS.settings);
    this.gService.saveUserSettings(this.MS.settings);
    this.MS.color.EXTENT = this.backgroundColor;
    this.engServ.changeExtentBackgroundColor();

    this.bsModalRef.hide();

  }

  saveSCD_clickhandler(event) {
    const checked = event.target.checked;
    this.MS.settings.AUTO_SAVE = checked;
  }

  dimStandard_clickhandler(event) {
    const checked = event.target.checked;
    this.MS.settings.DIMENSIONS_STANDARD = checked;
  }

  dimMetric_clickhandler(event) {
    const checked = event.target.checked;
    this.MS.settings.DIMENSIONS_METRIC = checked;
  }

}
