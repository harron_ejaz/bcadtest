import { Component, ViewEncapsulation } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-report-page-error',
  templateUrl: './report-page-error.component.html',
  styleUrls: ['./report-page-error.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportPageErrorComponent {
  public polygonExistMessage = new Array();
  public polygonNagetiveMessage = new Array();
  constructor( public MS:MessageService, public bsModalRef: BsModalRef) { 

    this.polygonExistMessage = this.MS.report.POLYGON_EXTIST_MESSAGES;
    this.polygonNagetiveMessage = this.nagetiveMessage();
  }

  nagetiveMessage() {
    let messages = [];
    for(let i=0; i < this.MS.report.POLYGON_NAGETIVE_ALPHABETS.length; i++) {
      messages.push(this.MS.report.POLYGON_NAGETIVE_ALPHABETS[i]);
    }
    return messages;
  }

  ok_clickHandler() {
    this.bsModalRef.hide(); //this.subscriptions_PolygonBreakConfirm in selection.ts runs
  }
}

