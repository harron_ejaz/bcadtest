import { Component, ViewEncapsulation } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-submit-error',
  templateUrl: './submit-error.component.html',
  styleUrls: ['./submit-error.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubmitErrorComponent {

  constructor( public MS:MessageService, public bsModalRef: BsModalRef) { }

  ok_clickHandler() {
    this.bsModalRef.hide(); //this.subscriptions_PolygonBreakConfirm in selection.ts runs
    
  }

}
