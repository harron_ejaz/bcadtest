import { Component, OnInit } from '@angular/core';
import { BsModalRef } from "ngx-bootstrap/modal";
import { Router } from '@angular/router';

@Component({
  selector: 'app-close-project',
  templateUrl: './close-project.component.html',
  styleUrls: ['./close-project.component.scss']
})
export class CloseProjectComponent implements OnInit {

  constructor(public bsModalRef: BsModalRef,  private router: Router) { }

  ngOnInit(): void {
  }
  ok_clickHandler() {
    this.router.navigate(['open-project']);
    this.bsModalRef.hide();

  }


}
