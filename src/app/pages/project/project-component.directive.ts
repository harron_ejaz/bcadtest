import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[project-component-host]'
})
export class ProjectComponentDirective {

  constructor( public viewContainerRef: ViewContainerRef ) { }

}
