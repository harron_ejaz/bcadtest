import { EngineService } from './../../../engine/engine.service';
import { Component } from "@angular/core";
import { Subscription } from "rxjs";
import { MessageService } from "src/app/services/message.service";
import {
  trigger,
  transition,
  state,
  animate,
  style,
} from "@angular/animations";
import { FunctionService } from 'src/app/services/function.service';
import { ScdFileService } from 'src/app/services/scdfile.service';

@Component({
  selector: "app-project-information",
  templateUrl: "./project-information.component.html",
  styleUrls: ["./project-information.component.scss"],
  animations: [
    trigger("flyInOut", [
      state("in", style({ transform: "translateX(0)" })),
      transition("void => *", [
        style({ transform: "translateX(-100%)" }),
        animate(500),
      ]),
      transition("* => void", [
        animate(500, style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
})

export class ProjectInformationComponent {

  subscription: Subscription;

  public isMultipleStr: boolean = true;
  public orderID;
  autoSave: number = 0; 

  constructor(private engServ: EngineService, public MS: MessageService, private FS: FunctionService,     private gService: ScdFileService) {

    this.addStructure_clickHandler( null );

    this.orderID = localStorage.getItem("orderID");
    
     this.subscription = this.MS.getMessage().subscribe((message) => {

      if(message == "SET_ORDER_ID") {
        this.orderID = this.MS.project_information.ORDER_ID;
      }
      //scd data is going to load, so delete 1 bulding that was loaded automatically
      else if(message == "LOAD_SCD_DELETE_LAST_BUILDING") {
        this.deleteLastBuilding_clickHandler("scd_load");
      }

    });
    this.autoSaveSCD(0);
  }

  ngAfterViewInit() {
    let scope = this;
    setTimeout(function() { 
      scope.MS.sendMessage('LOAD_SCD'); 
    }, 1000);
  }

  isMultiStructure_changeHandler(checked: boolean) {
    this.isMultipleStr = checked;
    this.MS.project_information.IS_MULTI_STRUCTURE = checked;
  }

  addStructure_clickHandler( type:string ) {
        
    let i: number = this.MS.project_information.BUILDINGS.length;

    let obj: any = new Object();

    obj.Name = "Structure " + (i + 1);
    obj.Count = 1;
    obj.Viewport = null;
    obj.PlaneListBreakingPoint = 40;
    obj.TotalListBreakingPoint = 10;
    obj.SiteImage = null;
    obj.NorthImage = null;
    obj.SouthImage = null;
    obj.EastImage = null;
    obj.WestImage = null;
    obj.AlignmentLineStart = "0,0";
    obj.AlignmentLineEnd = "0,0";
    obj.MeasurementLineStart = "0,0";
    obj.MeasurementLineEnd = "0,0";
    obj.MeasurementLength = 0;
    obj.WhichNorth = null;
    obj.SubType = 0;
    obj.HasSubBuilding = false;
 

    this.MS.project_information.BUILDINGS.push( obj );

    //if button is clicked, 
    //means this function is not called automatically for the first time when project gets loaded
    if( type == 'click' ) {
      this.engServ.addBuildingsGroup( type );
    }

  }

  deleteLastBuilding_clickHandler(type:string) {

    this.MS.sendMessage("PROJECT_INFORMATION_DELETE_LAST_BUILDING_CLICK");

    //donot delete if only 1 record exists when delete button is clicked
    //also set index to newly selected item
    if( type == 'click' && this.engServ.rootBuildingsGroup.children.length == 1 ) {
      return;
    }
      

    let index:number = this.engServ.rootBuildingsGroup.children.length - 1;
    let buildingsGroup:any = this.engServ.rootBuildingsGroup.children[ index ];
    this.engServ.rootBuildingsGroup.remove( buildingsGroup );

    this.MS.project_information.BUILDINGS.pop();

  }

  projectAddress_changeHandler(value: string) {
    this.MS.project_information.PROJECT_ADDRESS = value;
  }

  customerNotes_changeHandler(value: string) {
    this.MS.project_information.CUSTOMER_NOTES = value;
  }

  qcNotes_changeHandler(value: string) {
    this.MS.project_information.QC_NOTES = value;
  }

  structureName_changeHandler(id: string, value: string) {
    let index: number = parseInt( id.split(":")[1] );
    this.MS.project_information.BUILDINGS[ index ].Name = value;

    let buildingsGroup:any = this.engServ.rootBuildingsGroup.children[ index ];
    buildingsGroup.name = value;
  }

  structureCount_changeHandler(id: string, value: string) {
    let i: number = parseInt(id.split(":")[1]);
    this.MS.project_information.BUILDINGS[ i ].Count = parseInt(value);
  }

  autoSaveSCD(index) {
    this.MS.saveAndSubmit.ignoreSpinnerAutoSave = true;
    if(index == 1) {
        setTimeout (() => {
          if( this.MS.settings.AUTO_SAVE ) {
            this.autoSave = 1;
            this.MS.saveAndSubmit.isSaveAndSubmit = false;
            this.MS.saveAndSubmit.ignoreSpinnerAutoSave = false;
            this.MS.sendMessage("SAVE_SCD");
          }
          this.autoSaveSCD(1);
        }, 300000); 
    } else {
      if(this.autoSave != 1) this.autoSaveSCD(1);
    }
  }

}
