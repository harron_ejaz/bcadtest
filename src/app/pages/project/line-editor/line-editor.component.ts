import { Component } from "@angular/core";
import { MessageService } from "src/app/services/message.service";
import {
  trigger,
  transition,
  state,
  animate,
  style,
} from "@angular/animations";


@Component({
  selector: "app-line-editor",
  templateUrl: "./line-editor.component.html",
  styleUrls: ["./line-editor.component.scss"],
  animations: [
    trigger("flyInOut", [
      state("in", style({ transform: "translateX(0)" })),
      transition("void => *", [
        style({ transform: "translateX(-100%)" }),
        animate(500),
      ]),
      transition("* => void", [
        animate(500, style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
})
export class LineEditorComponent {

  constructor(private MS: MessageService) {

  }

  ngAfterViewInit() {
    this.MS.sendMessage('LOAD_SCD');
  }

}
