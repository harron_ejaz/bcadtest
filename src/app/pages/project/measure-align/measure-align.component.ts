import { Component } from "@angular/core";
import { MessageService } from "src/app/services/message.service";

import {
  trigger,
  transition,
  state,
  animate,
  style,
} from "@angular/animations";

@Component({
  selector: "app-measure-align",
  templateUrl: "./measure-align.component.html",
  styleUrls: ["./measure-align.component.scss"],
  animations: [
    trigger("flyInOut", [
      state("in", style({ transform: "translateX(0)" })),
      transition("void => *", [
        style({ transform: "translateX(-100%)" }),
        animate(500),
      ]),
      transition("* => void", [
        animate(500, style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
})

export class MeasureAlignComponent {
  bgColor: string;
  public borderclass: string = "borderclass form-control";

  constructor(private MS: MessageService) {
    this.borderclass = "borderclass form-control";
  }

  ngAfterViewInit() {
    this.MS.sendMessage('LOAD_SCD');
  }

}
