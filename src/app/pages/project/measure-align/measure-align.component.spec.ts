import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasureAlignComponent } from './measure-align.component';

describe('MeasureAlignComponent', () => {
  let component: MeasureAlignComponent;
  let fixture: ComponentFixture<MeasureAlignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasureAlignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasureAlignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
