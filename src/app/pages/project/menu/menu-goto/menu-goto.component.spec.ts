import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuGotoComponent } from './menu-goto.component';

describe('MenuGotoComponent', () => {
  let component: MenuGotoComponent;
  let fixture: ComponentFixture<MenuGotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuGotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuGotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
