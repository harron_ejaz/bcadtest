import { Component, ViewEncapsulation } from "@angular/core";
import { MessageService } from "src/app/services/message.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-menu-goto",
  templateUrl: "./menu-goto.component.html",
  styleUrls: ["./menu-goto.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class MenuGotoComponent {
  
  subscription: Subscription;
  componentID: number = 0;

  constructor(public MS: MessageService) { 

    this.subscription = this.MS.getMessage().subscribe((message) => {
      if (message) {

        const msg: string = message.split(":")[0];
        const componentID: number = message.split(":")[1];

        if (msg == "Menu_FooterForComponent") {
          this.componentID = componentID;
        }
      }
    });
  }

  previous_clickHandler() {
    // send message to subscribers via observable subject
    this.MS.sendMessage("MENU_GOTO_PREVIOUS_CLICK");
  }

  next_clickHandler() {
    this.MS.sendMessage("MENU_GOTO_NEXT_CLICK");
  }

  navigate_changeHandler(componentID: number) {
    this.MS.sendMessage(
      "MENU_GOTO_NAVIGATE_CHANGE_COMPONENT_ID:" + componentID
    );
  }
}
