import { Component, ViewEncapsulation } from '@angular/core';
import { ObjectLoader, Group } from 'three';
import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from 'src/app/services/message.service';
import { Router } from '@angular/router';
import { CloseProjectComponent } from '../../dialog/close-project/close-project.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-menu-project',
  templateUrl: './menu-project.component.html',
  styleUrls: ['./menu-project.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuProjectComponent {

  private engGroup;
  private groupJSON:JSON;

  constructor(private engServ: EngineService, private MS: MessageService, private router: Router, private modalService: BsModalService) {}

  saveAs_clickHandler() {
    if( this.engServ.rootBuildingsGroup != undefined) {
      this.MS.engineGroupJSON = this.engServ.rootBuildingsGroup.toJSON();
      this.engServ.scene.remove(this.engServ.rootBuildingsGroup);
      
      console.log(this.MS.engineGroupJSON)   
      console.log("JSON saved in a variable"); 
    }
    else
      console.log("Canvas is not loaded"); 
  }

  save_clickHandler() {
    this.MS.sendMessage("SAVE_SCD");
  }


  import_clickHandler() { 
    const loader = new ObjectLoader();
    this.engGroup = loader.parse( this.groupJSON );

    this.engServ.rootBuildingsGroup = new Group();
    this.engServ.scene.add( this.engServ.rootBuildingsGroup );

    const groupLength:number = this.engGroup.children.length;
    for (let i:number = 0; i < groupLength; i++) {
      this.engServ.addToScene( this.engGroup.children[i] );
      
    }

    console.log("Canvas JSON loaded")
  }

  close_clickHandler() {
    this.modalService.show( CloseProjectComponent );
  }

}
