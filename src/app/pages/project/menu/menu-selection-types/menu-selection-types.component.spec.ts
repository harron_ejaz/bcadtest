import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSelectionTypesComponent } from './menu-selection-types.component';

describe('MenuSelectionTypesComponent', () => {
  let component: MenuSelectionTypesComponent;
  let fixture: ComponentFixture<MenuSelectionTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSelectionTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSelectionTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
