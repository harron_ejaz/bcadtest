import { Component } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu-selection-types',
  templateUrl: './menu-selection-types.component.html',
  styleUrls: ['./menu-selection-types.component.scss']
})
export class MenuSelectionTypesComponent {

  subscription: Subscription;

  constructor(public MS:MessageService) {
    this.subscription = this.MS.getMessage().subscribe((message) => {

      if (message == "DISABLE_ALL_MENU_SELECTION_BUTTONS") {
        this.falseAll();
        this.MS.sendMessage("MENU_SELECTION");
      }
      else if (message == "SELECTION") {
        this.MS.menu_selection.SELECTION = false;
        this.selection_clickHandler();
      }
      else if( message == "HOTKEY_CTRLA_SELECTALL_LINE_EDITOR") {
        this.selectAll_clickHandler();
      }
      else if (message == "HOTKEY_ESC_LINE_EDITOR") {
        this.selection_clickHandler();
      }
      
      
    });
   }


  selection_clickHandler() {
    this.MS.menu_selection.SELECTION = !this.MS.menu_selection.SELECTION;
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.sendMessage("MENU_SELECTION"); 
    
  }

  selectAll_clickHandler() {
    this.MS.menu_selection.SELECT_ALL = !this.MS.menu_selection.SELECT_ALL;
    this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH = false;
    this.MS.menu_selection.SELECT_BY_LINE_TYPE = false;
    this.MS.menu_selection.SELECT_BY_PITCH = false;
    this.MS.sendMessage("MENU_SELECTION");
  }

  selectByLineType_clickHandler() {
    this.MS.menu_selection.SELECT_BY_LINE_TYPE = !this.MS.menu_selection.SELECT_BY_LINE_TYPE;
    this.MS.menu_selection.SELECT_ALL = false;
    this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH = false;
    this.MS.menu_selection.SELECT_BY_PITCH = false;
    this.MS.sendMessage("MENU_SELECTION");
  }

  selectByPitch_clickHandler() {
    this.MS.menu_selection.SELECT_BY_PITCH = !this.MS.menu_selection.SELECT_BY_PITCH;
    this.MS.menu_selection.SELECT_ALL = false;
    this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH = false;
    this.MS.menu_selection.SELECT_BY_LINE_TYPE = false;
    this.MS.sendMessage("MENU_SELECTION");
  }

  selectByLineTypeAndPitch_clickHandler() {
    this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH = !this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH;
    this.MS.menu_selection.SELECT_ALL = false;
    this.MS.menu_selection.SELECT_BY_LINE_TYPE = false;
    this.MS.menu_selection.SELECT_BY_PITCH = false;
    this.MS.sendMessage("MENU_SELECTION");
  }

  falseAll() {
    this.MS.menu_selection.SELECTION = false;
    this.MS.menu_selection.SELECT_ALL = false;
    this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH = false;
    this.MS.menu_selection.SELECT_BY_LINE_TYPE = false;
    this.MS.menu_selection.SELECT_BY_PITCH = false;
  }

}
