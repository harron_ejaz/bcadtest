import { Component, ViewEncapsulation } from "@angular/core";
import { MessageService } from "src/app/services/message.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-menu-input-length",
  templateUrl: "./menu-input-length.component.html",
  styleUrls: ["./menu-input-length.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class MenuInputLengthComponent {

  subscription: Subscription;
  lengthInputBorder: string = "lengthInputBorder";

  constructor(public MS: MessageService) {
  }

  inputLength_changeHandler(value: string) {
    const inputLength: number = parseFloat(value); //feet
    this.MS.input_scaleLength.inputLength = inputLength;
    // send message to subscribers via observable subject
    this.MS.sendMessage("INPUT_LENGTH_CHANGE");

  }


}
