import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuInputLengthComponent } from './menu-input-lenght.component';

describe('MenuInputLenghtComponent', () => {
  let component: MenuInputLengthComponent;
  let fixture: ComponentFixture<MenuInputLengthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuInputLengthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuInputLengthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
