import { Component, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageService } from 'src/app/services/message.service';



@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent {

  showDropDown : string = '';
  subscription: Subscription; 
  overflowCls : string = "dropDownResponsiveAuto";

  constructor(public MS: MessageService) {
    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe(message => {

      if (message) {
        let msg: string = message.split(':')[0];
        let componentID: number = message.split(':')[1];
        
        if (msg == "CHANGE_SCROLL_WHILE_LINETYPE") { 
          this.overflowCls = "dropDownResponsive";
        }

        if (msg == "CHANGE_SCROLL_WHILE_LINETYPE_AUTO") { 
         this.overflowCls = "dropDownResponsiveAuto";
        }


 
      }

    });
  }

  areaEditorMenu_moveHandler(e: MouseEvent) {
    if( this.MS.area_editor.DRAGGING_POLYGON ) {
      document.body.style.cursor = "grabbing";
    }
  }

  areaEditorMenu_upHandler(e: MouseEvent) {
    if( this.MS.area_editor.DRAGGING_POLYGON ) {
      this.MS.area_editor.DRAGGING_POLYGON = false;
      document.body.style.cursor = "default";
    }
  }


}
