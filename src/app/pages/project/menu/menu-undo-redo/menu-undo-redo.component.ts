import { Component, ViewEncapsulation, Input } from '@angular/core';
import { ObjectLoader, Group } from 'three';
import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu-undo-redo',
  templateUrl: './menu-undo-redo.component.html',
  styleUrls: ['./menu-undo-redo.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuUndoRedoComponent {

  @Input() componentID: number

  subscription: Subscription;

  constructor(private engServ: EngineService, private MS:MessageService) {
    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe(message => {
      //Polygon break confirm dialog is clicked with No and Line Editor page is opened
      if (message == "UNDO" && this.componentID == 4) {
          console.log("undo 4")
      }
      else if (message == "UNDO" && this.componentID == 5) {
        console.log("undo 5")
      }
    });

  }

  undo_clickHandler() { 

    this.engServ.sessionStorageKeyIndex--;
 
    if( this.engServ.sessionStorageKeyIndex > -1 ) {

      this.updateEngineGroup();

    }
    else {
      this.engServ.sessionStorageKeyIndex = 0;
    }
    
  }

  redo_clickHandler() {

    if( this.engServ.sessionStorageKeyIndex < sessionStorage.length-1 ) {
      
      this.engServ.sessionStorageKeyIndex++;
      
      this.updateEngineGroup();
      
    }
      
  }

  updateEngineGroup() {

    //remove all dimensions from DimensionsGroup
    //send message to subscribers via observable subject
    this.MS.sendMessage('REMOVE_ALL_DIMENSIONS');

    //remove all objects from scene group
    this.engServ.scene.remove(this.engServ.rootBuildingsGroup); 

    //load json from session storage
    const loader = new ObjectLoader();
    const groupJSON = JSON.parse( sessionStorage.getItem( this.engServ.sessionStorageKeyIndex.toString() ) );

    let engGroup:Group = loader.parse( groupJSON );
    this.engServ.rootBuildingsGroup = new Group();
    this.engServ.rootBuildingsGroup.copy( engGroup );
    this.engServ.scene.add( this.engServ.rootBuildingsGroup ); 

    

    //add all labels to LabelGroup
    // send message to subscribers via observable subject
    this.MS.sendMessage('ADD_ALL_DIMENSIONS');
  

  }

}
