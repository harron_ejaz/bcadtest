import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuUndoRedoComponent } from './menu-undo-redo.component';

describe('MenuUndoRedoComponent', () => {
  let component: MenuUndoRedoComponent;
  let fixture: ComponentFixture<MenuUndoRedoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuUndoRedoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuUndoRedoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
