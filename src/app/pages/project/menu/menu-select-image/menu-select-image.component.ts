import { Component, ViewEncapsulation } from "@angular/core";
import { MessageService } from "src/app/services/message.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-menu-select-image",
  templateUrl: "./menu-select-image.component.html",
  styleUrls: ["./menu-select-image.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class MenuSelectImageComponent {

  google_chk: boolean = false;
  nearmap_chk: boolean = true;
  custom_chk: boolean = false;

  constructor(public MS: MessageService) {}

  loadSingleImage_clickHandler(evt: any) {
    if (evt.target.checked) {
      this.MS.menu_select_image.IS_SINGLE_OVERHEAD_IMAGE = true;
      this.MS.sendMessage("LOAD_SINGLE_IMAGE");
    } 
    else {
      this.MS.menu_select_image.IS_SINGLE_OVERHEAD_IMAGE = false;
      this.MS.sendMessage("LOAD_FOUR_IMAGE");
    }
  }

  loadFourImage_clickHandler(evt: any) {
    if (evt.target.checked) {
      this.MS.menu_select_image.IS_SINGLE_OVERHEAD_IMAGE = false;
      this.MS.sendMessage("LOAD_FOUR_IMAGE");
    } 
    else {
      this.MS.menu_select_image.IS_SINGLE_OVERHEAD_IMAGE = true;
      this.MS.sendMessage("LOAD_SINGLE_IMAGE");
    }
  }

  google_chk_clickHandler(evt: any) {
    this.google_chk = evt.target.checked;
    this.nearmap_chk = false;
    this.custom_chk = false;
  }

  nearmap_chk_clickHandler(evt: any) {
    this.google_chk = false;
    this.nearmap_chk = evt.target.checked;
    this.custom_chk = false;
  }

  custom_chk_clickHandler(evt: any) {
    this.google_chk = false;
    this.nearmap_chk = false;
    this.custom_chk = evt.target.checked;
  }

}
