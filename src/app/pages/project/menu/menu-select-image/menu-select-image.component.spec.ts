import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSelectImageComponent } from './menu-select-image.component';

describe('MenuSelectImageComponent', () => {
  let component: MenuSelectImageComponent;
  let fixture: ComponentFixture<MenuSelectImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSelectImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSelectImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
