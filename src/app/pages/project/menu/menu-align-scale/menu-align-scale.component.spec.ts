import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuAlignScaleComponent } from './menu-align-scale.component';

describe('MenuAlignScaleComponent', () => {
  let component: MenuAlignScaleComponent;
  let fixture: ComponentFixture<MenuAlignScaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuAlignScaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuAlignScaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
