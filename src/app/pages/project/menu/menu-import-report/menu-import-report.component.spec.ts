import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuImportReportComponent } from './menu-import-report.component';

describe('MenuImportReportComponent', () => {
  let component: MenuImportReportComponent;
  let fixture: ComponentFixture<MenuImportReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuImportReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuImportReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
