import { Component } from '@angular/core';
import { ObjectLoader, Group } from 'three';
import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-menu-import-report',
  templateUrl: './menu-import-report.component.html',
  styleUrls: ['./menu-import-report.component.scss']
})
export class MenuImportReportComponent {

  private engGroup:any;

  constructor(private engServ: EngineService, private MS: MessageService) { }

  import_clickHandler() { 
    const loader = new ObjectLoader();
    this.engGroup = loader.parse( this.MS.engineGroupJSON );

    this.engServ.rootBuildingsGroup = new Group();
    this.engServ.scene.add( this.engServ.rootBuildingsGroup );

    const groupLength:number = this.engGroup.children.length;
    for (let i:number = 0; i < groupLength; i++) {
      this.engServ.rootBuildingsGroup.add( this.engGroup.children[i] );
      
    }

    console.log("Project loaded")
  }

}
