import { Component, ViewEncapsulation } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu-clipboard',
  templateUrl: './menu-clipboard.component.html',
  styleUrls: ['./menu-clipboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuClipboardComponent{

  subscription: Subscription;

  constructor(private MS: MessageService) { 
    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe((message) => {

      if (message == "") {
        
      }

    });
  }

  cut_clickHandler() {
    this.MS.sendMessage("CLIPBOARD_CUT");
  }

  copy_clickHandler() {
    this.MS.sendMessage("CLIPBOARD_COPY");
  }

  delete_clickHandler() {
    this.MS.sendMessage("CLIPBOARD_DELETE");
  }

  paste_clickHandler() {
    this.MS.sendMessage("CLIPBOARD_PASTE");
  }

  falseAll() {
  }

}
