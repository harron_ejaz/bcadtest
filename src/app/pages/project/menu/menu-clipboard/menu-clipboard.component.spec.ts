import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuClipboardComponent } from './menu-clipboard.component';

describe('MenuClipboardComponent', () => {
  let component: MenuClipboardComponent;
  let fixture: ComponentFixture<MenuClipboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuClipboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuClipboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
