import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuInsertComponent } from './menu-insert.component';

describe('MenuInsertComponent', () => {
  let component: MenuInsertComponent;
  let fixture: ComponentFixture<MenuInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
