import { Component, ViewEncapsulation } from "@angular/core";
import { Subscription } from "rxjs";
import { MessageService } from "src/app/services/message.service";

@Component({
  selector: "app-menu-insert",
  templateUrl: "./menu-insert.component.html",
  styleUrls: ["./menu-insert.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class MenuInsertComponent {
  subscription: Subscription;

  line_btn_enabled = false;
  arc_btn_enabled = false;
  triangle_btn_enabled = false;
  text_btn_enabled = false;
  circle_btn_enabled = false;
  rectangle_btn_enabled = false;
  ellipse_btn_enabled = false;

  constructor(public MS: MessageService) {

    this.subscription = this.MS.getMessage().subscribe((message) => {
      if (message == "DISABLE_ALL_MENU_BUTTONS") {
        this.falseAll();
      }
      else if(message == "HOTKEY_CTRLSPACE_LINE_LINE_EDITOR") {
        this.line_clickHandler();
      }
    });
  }

  line_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.sendMessage("DISABLE_ALL_MENU_SELECTION_BUTTONS");
    //this.MS.sendMessage("ENABLE_MENU_SELECTION_BUTTON");
    this.MS.menu_insert.LINE = this.line_btn_enabled = true;
  }

  arc_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.sendMessage("DISABLE_ALL_MENU_SELECTION_BUTTONS");
   
    this.MS.menu_insert.ARC = this.arc_btn_enabled = true;
  }

  triangle_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.sendMessage("DISABLE_ALL_MENU_SELECTION_BUTTONS");
    this.MS.menu_insert.TRIANGLE = this.triangle_btn_enabled = true;
  }

  text_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.sendMessage("DISABLE_ALL_MENU_SELECTION_BUTTONS");
    this.MS.menu_insert.TEXT = this.text_btn_enabled = true;
  }

  text_inputHandler(value: string) {
    this.MS.menu_line_editor.TEXT = value;
  }

  circle_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.sendMessage("DISABLE_ALL_MENU_SELECTION_BUTTONS");
    this.MS.menu_insert.CIRCLE = this.circle_btn_enabled = true;
  }

  rectangle_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.menu_insert.RECTANGLE = this.rectangle_btn_enabled = true;
  }

  ellipse_clickHandler() {
    this.MS.sendMessage("DISABLE_ALL_MENU_BUTTONS");
    this.MS.sendMessage("DISABLE_ALL_MENU_SELECTION_BUTTONS");
    this.MS.menu_insert.ELLIPSE = this.ellipse_btn_enabled = true;
  }

  falseAll() {
    this.MS.menu_insert.LINE = this.line_btn_enabled = false;
    this.MS.menu_insert.ARC = this.arc_btn_enabled = false;
    this.MS.menu_insert.TRIANGLE = this.triangle_btn_enabled = false;
    this.MS.menu_insert.TEXT = this.text_btn_enabled = false;
    this.MS.menu_insert.CIRCLE = this.circle_btn_enabled = false;
    this.MS.menu_insert.RECTANGLE = this.rectangle_btn_enabled = false;
    this.MS.menu_insert.ELLIPSE = this.ellipse_btn_enabled = false;
    this.MS.sendMessage("REMOVE_TEXT");
  }
}
