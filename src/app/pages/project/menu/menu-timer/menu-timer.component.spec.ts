import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuTimerComponent } from './menu-timer.component';

describe('MenuTimerComponent', () => {
  let component: MenuTimerComponent;
  let fixture: ComponentFixture<MenuTimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuTimerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuTimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
