import { MOUSE } from 'node_modules/three';
import { Component, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { OrthographicCamera, Vector2, Vector3, BufferGeometry, Line, LineBasicMaterial } from 'three';

@Component({
  selector: 'app-menu-zoom',
  templateUrl: './menu-zoom.component.html',
  styleUrls: ['./menu-zoom.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuZoomComponent {

  subscription: Subscription;

  pan_btn_enabled = false;
  window_btn_enabled = false;
  extents_btn_enabled = false;

  private zoom: number;

  constructor( private engServ: EngineService, private MS: MessageService, private FS:FunctionService ) {
    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe((message) => {

        if (message == "DISABLE_ALL_MENU_BUTTONS") {
         
          this.falseAll();
        }
        else if(message == "MENU_ZOOM_WINDOW_ZOOM_TO_SELECTION") {
          this.zoomToSelection()
        }

    });
  }


  window_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.menu_zoom.WINDOW = this.window_btn_enabled = true;

    document.body.style.cursor = "default";
    this.engServ.selectionHelper.element.className = "selectionBox";

  }

  pan_clickHandler() {
    if( this.MS.menu_zoom.PAN ) {
      this.falseAll();
    }
    else {
      this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
      this.MS.sendMessage( "DISABLE_ALL_MENU_SELECTION_BUTTONS" );
      this.engServ.selectionHelper.element.className = "selectionBoxHide";
      this.engServ.orbitControls.mouseButtons.LEFT = MOUSE.PAN;
      this.MS.menu_zoom.PAN = this.pan_btn_enabled = true;
      document.body.style.cursor = "grab";
      
    }    

  }

  in_clickHandler() {
    this.zoom = this.engServ.camera.zoom;
    this.zoom = this.zoom + 0.1;
    this.zoomCamera();
  }

  out_clickHandler() {
    this.zoom = this.engServ.camera.zoom;
    if( this.zoom - 0.1 > 0.1 ) {
      this.zoom = this.zoom - 0.1;
      this.zoomCamera();
    }
  }

  extents_clickHandler() {
    let extentsGroup:any = this.engServ.commonGroup.getObjectByName("ExtentsGroup");
    if( extentsGroup.getObjectByName('Extents') ) {
      this.MS.menu_zoom.EXTENTS = this.extents_btn_enabled = false;
      this.clearExtents();
    }    
    else {
      this.MS.menu_zoom.EXTENTS = this.extents_btn_enabled = true;
      this.showExtents();
    }
      
  }

  falseAll() {
    this.pan_btn_enabled = this.MS.menu_zoom.PAN = false;
    this.window_btn_enabled = this.MS.menu_zoom.WINDOW = false;
    this.engServ.orbitControls.mouseButtons.LEFT = MOUSE.ROTATE;
    document.body.style.cursor = "default";
    this.engServ.selectionHelper.element.className = "selectionBoxHide";
  }

  showExtents() {
    let center:Vector3 = new Vector3( this.engServ.canvas.clientTop, this.engServ.canvas.clientLeft, 0 );
    const width:number = this.engServ.canvas.clientWidth / 2;
    const height:number = this.engServ.canvas.clientHeight / 2;

    //side lines
    let points = []; 
    points.push( new Vector3( center.x-width, center.y+height, 0 ) );
    points.push( new Vector3( center.x+width, center.y+height, 0 ) );
    points.push( new Vector3( center.x+width, center.y-height, 0 ) );
    points.push( new Vector3( center.x-width, center.y-height, 0 ) );
    points.push( new Vector3( center.x-width, center.y+height, 0 ) );

    let geometry = new BufferGeometry().setFromPoints( points );

    const material = new LineBasicMaterial( { color: this.MS.color.EXTENTS } );

    let extents = new Line( geometry, material );
    extents.name = "Extents";
    let extentsGroup:any = this.engServ.commonGroup.getObjectByName("ExtentsGroup"); 
    extentsGroup.add( extents );

    //center horizontal line
    points = [];
    points.push( new Vector3( center.x-width, center.y, 0 ) );
    points.push( new Vector3( center.x+width, center.y, 0 ) );

    geometry = new BufferGeometry().setFromPoints( points );

    extents = new Line( geometry, material );
    extents.name = "Extents";
    extentsGroup.add( extents );

    //center vertocal line
    points = [];
    points.push( new Vector3( center.x, center.y-height, 0 ) );
    points.push( new Vector3( center.x, center.y+height, 0 ) );

    geometry = new BufferGeometry().setFromPoints( points );

    extents = new Line( geometry, material );
    extents.name = "Extents";
    extentsGroup.add( extents );
    
  }

  clearExtents() {
    //remove all extents from ExtentsGroup
    let extentsGroup:any = this.engServ.commonGroup.getObjectByName("ExtentsGroup"); 
    for (let i:number = 0; i < extentsGroup.children.length; i++) {
        let dimensions = extentsGroup.children[i];
        extentsGroup.remove( dimensions );
        i = -1;
    }
  }

  zoomCamera() {
    this.engServ.camera.zoom = this.zoom;
    this.engServ.camera.updateProjectionMatrix();
  }

  zoomToSelection() {
    let camera:OrthographicCamera = this.engServ.camera;

    const topLeft:Vector2 = this.engServ.selectionHelper.pointTopLeft;
    const bottomRight:Vector2 = this.engServ.selectionHelper.pointBottomRight;

    const selectionWidth:number = topLeft.x - bottomRight.x;
    const selectionHeight:number = topLeft.y - bottomRight.y;

    const relativeTopLeft:Vector3 = this.engServ.get3DPosition( topLeft.x, topLeft.y );
    const relativeBottomRight:Vector3 = this.engServ.get3DPosition( bottomRight.x, bottomRight.y );

    let center:Vector3 = this.FS.get_mid_point( relativeTopLeft.x, relativeTopLeft.y, relativeBottomRight.x, relativeBottomRight.y );

    //if section is made outside canvas
    if( center == undefined ) {
      return;
    }

    camera.position.set( center.x, center.y, 400 );
    
    //The focus point of the orbit controls
    this.engServ.orbitControls.target = center;

    //smooth zoom in
    let scope=this;
    setTimeout(() => {
      scope.in_clickHandler();
    }, 50);
    setTimeout(() => {
      scope.in_clickHandler();
    }, 100);
    setTimeout(() => {
      scope.in_clickHandler();
    }, 150);
    setTimeout(() => {
      scope.in_clickHandler();
    }, 200);
    setTimeout(() => {
      scope.in_clickHandler();
    }, 250);
    
  }

}
