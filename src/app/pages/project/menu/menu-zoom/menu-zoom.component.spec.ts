import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuZoomComponent } from './menu-zoom.component';

describe('MenuZoomComponent', () => {
  let component: MenuZoomComponent;
  let fixture: ComponentFixture<MenuZoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuZoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuZoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
