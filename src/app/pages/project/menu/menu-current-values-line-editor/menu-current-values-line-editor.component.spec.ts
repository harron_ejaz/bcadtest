import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCurrentValuesLineEditorComponent } from './menu-current-values-line-editor.component';

describe('MenuCurrentValuesLineEditorComponent', () => {
  let component: MenuCurrentValuesLineEditorComponent;
  let fixture: ComponentFixture<MenuCurrentValuesLineEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuCurrentValuesLineEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCurrentValuesLineEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => { 
    expect(component).toBeTruthy();
  });
});
