import { Component, ElementRef, ViewChild } from "@angular/core";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";
import { MessageService } from 'src/app/services/message.service';
import { EngineService } from 'src/app/engine/engine.service';
import { ScdFileService } from 'src/app/services/scdfile.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';

import { Color } from 'three';
import { Subscription } from 'rxjs';

@Component({
  selector: "app-menu-current-values-line-editor",
  templateUrl: "./menu-current-values-line-editor.component.html",
  styleUrls: ["./menu-current-values-line-editor.component.scss"],
  providers: [{ provide: BsDropdownConfig, useValue: { isAnimated: true, autoClose: true } }]

})
export class MenuCurrentValuesLineEditorComponent {

  subscription: Subscription;
  faAngleDown = faAngleDown;
  @ViewChild('labelLineType') el:ElementRef;
  @ViewChild('inputLineType') elp:ElementRef;
  @ViewChild('dropDownLineType') eld:ElementRef;
  private lineTypeCounter: number = 1;
  private counter: number = 1;
  constructor( private engServ: EngineService, public MS: MessageService, private gService: ScdFileService, private toastr: ToastrService ) { 
    
    this.subscription = this.MS.getMessage().subscribe((message) => {

      //last building structure delte in project informaion page,
      //select building combo to 0 index
      if(message == "PROJECT_INFORMATION_DELETE_LAST_BUILDING_CLICK") {
        this.buildingsGroup_changeHandler('Structure:0');
      }

    });
    
    this.getAllLineType();
  }

  

  getAllLineType(type="roofing") {


    let productType = this.MS.scope_queue.GRID_TAB;

    if(productType == "roofinggutter") {
      productType = "roofing";
    }

    let url = "Linetypes/" + productType;
      this.gService.getAll(url).subscribe((data: any) => {
        console.log("LINE_TYPE_DATA > ", data);
        if(data.length > 0) {  

          for(let i=0; i < data.length; i++) {
     
            let obj:any = this.filter_lineType_response(productType, data[i]);

            if(!this.isEmptyObject(obj)) {

              let color:any = new Color( obj.COLOR.toLowerCase() );            
              color = '#' + color.getHexString();

              obj.SOLID = data[i].Construction;

              obj.COLOR = color;

              this.MS.LINE_TYPES.push(obj);             

            }

          }

          this.MS.menu_line_editor.LINE_TYPES_REF_SELECTED = this.MS.LINE_TYPES[0].LINE_TYPES_REF;

        }

      },
      (error: HttpErrorResponse) => {

        this.toastr.warning('LineType API not responding.' , error.name);
        let obj:any =  { LINE_TYPES_REF : 1, NAME : 'Eave', COLOR : '#ed1f26', SOLID : 0 };
        this.MS.LINE_TYPES.push(obj);

      });
    
  }

  filter_lineType_response (type, data) {

    let obj:any = {};
    let concreteIgnore: string[] = ['Area Label', 'Dimension', 'Eave'];
    let gutterIgnore: string[] = ['Area Label', 'Gutter', 'Base', 'Dimension', '1st Story', '2nd Story', '3rd Story', '4th Story'];
    let roofingIgnore: string[] = ['Area', 'Base', 'Roofing'];
    let insulationIgnore: string[] = ['Area Label', 'Insulation'];
    let paintIgnore: string[] = ['Eave', 'Step Flashing', 'Flat Drip Edge', 'Hip', 'Valley', 'Ridge', 'Headwall Flashing', 'Slope Change', 'Construction', 'Dimension', 'Area Label', 'Rake Edge', 'Area Label Low', 'Clerestory', 'Parapet', 'Guide Line', ''];
    let roofScopeIgnore: string[] = ['Dimension', 'Area Label', 'Area Label Low', 'Guide Line', 'Ground'];
    let sidingIgnore: string[] = ['Eave', 'Step Flashing', 'Flat Drip Edge', 'Hip', 'Valley', 'Ridge', 'Headwall Flashing', 'Slope Change', 'Dimension', 'Area Label', 'Rake Edge', 'Area Label Low', 'Clerestory', 'Parapet', 'Clerestory', 'Guide Line'];
    
    let isObject = false;
    

    if (type == 'concrete') {
      if(concreteIgnore.includes(data.Name)) {
        isObject = true;
      }
    } else
    if (type == 'gutter') {
      if(gutterIgnore.includes(data.Name)) {
        isObject = true;
      }
    } else
    if (type == 'insulation') {
      if(insulationIgnore.includes(data.Name)) {
        isObject = true;
      }
    } else
    if (type == 'paint') {
      if(paintIgnore.includes(data.Name)) {
        isObject = true;
      }
    } else
    if (type == 'roofscopex') {
      if(roofScopeIgnore.includes(data.Name)) {
        isObject = true;
      }
    } else
    if (type == 'siding') {
      if(sidingIgnore.includes(data.Name)) {
        isObject = true;
      }
    } else
    if (type == 'roofinggutter') {
      if(roofingIgnore.includes(data.Name)) {
        isObject = true;
      }
    } else 
    if(type == "roofing" || type == "Roofing") {
      if(roofingIgnore.includes(data.Name)) {
        isObject = true;
      }
    }

    if(isObject == false) { 
      const borderType  = this.lineTypeBorderType(data.Pattern);
      obj =  { LINE_TYPES_REF : data.Id, NAME : data.Name, COLOR : data.Color, SOLID : 0, BORDER_TYPE: borderType};
    }

    return obj;
  }

  lineTypeBorderType(lineTypePattern) {
    let borderType = "solid";
    if(lineTypePattern == '1.0,1.0') {
      borderType = "solid";
    } 
    else if(lineTypePattern == '2.0,2.0') {
      borderType = "dotted";
    } 
    else if(lineTypePattern == '3.0,3.0') {
      borderType = "dashed";
    }
    return borderType;
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  buildingsGroup_changeHandler( id:string ) {
    let index: number = parseInt(id.split(":")[1]);
    this.MS.menu_line_editor.BUILDING_GROUP_SELECTED = index;

    this.engServ.hideAllBuildingsGroups();
    this.engServ.showSelectedBuildingsGroup( index );
    this.MS.sendMessage("MENU_CURRENT_VALUES_BUILDINGS_GROUP_CHANGE")
  }
  
  lineType_clickHandler(index:number) {
    this.lineTypeCounter = 1;
    this.MS.menu_line_editor.LINE_TYPES_REF_SELECTED = index;
    this.MS.sendMessage("MENU_LINE_TYPE_CHANGED");
  }

  openDropDown () {
    this.lineTypeCounter++;
    if(this.lineTypeCounter == 4) {
      this.counter++;
      this.eld.nativeElement.click();
      if(this.counter == 2) {
        this.lineTypeCounter = 3;
        this.counter = 0;
      } else {
        this.lineTypeCounter = 1;
      }      
    }

    this.MS.sendMessage("CHANGE_SCROLL_WHILE_LINETYPE");
  }


  openDropDownHide () {
    this.MS.sendMessage("CHANGE_SCROLL_WHILE_LINETYPE_AUTO");
  }
  
  pitch_changeHandler(rise:number) {
    this.MS.menu_line_editor.RISE_SELECTED = rise;
    this.MS.sendMessage("MENU_PITCH_CHANGED");
  }

}
