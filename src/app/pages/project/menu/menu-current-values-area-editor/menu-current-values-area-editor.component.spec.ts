import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCurrentValuesAreaEditorComponent } from './menu-current-values-area-editor.component';

describe('MenuCurrentValuesAreaEditorComponent', () => {
  let component: MenuCurrentValuesAreaEditorComponent;
  let fixture: ComponentFixture<MenuCurrentValuesAreaEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuCurrentValuesAreaEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCurrentValuesAreaEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
