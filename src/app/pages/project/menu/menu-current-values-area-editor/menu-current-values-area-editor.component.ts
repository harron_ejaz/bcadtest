import { Component } from "@angular/core";
import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-menu-current-values-area-editor',
  templateUrl: './menu-current-values-area-editor.component.html',
  styleUrls: ['./menu-current-values-area-editor.component.scss']
})
export class MenuCurrentValuesAreaEditorComponent {

  constructor( private engServ: EngineService, public MS: MessageService ) {}

  buildingsGroup_changeHandler( id:string ) {

    let index: number = parseInt(id.split(":")[1]);
    this.MS.menu_line_editor.BUILDING_GROUP_SELECTED = index;

    this.engServ.hideAllBuildingsGroups();
    this.engServ.showSelectedBuildingsGroup( index );

    this.MS.sendMessage("POLYGON_DETECT_INIT");
    this.MS.sendMessage("SHOW_POLYGON_SVG");
  }

  showImage_clickHandler() {
    this.MS.menu_line_editor.SHOW_IMAGE = !this.MS.menu_line_editor.SHOW_IMAGE;
    if( this.MS.menu_line_editor.SHOW_IMAGE ) {
      this.MS.sendMessage( "SHOW_CANVAS_IMAGE" );
    }
    else {
      this.MS.sendMessage( "HIDE_CANVAS_IMAGE" );
    }
  }

  showDims_clickHandler() {
    this.MS.menu_line_editor.SHOW_DIMS = !this.MS.menu_line_editor.SHOW_DIMS;
    if( this.MS.menu_line_editor.SHOW_DIMS ) {
      this.MS.sendMessage( "ADD_ALL_DIMENSIONS" );
    }
    else {
      this.MS.sendMessage( "REMOVE_ALL_DIMENSIONS" );
    }
  }

  

}
