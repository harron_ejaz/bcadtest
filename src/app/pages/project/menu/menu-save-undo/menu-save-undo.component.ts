import { Component, ViewEncapsulation } from '@angular/core';
import { ObjectLoader, Group } from 'three';
import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu-save-undo',
  templateUrl: './menu-save-undo.component.html',
  styleUrls: ['./menu-save-undo.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuSaveUndoComponent {

  private engGroup;
  private groupJSON:JSON;

  subscription: Subscription;
  
  constructor(private engServ: EngineService, private MS:MessageService) {
    // subscribe to component messages
    this.subscription = this.MS.getMessage().subscribe(message => {
      //Polygon break confirm dialog is clicked with No and Line Editor page is opened
      if(message == "HOTKEY_CTRLZ_UNDO_LINE_EDITOR") {
        this.undo_clickHandler();
      }
      else if(message == "HOTKEY_CTRLY_REDO_LINE_EDITOR") {
        this.redo_clickHandler();
      }
    });

  }

  undo_clickHandler() { 

    if( this.engServ.sessionStorageKeyIndex <= -1 ){
      return;
    }

    this.engServ.sessionStorageKeyIndex--;
 
    if( this.engServ.sessionStorageKeyIndex >= -1 ) {
      this.updateEngineGroup();
    }
    
  }

  redo_clickHandler() {
    if( this.engServ.sessionStorageKeyIndex < sessionStorage.length-1 ) {
      this.engServ.sessionStorageKeyIndex++;
      
      this.updateEngineGroup( );
      
    }
      
  }

  updateEngineGroup() {
    
    //remove all dimensions from DimensionsGroup
    //send message to subscribers via observable subject
    this.MS.sendMessage('REMOVE_ALL_DIMENSIONS');

    //remove all objects from scene group
    this.engServ.scene.remove(this.engServ.rootBuildingsGroup); 

    const loader = new ObjectLoader();
    let groupJSON:any;

    if( this.engServ.sessionStorageKeyIndex > -1 ) {
      //load json from session storage
      groupJSON = JSON.parse( sessionStorage.getItem( this.engServ.sessionStorageKeyIndex.toString() ) );
    }
    else if( this.engServ.sessionStorageKeyIndex == -1 ) {
      groupJSON = JSON.parse( this.engServ.defaultRootBuildingsGroup );
    }
    
    let engGroup:Group = loader.parse( groupJSON );
    this.engServ.rootBuildingsGroup = new Group();
    this.engServ.rootBuildingsGroup.copy( engGroup );
    this.engServ.scene.add( this.engServ.rootBuildingsGroup ); 

    if( this.engServ.sessionStorageKeyIndex == -1 ) {
      this.engServ.showSelectedBuildingsGroup( this.MS.menu_line_editor.BUILDING_GROUP_SELECTED );
    }
    

    //add all labels to LabelGroup
    // send message to subscribers via observable subject
    this.MS.sendMessage('ADD_ALL_DIMENSIONS');
  

  }

  saveAs_clickHandler() {
    this.MS.saveAndSubmit.isSaveAndSubmit = false;

    if( this.engServ.rootBuildingsGroup != undefined) {
      this.MS.engineGroupJSON = this.engServ.rootBuildingsGroup.toJSON();
      this.engServ.scene.remove(this.engServ.rootBuildingsGroup);
      
      console.log(this.MS.engineGroupJSON)   
      console.log("JSON saved in a variable"); 
    }
    else
      console.log("Canvas is not loaded"); 
  }

  save_clickHandler() {
    this.MS.saveAndSubmit.isSaveAndSubmit = false;
    this.MS.sendMessage("SAVE_SCD");
  }


}
