import { Component, ViewEncapsulation } from '@angular/core';
import { MessageService } from "src/app/services/message.service";
import { EngineService } from 'src/app/engine/engine.service';
import { LoadSCD } from 'src/app/tasks/loadSCD/load-scd';
import { ScdFileService } from 'src/app/services/scdfile.service';

@Component({
  selector: 'app-menu-report',
  templateUrl: './menu-report.component.html',
  styleUrls: ['./menu-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuReportComponent {

  private loadSCD: LoadSCD;

  constructor(public MS: MessageService, private engServ: EngineService, private gService: ScdFileService) { }

  saveAndSubmit_handler() {
    this.MS.saveAndSubmit.isSaveAndSubmit = true;
    
    this.MS.sendMessage("SAVE_SCD");
    
    this.MS.sendMessage("REPORT_SUBMIT");
  }


  buildingsGroup_changeHandler( id:string ) {
    let index: number = parseInt(id.split(":")[1]);
    this.MS.menu_line_editor.BUILDING_GROUP_SELECTED = index;

    this.engServ.hideAllBuildingsGroups();
    this.engServ.showSelectedBuildingsGroup( index );
    this.MS.sendMessage("MENU_CURRENT_VALUES_BUILDINGS_GROUP_CHANGE")
  }
}
