import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MenuComponent } from "./menu.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { CountdownModule } from 'ngx-countdown';

import { MenuGotoComponent } from "./menu-goto/menu-goto.component";
import { MenuCurrentValuesAreaEditorComponent } from "./menu-current-values-area-editor/menu-current-values-area-editor.component";
import { MenuReportComponent } from "./menu-report/menu-report.component";
import { MenuInsertComponent } from "./menu-insert/menu-insert.component";
import { MenuModifyComponent } from "./menu-modify/menu-modify.component";
import { MenuCurrentValuesLineEditorComponent } from "./menu-current-values-line-editor/menu-current-values-line-editor.component";
import { MenuProjectComponent } from "./menu-project/menu-project.component";
import { MenuTimerComponent } from "./menu-timer/menu-timer.component";
import { MenuImportReportComponent } from "./menu-import-report/menu-import-report.component";
import { MenuSelectImageComponent } from "./menu-select-image/menu-select-image.component";
import { MenuUndoRedoComponent } from "./menu-undo-redo/menu-undo-redo.component";
import { MenuSelectionTypesComponent } from "./menu-selection-types/menu-selection-types.component";
import { MenuAlignScaleComponent } from "./menu-align-scale/menu-align-scale.component";
import { MenuInputLengthComponent } from './menu-input-length/menu-input-length.component';
import { MenuZoomComponent } from "./menu-zoom/menu-zoom.component";

import { MenuSettingsLineEditorComponent } from './menu-settings-line-editor/menu-settings-line-editor.component';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { MenuSaveUndoComponent } from './menu-save-undo/menu-save-undo.component';
import { MenuClipboardComponent } from './menu-clipboard/menu-clipboard.component';
import { TwoDigitDecimaNumberDirectiveDirective } from './menu-input-length/two-digit-decima-number-directive.directive';
import { NgxMaskModule, IConfig } from 'ngx-mask'

@NgModule({
  declarations: [
    MenuComponent,
    MenuGotoComponent,
    MenuCurrentValuesAreaEditorComponent,
    MenuReportComponent,
    MenuInsertComponent,
    MenuModifyComponent,
    MenuCurrentValuesLineEditorComponent,
    MenuProjectComponent,
    MenuTimerComponent,
    MenuImportReportComponent,
    MenuSelectImageComponent,
    MenuAlignScaleComponent,
    MenuInputLengthComponent,
    MenuUndoRedoComponent,
    MenuSelectionTypesComponent,
    MenuZoomComponent,
    MenuSettingsLineEditorComponent,
    MenuSaveUndoComponent,
    MenuClipboardComponent,
    TwoDigitDecimaNumberDirectiveDirective,
    ],

  imports: [CommonModule, FontAwesomeModule, CountdownModule, PopoverModule.forRoot(), NgxMaskModule.forRoot()],
  exports: [MenuComponent],
})
export class MenuModule {}
