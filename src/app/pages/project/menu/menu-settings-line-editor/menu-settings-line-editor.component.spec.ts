import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSettingsLineEditorComponent } from './menu-settings-line-editor.component';

describe('MenuSettingsLineEditorComponent', () => {
  let component: MenuSettingsLineEditorComponent;
  let fixture: ComponentFixture<MenuSettingsLineEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSettingsLineEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSettingsLineEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
