import { Component, ViewEncapsulation } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SettingsComponent } from '../../dialog/settings/settings.component';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';
import { ScdFileService } from 'src/app/services/scdfile.service';

@Component({
  selector: 'app-menu-settings-line-editor',
  templateUrl: './menu-settings-line-editor.component.html',
  styleUrls: ['./menu-settings-line-editor.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuSettingsLineEditorComponent {

  bsModalRef: BsModalRef;
  editDims_btn_enabled = false;

  subscription: Subscription;

  constructor(private modalService: BsModalService, public MS: MessageService, private gService: ScdFileService) { 

    this.subscription = this.MS.getMessage().subscribe((message) => {

      if (message == "DISABLE_ALL_MENU_BUTTONS") {
        this.falseAll();
      }

    });

  }

  showImage_clickHandler() {
    this.MS.menu_line_editor.SHOW_IMAGE = !this.MS.menu_line_editor.SHOW_IMAGE;
    if( this.MS.menu_line_editor.SHOW_IMAGE ) {
      this.MS.sendMessage( "SHOW_CANVAS_IMAGE" );
    }
    else {
      this.MS.sendMessage( "HIDE_CANVAS_IMAGE" );
    }
  }

  showDims_clickHandler() {
    this.MS.menu_line_editor.SHOW_DIMS = !this.MS.menu_line_editor.SHOW_DIMS;
    if( this.MS.menu_line_editor.SHOW_DIMS ) {
      this.MS.sendMessage( "ADD_ALL_DIMENSIONS" );
    }
    else {
      this.MS.sendMessage( "REMOVE_ALL_DIMENSIONS" );
    }
  }

  editDims_clickHandler() {
    if( this.editDims_btn_enabled ) {
      this.MS.sendMessage( "DISABLE_EDIT_DIMS" );
      this.MS.menu_line_editor.EDIT_DIMS = this.editDims_btn_enabled = false;
    }
    else {
      this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
      this.MS.menu_line_editor.EDIT_DIMS = this.editDims_btn_enabled = true;
    }
  }

  lengthSnap_inputHandler( value:string ) {
    this.MS.settings.GRID_SNAP_Length = parseFloat( value ) * 12; //convert feet to inches
    this.gService.saveUserSettings(this.MS.settings);
  }

  radialSnap_inputHandler( value:string ) {
    this.MS.settings.RADIAL_DEGREES = parseFloat( value );
    this.gService.saveUserSettings(this.MS.settings);
  }

  settings_clickHandler() {
    this.bsModalRef = this.modalService.show( SettingsComponent );
  }

  falseAll() {
    this.MS.menu_line_editor.EDIT_DIMS = this.editDims_btn_enabled = false;
  }

}
