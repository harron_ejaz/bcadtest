import { Screenshot } from 'src/app/engine/tasks/screenshot';
import { Component, ViewEncapsulation } from "@angular/core";
import { MessageService } from "src/app/services/message.service";
import { Subscription } from "rxjs";
import { EngineService } from 'src/app/engine/engine.service';
import { Vector2, Vector3, Shape, MeshBasicMaterial, Mesh, ShapeGeometry, BufferGeometry, LineBasicMaterial, Line } from 'three';


@Component({
  selector: "app-menu-modify",
  templateUrl: "./menu-modify.component.html",
  styleUrls: ["./menu-modify.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class MenuModifyComponent {

  move_btn_enabled = false;
  offset_btn_enabled = false;
  fillet_btn_enabled = false;
  stretch_btn_enabled = false;
  mirror_btn_enabled = false;
  extend_btn_enabled = false;
  rotate_btn_enabled = false;
  trim_btn_enabled = false;
  editText_btn_enabled = false;
  viewport_btn_enabled = false;

  subscription: Subscription;
  screenshot: Screenshot;

  constructor( private engServ: EngineService, private MS: MessageService ) {

    this.screenshot = new Screenshot( this.engServ, this.MS );
    
    this.subscription = this.MS.getMessage().subscribe((message) => {

        if (message == "DISABLE_ALL_MENU_BUTTONS") {
          this.falseAll();
    
        }
        else if(message == "MENU_MODIFY_VIEWPORT") {
          this.assign_viewport();
        } 
        else if(message == "HOTKEY_CTRLG_MOVE_LINE_EDITOR") {
          this.move_clickHandler();
        }

    });

  }


  move_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.sendMessage( "ENABLE_MENU_SELECTION_BUTTON" );
    this.MS.menu_modify.MOVE = this.move_btn_enabled = true;
  }

  offset_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.sendMessage( "ENABLE_MENU_SELECTION_BUTTON" );
    this.offset_btn_enabled = this.MS.menu_modify.OFFSET = true;
  }

  fillet_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.sendMessage( "ENABLE_MENU_SELECTION_BUTTON" );
    this.fillet_btn_enabled = this.MS.menu_modify.FILLET = true;
  }

  stretch_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.sendMessage( "ENABLE_MENU_SELECTION_BUTTON" );
    this.stretch_btn_enabled = this.MS.menu_modify.STRETCH = true;
  }

  mirror_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.sendMessage( "ENABLE_MENU_SELECTION_BUTTON" );
    this.mirror_btn_enabled = this.MS.menu_modify.MIRROR = true;
  }

  extend_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.sendMessage( "ENABLE_MENU_SELECTION_BUTTON" );
    this.extend_btn_enabled = this.MS.menu_modify.EXTEND = true;
  }

  toFront_clickHandler() {
    this.MS.sendMessage("TO_FRONT");
  }

  toBack_clickHandler() {
    this.MS.sendMessage("TO_BACK");
  }

  rotate_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.sendMessage( "ENABLE_MENU_SELECTION_BUTTON" );
    this.rotate_btn_enabled = this.MS.menu_modify.ROTATE = true; 
  }

  trim_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.MS.sendMessage( "ENABLE_MENU_SELECTION_BUTTON" );
    this.trim_btn_enabled = this.MS.menu_modify.TRIM = true;
  }

  editText_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    //this.MS.sendMessage("DISABLE_ALL_MENU_SELECTION_BUTTONS");
    this.editText_btn_enabled = true;
  }

  viewport_clickHandler() {
    this.MS.sendMessage( "DISABLE_ALL_MENU_BUTTONS" );
    this.viewport_btn_enabled = this.MS.menu_modify.VIEW_PORT = true;

    document.body.style.cursor = "default";
    this.engServ.selectionHelper.element.className = "selectionBox";
  }

  assign_viewport() {

    this.clear_viewport();

    const topLeft:Vector2 = this.engServ.selectionHelper.pointTopLeft;
    const bottomRight:Vector2 = this.engServ.selectionHelper.pointBottomRight;

    const relativeTopLeft:Vector3 = this.engServ.get3DPosition( topLeft.x, topLeft.y );
    const relativeBottomRight:Vector3 = this.engServ.get3DPosition( bottomRight.x, bottomRight.y );

    const points = [];
    points.push( relativeTopLeft );
    points.push( new Vector3( relativeBottomRight.x, relativeTopLeft.y, 0 ) );
    points.push( relativeBottomRight );
    points.push( new Vector3( relativeTopLeft.x, relativeBottomRight.y, 0 ) );
    points.push( relativeTopLeft );

    const geometry = new BufferGeometry().setFromPoints( points );

    const material = new LineBasicMaterial( { color: this.MS.color.VIEWPORT } );

    let viewport = new Line( geometry, material );
    viewport.renderOrder = 1;
    viewport.name = "Viewport";
    let buildingsGroup:any = this.engServ.getSelectedBuildingsGroup();
    buildingsGroup.add( viewport );

    let scope = this;
    setTimeout(function() { scope.screenshot.init( topLeft, bottomRight ); }, 500);

  }

  clear_viewport() {
    let buildingsGroup:any = this.engServ.getSelectedBuildingsGroup();
    buildingsGroup.remove( buildingsGroup.getObjectByName( "Viewport" ) );
  }

  falseAll() {
    this.move_btn_enabled = this.MS.menu_modify.MOVE = false;
    this.offset_btn_enabled = this.MS.menu_modify.OFFSET = false;
    this.fillet_btn_enabled = this.MS.menu_modify.FILLET = false;
    this.stretch_btn_enabled = this.MS.menu_modify.STRETCH = false;
    this.mirror_btn_enabled = this.MS.menu_modify.MIRROR = false;
    this.extend_btn_enabled = this.MS.menu_modify.EXTEND = false;
    this.rotate_btn_enabled = this.MS.menu_modify.ROTATE = false;
    this.trim_btn_enabled = this.MS.menu_modify.TRIM = false;
    this.editText_btn_enabled = this.MS.menu_modify.EDIT_TEXT = false;
    this.viewport_btn_enabled = this.MS.menu_modify.VIEW_PORT = false;
  }

}
