import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageAcquisitionComponent } from './image-acquisition.component';

describe('ImageAcquisitionComponent', () => {
  let component: ImageAcquisitionComponent;
  let fixture: ComponentFixture<ImageAcquisitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageAcquisitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageAcquisitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
