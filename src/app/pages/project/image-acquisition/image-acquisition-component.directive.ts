import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[image-acquisition-component-host]'
})
export class ImageAcquisitionComponentDirective {

  constructor( public viewContainerRef: ViewContainerRef ) { }

}
