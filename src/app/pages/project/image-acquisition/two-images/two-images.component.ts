import { Component, OnInit } from '@angular/core';
import { faImage } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-two-images',
  templateUrl: './two-images.component.html',
  styleUrls: ['./two-images.component.css']
})
export class TwoImagesComponent implements OnInit {

  faImage = faImage;
  westImageURL:any = "";
  eastImageURL:any = "";

  constructor() { }

  ngOnInit() {
  }

  westImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event:Event) => { // called once readAsDataURL is completed
        this.westImageURL = reader.result;
      }
    }
  }

  eastImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event:Event) => { // called once readAsDataURL is completed
        this.eastImageURL = reader.result;
      }
    }
  }

}
