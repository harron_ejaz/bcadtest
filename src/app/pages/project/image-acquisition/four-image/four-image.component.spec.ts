import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourImageComponent } from './four-image.component';

describe('FiveImagesComponent', () => {
  let component: FourImageComponent;
  let fixture: ComponentFixture<FourImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiveImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
