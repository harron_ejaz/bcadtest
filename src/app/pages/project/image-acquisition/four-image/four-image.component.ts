import { Component, NgZone } from "@angular/core";
import { MessageService } from "src/app/services/message.service";
import * as OpenSeadragon from 'openseadragon';
import { NgxFileDropEntry, FileSystemFileEntry } from 'ngx-file-drop';

@Component({
  selector: "app-four-image",
  templateUrl: "./four-image.component.html",
  styleUrls: ["./four-image.component.scss"],
})
export class FourImageComponent {

  northImageViewer:any;
  westImageViewer:any;
  topImageViewer:any;
  eastImageViewer:any;
  southImageViewer:any;

  constructor(public MS: MessageService, private ngZone: NgZone) {}

  ngOnInit() {
    
    this.northImageViewer = this.ngZone.runOutsideAngular(() =>
      OpenSeadragon({
        id: "northImageViewerFour",
        zoomInButton: "north-zoom-in",
        zoomOutButton: "north-zoom-out",
        showFullPageControl: false,
        showHomeControl: false,
        minZoomLevel: 0,
        maxZoomLevel: 10
      })
    );

    this.westImageViewer = this.ngZone.runOutsideAngular(() =>
      OpenSeadragon({
        id: "westImageViewerFour",
        zoomInButton: "west-zoom-in",
        zoomOutButton: "west-zoom-out",
        showFullPageControl: false,
        showHomeControl: false,
        minZoomLevel: 0,
        maxZoomLevel: 10
      })
    );

    this.topImageViewer = this.ngZone.runOutsideAngular(() =>
      OpenSeadragon({
        id: "topImageViewerFour",
        showNavigationControl: false,
        showFullPageControl: false,
        showHomeControl: false,
        minZoomLevel: 0,
        maxZoomLevel: 10
      })
    );

    this.eastImageViewer = this.ngZone.runOutsideAngular(() =>
      OpenSeadragon({
        id: "eastImageViewerFour",
        zoomInButton: "east-zoom-in",
        zoomOutButton: "east-zoom-out",
        showFullPageControl: false,
        showHomeControl: false,
        minZoomLevel: 0,
        maxZoomLevel: 10
      })
    );

    this.southImageViewer = this.ngZone.runOutsideAngular(() =>
      OpenSeadragon({
        id: "southImageViewerFour",
        zoomInButton: "south-zoom-in",
        zoomOutButton: "south-zoom-out",
        showFullPageControl: false,
        showHomeControl: false,
        minZoomLevel: 0,
        maxZoomLevel: 10
      })
    );


    
  }

  ngAfterViewInit() {

    this.northImageViewer.addSimpleImage({
      url: this.MS.imagery.NORTH_IMAGE_URL
    });

    this.westImageViewer.addSimpleImage({
      url: this.MS.imagery.WEST_IMAGE_URL
    });

    this.topImageViewer.addSimpleImage({
      url: this.MS.imagery.TOP_IMAGE_URL
    });

    this.eastImageViewer.addSimpleImage({
      url: this.MS.imagery.EAST_IMAGE_URL
    });

    this.southImageViewer.addSimpleImage({
      url: this.MS.imagery.SOUTH_IMAGE_URL
    });

    this.northImageViewer.panHorizontal = false;
    this.northImageViewer.panVertical = false;

    this.westImageViewer.panHorizontal = false;
    this.westImageViewer.panVertical = false;

    this.topImageViewer.panHorizontal = false;
    this.topImageViewer.panVertical = false;

    this.eastImageViewer.panHorizontal = false;
    this.eastImageViewer.panVertical = false;

    this.southImageViewer.panHorizontal = false;
    this.southImageViewer.panVertical = false;

  }

  public northImage_dropHandler(files: NgxFileDropEntry[]) {
    const droppedFile = files[0];
    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        // Here you can access the real file
        let FileList = new Array();
        FileList.push( file );
        let event = { target : { files : FileList } };
        this.northImage_changeHandler( event );
      });
    }
  }

 northImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader:any = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: Event) => {
        // called once readAsDataURL is completed
        this.MS.imagery.NORTH_IMAGE_URL = reader.result;

        this.northImageViewer.world.removeAll();

        this.northImageViewer.addSimpleImage({
          url: this.MS.imagery.NORTH_IMAGE_URL
        });
    
      };
    }
  }

  public westImage_dropHandler(files: NgxFileDropEntry[]) {
    const droppedFile = files[0];
    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        // Here you can access the real file
        let FileList = new Array();
        FileList.push( file );
        let event = { target : { files : FileList } };
        this.westImage_changeHandler( event );
      });
    }
  }

  westImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader:any = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: Event) => {
        // called once readAsDataURL is completed
        this.MS.imagery.WEST_IMAGE_URL = reader.result;

        this.westImageViewer.world.removeAll();

        this.westImageViewer.addSimpleImage({
          url: this.MS.imagery.WEST_IMAGE_URL
        });
        
      };
    }
  }

  public topImage_dropHandler(files: NgxFileDropEntry[]) {
    const droppedFile = files[0];
    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        // Here you can access the real file
        let FileList = new Array();
        FileList.push( file );
        let event = { target : { files : FileList } };
        this.topImage_changeHandler( event );
      });
    }
  }

  topImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader:any = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: Event) => {
        // called once readAsDataURL is completed
        this.MS.imagery.TOP_IMAGE_URL = reader.result;

        this.topImageViewer.world.removeAll();

        this.topImageViewer.addSimpleImage({
          url: this.MS.imagery.TOP_IMAGE_URL
        });
        
      };
    }
  }

  public eastImage_dropHandler(files: NgxFileDropEntry[]) {
    const droppedFile = files[0];
    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        // Here you can access the real file
        let FileList = new Array();
        FileList.push( file );
        let event = { target : { files : FileList } };
        this.eastImage_changeHandler( event );
      });
    }
  }

  eastImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader:any = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: Event) => {
        // called once readAsDataURL is completed
        this.MS.imagery.EAST_IMAGE_URL = reader.result;

        this.eastImageViewer.world.removeAll();

        this.eastImageViewer.addSimpleImage({
          url: this.MS.imagery.EAST_IMAGE_URL
        });
        
      };
    }
  }

  public southImage_dropHandler(files: NgxFileDropEntry[]) {
    const droppedFile = files[0];
    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        // Here you can access the real file
        let FileList = new Array();
        FileList.push( file );
        let event = { target : { files : FileList } };
        this.southImage_changeHandler( event );
      });
    }
  }

  southImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader:any = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: Event) => {
        // called once readAsDataURL is completed
        this.MS.imagery.SOUTH_IMAGE_URL = reader.result;

        this.southImageViewer.world.removeAll();

        this.southImageViewer.addSimpleImage({
          url: this.MS.imagery.SOUTH_IMAGE_URL
        });
        
      };
    }
  }

  pan_clickHandler(imageViewer:string) {

    let viewer:any;

    if(imageViewer == 'northImageViewer') {
      viewer = this.northImageViewer;
      if( this.MS.imagery.NORTH_IMAGE_URL == "assets/images/browseImage.jpg" ) {
        viewer.panHorizontal = viewer.panVertical = false;
        return;
      }
    }
      
    else if(imageViewer == 'westImageViewer') {
      viewer = this.westImageViewer;
      if( this.MS.imagery.WEST_IMAGE_URL == "assets/images/browseImage.jpg" ) {
        viewer.panHorizontal = viewer.panVertical = false;
        return;
      }
    }
      
    else if(imageViewer == 'eastImageViewer') {
      viewer = this.eastImageViewer;
      if( this.MS.imagery.EAST_IMAGE_URL == "assets/images/browseImage.jpg" ) {
        viewer.panHorizontal = viewer.panVertical = false;
        return;
      }
    }
      
    else if(imageViewer == 'southImageViewer') {
      viewer = this.southImageViewer;
      if( this.MS.imagery.SOUTH_IMAGE_URL == "assets/images/browseImage.jpg" ) {
        viewer.panHorizontal = viewer.panVertical = false;
        return;
      }
    }
      

    viewer.panHorizontal = !viewer.panHorizontal;
    viewer.panVertical = !viewer.panVertical;
  
  }

  delete_clickHandler(imageViewer:string) {

    let viewer:any;

    if(imageViewer == 'northImageViewer') {
      viewer = this.northImageViewer;
      this.MS.imagery.NORTH_IMAGE_URL = "assets/images/browseImage.jpg";
      viewer.world.removeAll();
      viewer.addSimpleImage({
        url: this.MS.imagery.NORTH_IMAGE_URL
      });
    } 
    else if(imageViewer == 'westImageViewer') {
      viewer = this.westImageViewer;
      this.MS.imagery.WEST_IMAGE_URL = "assets/images/browseImage.jpg";
      viewer.world.removeAll();
      viewer.addSimpleImage({
        url: this.MS.imagery.WEST_IMAGE_URL
      });
    }
    else if(imageViewer == 'topImageViewer') {
      viewer = this.topImageViewer;
      this.MS.imagery.TOP_IMAGE_URL = "assets/images/browseImage.jpg";
      viewer.world.removeAll();
      viewer.addSimpleImage({
        url: this.MS.imagery.TOP_IMAGE_URL
      });
    }
    else if(imageViewer == 'southImageViewer') {
      viewer = this.southImageViewer;
      this.MS.imagery.SOUTH_IMAGE_URL = "assets/images/browseImage.jpg";
      viewer.world.removeAll();
      viewer.addSimpleImage({
        url: this.MS.imagery.SOUTH_IMAGE_URL
      });
    }
    else if(imageViewer == 'eastImageViewer') {
      viewer = this.eastImageViewer;
      this.MS.imagery.EAST_IMAGE_URL = "assets/images/browseImage.jpg";
      viewer.world.removeAll();
      viewer.addSimpleImage({
        url: this.MS.imagery.EAST_IMAGE_URL
      });
    }
  
  }


}
