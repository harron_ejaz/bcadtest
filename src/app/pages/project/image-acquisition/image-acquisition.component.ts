import {
  Component,
  OnInit,
  ViewChild,
  ComponentFactoryResolver,
} from "@angular/core";
import { ImageAcquisitionComponentDirective } from "./image-acquisition-component.directive";
import { FourImageComponent } from "./four-image/four-image.component";
import { OneImageComponent } from "./one-image/one-image.component";
import { MessageService } from "src/app/services/message.service";
import { Subscription } from "rxjs";
import {
  trigger,
  transition,
  state,
  animate,
  style,
} from "@angular/animations";

@Component({
  selector: "app-image-acquisition",
  templateUrl: "./image-acquisition.component.html",
  styleUrls: ["./image-acquisition.component.css"],
  animations: [
    trigger("flyInOut", [
      state("in", style({ transform: "translateX(0)" })),
      transition("void => *", [
        style({ transform: "translateX(-100%)" }),
        animate(500),
      ]),
      transition("* => void", [
        animate(500, style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
})
export class ImageAcquisitionComponent implements OnInit {
  subscription: Subscription;
  componentFactory: any[] = new Array();
  componentRef: any[] = new Array();
  viewContainerRef: any;

  @ViewChild(ImageAcquisitionComponentDirective, { static: true })
  imageAcquisitionComponentHost: ImageAcquisitionComponentDirective;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private MS: MessageService
  ) {
    this.subscription = this.MS.getMessage().subscribe((message) => {
      if (message == "LOAD_SINGLE_IMAGE") {
        this.loadComponent(1, OneImageComponent);
      }
      else if (message == "LOAD_FOUR_IMAGE") { 
        this.loadComponent(0, FourImageComponent);
      }
    });
  }
  
  ngOnInit() {

    this.viewContainerRef = this.imageAcquisitionComponentHost.viewContainerRef;

    if( this.MS.menu_select_image.IS_SINGLE_OVERHEAD_IMAGE == true || this.MS.menu_select_image.IS_SINGLE_OVERHEAD_IMAGE == undefined )
      this.MS.sendMessage("LOAD_SINGLE_IMAGE");
    else if( this.MS.menu_select_image.IS_SINGLE_OVERHEAD_IMAGE == false )
      this.MS.sendMessage("LOAD_FOUR_IMAGE");  
      
  }

  loadComponent(id:number, component: any) {
    this.viewContainerRef.detach();

    if (this.componentFactory[id] == undefined) {
      this.componentFactory[id] = this.componentFactoryResolver.resolveComponentFactory( component );
      this.componentRef[id] = this.viewContainerRef.createComponent( this.componentFactory[id] );
    }
    else {
      this.viewContainerRef.insert(this.componentRef[id].hostView);
    }
    
  }

}
