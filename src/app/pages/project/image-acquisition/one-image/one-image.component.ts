import { Component, NgZone } from "@angular/core";
import { MessageService } from "src/app/services/message.service";
import * as OpenSeadragon from 'openseadragon';
import { NgxFileDropEntry, FileSystemFileEntry } from 'ngx-file-drop';


@Component({
  selector: "app-one-image",
  templateUrl: "./one-image.component.html",
  styleUrls: ["./one-image.component.scss"],
})
export class OneImageComponent {

  reportImageViewer:any;
  topImageViewer:any;

  constructor(public MS: MessageService, private ngZone: NgZone) {
  }

  ngOnInit() {
    
    this.reportImageViewer = this.ngZone.runOutsideAngular(() =>
      OpenSeadragon({
        id: "reportImageViewerOne",
        //prefixUrl:      "/openseadragon/images/",
        zoomInButton: "report-zoom-in",
        zoomOutButton: "report-zoom-out",
        showFullPageControl: false,
        showHomeControl: false,
        minZoomLevel: 0,
        maxZoomLevel: 10
        //showNavigator:  true
      })
    );

    this.topImageViewer = this.ngZone.runOutsideAngular(() =>
      OpenSeadragon({
        id: "topImageViewerOne",

        showNavigationControl: false,
        showFullPageControl: false,
        showHomeControl: false,
        minZoomLevel: 0,
        maxZoomLevel: 10
      })
    );

    
  }

  ngAfterViewInit() {
    this.reportImageViewer.addSimpleImage({
      url: this.MS.imagery.NORTH_IMAGE_URL
    });

    this.topImageViewer.addSimpleImage({
      url: this.MS.imagery.TOP_IMAGE_URL
    });

    this.reportImageViewer.panHorizontal = false;
    this.reportImageViewer.panVertical = false;

    this.topImageViewer.panHorizontal = false;
    this.topImageViewer.panVertical = false;
  }

  public reportImage_dropHandler(files: NgxFileDropEntry[]) {
    const droppedFile = files[0];
    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        // Here you can access the real file
        let FileList = new Array();
        FileList.push( file );
        let event = { target : { files : FileList } };
        this.reportImage_changeHandler( event );
      });
    }
  }

  reportImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader:any = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: Event) => {
        // called once readAsDataURL is completed
        this.MS.imagery.NORTH_IMAGE_URL = reader.result;

        this.reportImageViewer.world.removeAll();

        this.reportImageViewer.addSimpleImage({
          url: this.MS.imagery.NORTH_IMAGE_URL
        });

      };
    }
    
  }

  public topImage_dropHandler(files: NgxFileDropEntry[]) {
    const droppedFile = files[0];
    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        // Here you can access the real file
        let FileList = new Array();
        FileList.push( file );
        let event = { target : { files : FileList } };
        this.topImage_changeHandler( event );
      });
    }
  }

  topImage_changeHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader:any = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: Event) => {
        // called once readAsDataURL is completed
        this.MS.imagery.TOP_IMAGE_URL = reader.result;

        this.topImageViewer.world.removeAll();

        this.topImageViewer.addSimpleImage({
          url: this.MS.imagery.TOP_IMAGE_URL
        });

      };
    }
  }

  pan_clickHandler(imageViewer:string) {

    let viewer:any;

    if(imageViewer == 'reportImageViewer') {

      viewer = this.reportImageViewer;

      if( this.MS.imagery.NORTH_IMAGE_URL == "assets/images/browseImage.jpg" ) {
        viewer.panHorizontal = viewer.panVertical = false;
        return;
      }
      
    }      

    viewer.panHorizontal = !viewer.panHorizontal;
    viewer.panVertical = !viewer.panVertical;

  }

  delete_clickHandler(imageViewer:string) {

    let viewer:any;

    if(imageViewer == 'reportImageViewer'){
      viewer = this.reportImageViewer;
      this.MS.imagery.NORTH_IMAGE_URL = "assets/images/browseImage.jpg";
      viewer.world.removeAll();
      viewer.addSimpleImage({
        url: this.MS.imagery.NORTH_IMAGE_URL
      });
    }
    else if(imageViewer == 'topImageViewer') {
      viewer = this.topImageViewer;
      this.MS.imagery.TOP_IMAGE_URL = "assets/images/browseImage.jpg";
      viewer.world.removeAll();
      viewer.addSimpleImage({
        url: this.MS.imagery.TOP_IMAGE_URL
      });
    }
  
  }

  

}
