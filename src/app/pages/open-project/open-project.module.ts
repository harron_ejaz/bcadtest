import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { AgGridModule } from 'ag-grid-angular';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AgGridComponent } from './ag-grid/ag-grid.component';

import { OpenProjectRoutingModule } from './open-project-routing.module';
import { OpenProjectComponent } from './open-project.component';


@NgModule({
  declarations: [AgGridComponent, OpenProjectComponent],
  imports: [
    CommonModule,
    TabsModule.forRoot(),
    AgGridModule.withComponents([]),
    AccordionModule.forRoot(),
    FontAwesomeModule,
    OpenProjectRoutingModule,
  ]
})
export class OpenProjectModule { }
