import { ScdFileService } from './../../services/scdfile.service';
import { Component, ViewEncapsulation } from "@angular/core";
import { NewProjectComponent } from "./new-project/new-project.component";
import { Router } from "@angular/router";
import {
  trigger,
  transition,
  state,
  animate,
  style,
} from "@angular/animations";
import { ToastrService } from 'ngx-toastr';

import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from "rxjs";
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: "app-open-project",
  templateUrl: "./open-project.component.html",
  styleUrls: ["./open-project.component.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger("flyInOut", [
      state("in", style({ transform: "translateX(0)" })),
      transition("void => *", [
        style({ transform: "translateX(-100%)" }),
        animate(500),
      ]),
      transition("* => void", [
        animate(500, style({ transform: "translateX(100%)" })),
      ]),
    ]),
  ],
})
export class OpenProjectComponent {

  subscription: Subscription;
  bsModalRef: BsModalRef;
  projectsData = [{here:"dfs"}];
  
  projectsDataGrid = [];


  constructor(private router: Router, private modalService: BsModalService, 
    private gService: ScdFileService, private MS:MessageService, private toastr: ToastrService) {
      

      this.subscription = this.MS.getMessage().subscribe((message) => {
        if(message == "SCOPE_QUEUE_ROW_DOUBLE_CLICK") {
          this.openProject();
        }
        if(message == "NEW_PROJECT_REFRESH_API") {
          this.refreshProject();
        }

        if(message == "ADD_NEW_PROJECT") {
          this.refreshProject();
        }
      });

      
      this.init();

    }

    init() {
      this.loadConcreteProducts();
      this.loadGutterProducts();
      this.loadPaintProducts();
      this.loadRoofingProducts();
      this.loadRoofing_gutterProducts();
      this.loadRoofscopexProducts();
      this.loadRoofxProducts();
      this.loadSidingProducts();
      this.loadInsulationProducts();
    }  

  gridTab_changeHandler($event) {
    this.MS.scope_queue.GRID_TAB = $event.heading;
    this.MS.sendMessage("SCOPE_QUEUE_GRID_TAB_CHANGE");
  }

  search_changeHandler(value:any) {
    this.MS.scope_queue.SEARCH = value;
    this.MS.sendMessage("REFRESH_CELL");
    this.MS.sendMessage("SCOPE_QUEUE_SEARCH_CHANGE");
  }

  get_a_scope_clickHandler() {
    this.openProject();  
  }

  openProject() {
  
    //if a grid row is selected, then open project information page, else open new project popup modal
    if( this.MS.scope_queue.GRID_ROW_SELECTED ) {
      let grid_selected_row_id = this.MS.scope_queue.GRID_ROW_SELECTED_ID;
      let url = "Projects/" + grid_selected_row_id;   
      this.gService.getAll(url).subscribe((data: any) => {
    
          let url = "Projects/" + grid_selected_row_id;
          this.gService.getAll(url).subscribe((data: any) => {
          this.MS.newProjectObject.SCD_OBJ = data;
          this.MS.currentSCDProjectObject.SCD_DATA = data;
          

      	this.router.navigate(["project"]);
          console.log("AFTER > ", this.MS.newProjectObject.SCD_OBJ);
      },
      (error: HttpErrorResponse) => {
        //open project for testing and developement
        this.toastr.warning( 'SCD API not working.' , error.name);
        
      });
      
    }); 
  } else {
      return false;
  }
}

  new_project_clickHandler() {
      this.bsModalRef = this.modalService.show(NewProjectComponent);   
  }

  loadConcreteProducts() {
    let url = "Projects/Concrete";   
    this.gService.getAll(url).subscribe((data: any) => {
      this.updateProjectData(data);
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Concrete API not responding.' , error.name);
    });
  }

  loadRoofingProducts() {
    let url = "Projects/roofing";   
    this.gService.getAll(url).subscribe((data: any) => {
      this.updateProjectData(data);
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Roofing API not responding.' , error.name);
    });
  }

  loadGutterProducts() {
    let url = "Projects/gutter";   
    this.gService.getAll(url).subscribe((data: any) => {
      this.updateProjectData(data);
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Gutter API not responding.' , error.name);
    });
  }

  loadPaintProducts() {
    let url = "Projects/paint";   
    this.gService.getAll(url).subscribe((data: any) => {
      this.updateProjectData(data);
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Paint API not responding.' , error.name);
    });
  }

  loadSidingProducts() {
    let url = "Projects/siding";   
    this.gService.getAll(url).subscribe((data: any) => {
      this.updateProjectData(data);
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Siding API not responding.' , error.name);
    });
  }

  loadInsulationProducts() {
    let url = "Projects/insulation";   
    this.gService.getAll(url).subscribe((data: any) => {
      console.log("Insulation > ", data);
      this.updateProjectData(data);
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Insulation API not responding.' , error.name);
    });
  }

  loadRoofxProducts() {
    let url = "Projects/roofx";   
  }

  loadRoofscopexProducts() {
    let url = "Projects/roofscopex";   
    this.gService.getAll(url).subscribe((data: any) => {
      this.updateProjectData(data);
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Roofscopex API not responding.' , error.name);

    });
  }

  loadRoofing_gutterProducts() {
    let url = "Projects/roofinggutter";   
    this.gService.getAll(url).subscribe((data: any) => {
      this.updateProjectData(data);
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Roofinggutter API not responding.' , error.name);
    });
  }

  refreshProject() {
    this.projectsDataGrid = [];
    this.init();
  }

  updateProjectData(data) {
    
    if(data.length > 0) {
      for(let i=0; i < data.length; i++) {
        const obj = {
          DUE_DATE: data[i].DueDate,
          NAME: data[i].Name,
          ADDRESS: data[i].Address,
          DESCRIPTION: data[i].Description,
          STATUS: data[i].Status,
          REVISION: data[i].Revision,
          GOOGLE_IMAGE: data[i].GoogleImage,
          CLAIM_NUMBER: data[i].ClaimNumber,
          JOB_NUMBER: data[i].MasterRevisionRef,
          CREATED_ON: data[i].CreatedOn,
          UPDATED_ON: data[i].UpdatedOn,
          USERS_REF: data[i].UsersRef,
          PRODUCT_TYPE: data[i].ProductType,
          ID: data[i].Id,
          FILENAME: "filename",
          Type: "filename",
        }

        this.projectsDataGrid.push(obj);
      }
    }
    console.log("API_DATA_LENGTH > ", this.projectsDataGrid.length);
    
  }
  
  logout_clickHandler() {
    window.location.reload();
  }

}
