import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from "rxjs";
import { ScdFileService } from './../../../services/scdfile.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss']
})
export class NewProjectComponent {
  
  subscription: Subscription;

  projectName = "";
  address = "";
  description = "";
  dueDate = "";
  dateOfLoss = "";
  notes = "";
  messageType = "";
  message = "";

  constructor(public bsModalRef: BsModalRef, private MS : MessageService,  private gService: ScdFileService, private toastr: ToastrService) { }

  get_projectName(value) {
    this.projectName = value;
  }

  get_address(value) {
    this.address = value;
  }

  get_description(value) {
    this.description = value;
  }

  get_due_date(value) {
    console.log(value);
    this.dueDate = value;
  }

  get_date_of_loss(value) {
    this.dateOfLoss = value;
  }

  get_notes(value) {
    this.notes = value;
  }

  onSubmit() {

    let dueDate = document.getElementById("dueDate");
    console.log(dueDate);
    
/**
 * 
 * ROOFSCOPE = 1,
		GUTTERSCOPE = 4,
		PAINTSCOPE = 5,
		SIDINGSCOPE = 6,
		INSULATIONSCOPE = 7,
		CONCRETESCOPE = 10,
		PAINTPROSCOPE = 11,
		ROOFSCOPEX = 12;
 * 
 */

/**
 * ROOFSCOPE = 1,
		GUTTERSCOPE = 4,
		SIDINGSCOPE = 6,
		INSULATIONSCOPE = 7,
		CONCRETESCOPE = 10,
		PAINTPROSCOPE = 11,
		ROOFSCOPEX = 12,
        Roofscope+ = 17;

1 => Tracescope::ROOFSCOPE,
		2 => Tracescope::PAINTSCOPE,
		3 => Tracescope::PAINTPROSCOPE,
		4 => Tracescope::SIDINGSCOPE,
		5 => Tracescope::INSULATIONSCOPE,
		6 => Tracescope::CONCRETESCOPE,
		7 => Tracescope::GUTTERSCOPE,
		8 => Tracescope::ROOFSCOPEX,
        9 => Tracescope::HYBRIDROOFGUTTERSCOPE,

 */

  let apiUrl = "Projects/roofing";
  let ProductType = "1";

   if( this.MS.scope_queue.GRID_TAB == "All" ) {
    apiUrl = "Projects/roofing";
    ProductType = "1";
  } else if( this.MS.scope_queue.GRID_TAB == "Concrete" ) {
    apiUrl = "Projects/concrete";
    ProductType = "6";
  } else if( this.MS.scope_queue.GRID_TAB == "RoofX" ) {
    apiUrl = "Projects/roofscopex";
    ProductType = "8";
  }else if( this.MS.scope_queue.GRID_TAB == "Roof+" ) {
    apiUrl = "Projects/roofinggutter";
    ProductType = "9";
  }else if( this.MS.scope_queue.GRID_TAB == "Gutter" ) {
    apiUrl = "Projects/gutter";
    ProductType = "7";
  }else if( this.MS.scope_queue.GRID_TAB == "Siding" ) {
    apiUrl = "Projects/siding";
    ProductType = "4";
  }else if( this.MS.scope_queue.GRID_TAB == "Paint" ) {
    apiUrl = "Projects/paint";
    ProductType = "3";
  }else if( this.MS.scope_queue.GRID_TAB == "Insulation" ) {
    apiUrl = "Projects/insulation";
    ProductType = "5";
  } else if(this.MS.scope_queue.GRID_TAB == "Roofing") {
    apiUrl = "Projects/roofing";
    ProductType = "1";
  }

    let projectInformation = {
      MasterRevisionRef : '',
      ProductType : ProductType,
      Name : this.projectName,
      Address : this.address,
      SiteAddress: this.address,
      Description : this.description,
      DueDate : this.dueDate,
      DateOfLoss :this.dateOfLoss, 
      Notes : this.notes,
      Status: 1,
      IsCurrent : true,
      Id: "",
      Buildings: [{"Id":"605110ff6ce5556565","ProjectsRef":"","Name":"Structure 1"}]
    }

    this.gService.addNewProject(apiUrl, projectInformation).subscribe((data: any) => {
      let res = data.match(/saved/g);

      if(res.length > 0) {
        let newProjectID = data.replace("saved", "");
        newProjectID = newProjectID.replace("new", "");
        newProjectID = newProjectID.replace(" ", "");

        this.MS.sendMessage("NEW_PROJECT_REFRESH_API");

        this.messageType = "success";
        this.message = "Project Added Successfully! " + newProjectID;
        
        setTimeout(() => {
          this.bsModalRef.hide();
        }, 2000);
  
      } else {
        this.toastr.error( 'Project not added!' , "Error!");
      }
    },
    (error: HttpErrorResponse) => {
      this.toastr.warning( 'Project API not responding.' , error.name);
    });

    
  }
}
