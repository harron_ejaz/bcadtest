import { Component, Input, OnChanges,SimpleChanges, ViewEncapsulation } from "@angular/core";
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from "rxjs";

@Component({
  selector: "app-ag-grid",
  templateUrl: "./ag-grid.component.html",
  styleUrls: ["./ag-grid.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class AgGridComponent implements OnChanges  {
  @Input() public projectsDataGrid: any;
  @Input() public dataGridType: any;

  private gridApi;
  private gridColumnApi;

  public noRowsTemplate;

  columnDefs;
  private defaultColDef;
  private autoGroupColumnDef;
  private rowModelType;
  private serverSideStoreType;
  private rowGroupPanelShow;
  rowData: [{}];
  allRowData: any = [{}];
  subscription: Subscription;

  constructor( private MS:MessageService ) {
    this.subscription = this.MS.getMessage().subscribe((message) => {
      if(message == "REFRESH_CELL") {
        this.onRefreshCells();
      }
    });

  this.noRowsTemplate ="<span></span>";

    this.columnDefs = [
      { headerName: "Due Date", field: "DUE_DATE", sortable: true, width: 130, checkboxSelection: true,
        cellStyle: params => {
          if(this.MS.scope_queue.SEARCH !="") {
            const filterValueStr = params.value.toString();
            let val = filterValueStr.includes(this.MS.scope_queue.SEARCH);
            
            if(val === true) {
              return {color: 'red'};
            }
            if(val === false) {
              return {color: 'black'};  
            }
            return null;
          } {
            return {color: 'black'};  
          }
        }
      },
      { headerName: "Name", field: "NAME", sortable: true, width: 130 ,
        cellStyle: params => {

          if(this.MS.scope_queue.SEARCH !="") {
            const filterValueStr = params.value.toLowerCase()
            let valx = filterValueStr.includes(this.MS.scope_queue.SEARCH.toLowerCase());
            
            if(valx == true) {
              return {color: 'red'};
            }else if(valx == false) {
              return {color: 'black'};  
            }
          } {
            return {color: 'black'};  
          }
        }
      },
      { headerName: "Address", field: "ADDRESS", sortable: true, width: 170,
      cellStyle: params => {
        if(this.MS.scope_queue.SEARCH !="") {
          const filterValueStr = params.value.toLowerCase()
          let valxxx = filterValueStr.includes(this.MS.scope_queue.SEARCH.toLowerCase());
          
          if(valxxx == true) {
            return {color: 'red'};
          }else if(valxxx == false) {
            return { color: 'black' };  
          }
        } {
          return {color: 'black'};  
        }
      }
    },
      { headerName: "Description", field: "DESCRIPTION", sortable: true, width: 180 ,
      cellStyle: params => {
        if(this.MS.scope_queue.SEARCH !="") {
          const filterValueStr = params.value.toLowerCase()
          let valxx = filterValueStr.includes(this.MS.scope_queue.SEARCH.toLowerCase());
          
          if(valxx == true) {
            return {color: 'red'};
          }else if(valxx == false) {
            return { color: 'black' };  
          }
        } {
          return {color: 'black'};  
        }
      }
    },
      { headerName: "Status", field: "STATUS", sortable: true, width: 75 },
      { headerName: "Revision", field: "REVISION", sortable: true, width: 80 },
      { headerName: "Google Image", field: "GOOGLE_IMAGE", sortable: true, width: 110,
        cellRenderer: function (params) {
          return '<a href="'+(params.valueFormatted ? params.valueFormatted : params.value)+'" target="_blank">Click for Image</a>';
        },
      },
      { headerName: "Claim No", field: "CLAIM_NUMBER", sortable: true, width: 95},
      { headerName: "Job No", field: "JOB_NUMBER", sortable: true, width: 85},
      { headerName: "Created On", field: "CREATED_ON", sortable: true, width: 100},
      { headerName: "Updated On", field: "UPDATED_ON", sortable: true, width: 100},
      { headerName: "Users Ref", field: "USERS_REF", sortable: true, width: 80 },
      { headerName: "Product Type", field: "PRODUCT_TYPE", sortable: true, width: 120 },
      { headerName: "Id", field: "ID", sortable: true, width: 60 },
      { headerName: "Filename", field: "FILENAME", sortable: true, width: 90 },
      { headerName: "Type", field: "Type", sortable: true, width: 90 },
    ];
    this.rowData = this.projectsDataGrid;
    this.allRowData = this.projectsDataGrid;

    this.rowModelType = 'serverSide';
    this.serverSideStoreType = 'full';
    this.rowGroupPanelShow = 'always';

    console.log("dataGridType > ", this.dataGridType);

    this.subscription = this.MS.getMessage().subscribe((message) => {

      if( message == "SCOPE_QUEUE_GRID_TAB_CHANGE" ) {
        this.updateGridOnTabSelection(this.MS.scope_queue.GRID_TAB);
      }
      else if( message == "SCOPE_QUEUE_SEARCH_CHANGE" ) {
        console.log("this.search() Called!");
        this.search();
      }
      else if( message == "ADD_NEW_PROJECT" ) {
        this.onAddRow(this.MS.new_project);
      }

      
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    let change = changes['projectsDataGrid'];
    this.rowData = change.currentValue;
    this.allRowData = change.currentValue;
    setTimeout(() => {
      this.refreshStore(change.currentValue);  
      this.updateGridOnTabSelection(this.MS.scope_queue.GRID_TAB); 
    }, 5000);
  }

  onRefreshCells() {
    this.gridApi.redrawRows();
  }
  onAddRow(newProject) {
    this.gridApi.applyTransaction({ add: [newProject] });
  }

  updateGridOnTabSelection(tab:String) {
    let productType;
     
     if( tab == "All" || tab == "" ) {
      productType = "all";
      
      this.gridApi.setRowData([]);
      this.gridApi.updateRowData({
        add: this.rowData
      });
      return true;
    } else if( tab == "Concrete" ) {
      productType = "concrete";
    } else if( tab == "RoofX" ) {
      productType = "roofscopex";
    }else if( tab == "Roof+" ) {
      productType = "roofinggutter";
    }else if( tab == "Gutter" ) {
      productType = "gutter";
    }else if( tab == "Siding" ) {
      productType = "siding";
    }else if( tab == "Paint" ) {
      productType = "paint";
    }else if( tab == "Insulation" ) {
      productType = "insulation";
    } else if(tab == "Roofing") {
      productType = "roofing";
    }

    let updateRows : any = [];
    for( let i = 0; i < this.allRowData.length; i++ ) {
      let rowID = this.allRowData[i];
      if(productType == rowID.PRODUCT_TYPE) {
        updateRows.push(rowID);
      }
    }

    this.gridApi.setRowData([]);
    this.gridApi.updateRowData({
      add: updateRows
    });

  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
    var selectedRow = event.api.getSelectedRows();
    if(rowCount > 0) 
    {
      this.MS.scope_queue.GRID_ROW_SELECTED = true;
      this.MS.scope_queue.GRID_ROW_SELECTED_ID = selectedRow[0].JOB_NUMBER;
      this.MS.scope_queue.GRID_TAB = selectedRow[0].PRODUCT_TYPE;
    } else if(rowCount == 0) {
      this.MS.scope_queue.GRID_ROW_SELECTED = false;
      this.MS.scope_queue.GRID_ROW_SELECTED_ID = 0;
    }
  }

  onRowDoubleClicked(event) {
    this.MS.sendMessage("SCOPE_QUEUE_ROW_DOUBLE_CLICK");
  }

  refreshStore(rows) {
    let api = this.gridApi;

    api.setRowData([]);
    api.updateRowData({
      add: rows
    });

  }

  onUpdateSomeValues() {
    console.log("LHR");
  }

  onGridReady( params ) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi; 
  } 

  search() {
    this.gridApi.setQuickFilter( this.MS.scope_queue.SEARCH );
    this.gridApi.refreshCells();
  }
  
  
}
