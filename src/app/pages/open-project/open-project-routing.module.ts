import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OpenProjectComponent } from './open-project.component';

const routes: Routes = [
  {
    path: '',
    component: OpenProjectComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpenProjectRoutingModule { }
