import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TabsModule } from "ngx-bootstrap/tabs";
import { AccordionModule } from "ngx-bootstrap/accordion";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NewProjectComponent } from './open-project/new-project/new-project.component';
import { SettingsComponent } from './project/dialog/settings/settings.component';
import { AppRoutingModule } from '../app-routing.module';


import { BackButtonDisableModule } from 'angular-disable-browser-back-button';

@NgModule({
  declarations: [ NewProjectComponent, SettingsComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    AppRoutingModule,
    BackButtonDisableModule.forRoot({
      preserveScrollPosition: true
    })
  ],
  exports: [],
  entryComponents: [NewProjectComponent, SettingsComponent]
})
export class PagesModule { }
