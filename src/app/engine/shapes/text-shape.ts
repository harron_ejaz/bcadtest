import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from './../../services/function.service';
import { Subscription } from 'rxjs';
import { Vector3, FontLoader, TextGeometry, Material, Mesh, MeshPhongMaterial, MeshNormalMaterial, MeshBasicMaterial } from 'three';

export class TextShape {

    private text:any = "";
    private font:any;
    private textValue:string = "";

    subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {
    
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if (message == "REMOVE_TEXT") { 
                this.removeText();

            }
        })

        this.loadFont();
    
    }

    onClick( relative:Vector3 ) {
        this.addText(relative);
    }

    onMouseMove( relative:Vector3, e:any ) {
        if(this.text && this.textValue == this.MS.menu_line_editor.TEXT) {
            this.updateText(relative);
        } 
        else if(!this.text) {
            this.addText(relative);
        }     
        else if(this.textValue != this.MS.menu_line_editor.TEXT) {
            this.removeText();
            this.addText(relative);
        }    
        
    }

    removeText() {
        if(this.text) {
            this.engServ.removeFromScene( this.text );
            this.text = this.textValue = "";
        }
        
    }




    // update text
    updateText(relative:Vector3) { 
        this.text.position.copy( relative )

    }

    // add text
    addText(relative:Vector3){ 

        if( this.MS.menu_line_editor.TEXT == "" ) return;

        this.textValue = this.MS.menu_line_editor.TEXT;

        let geometry:TextGeometry = new TextGeometry( this.MS.menu_line_editor.TEXT, {
            font: this.font,
            size: 1
        } );

        geometry.computeBoundingBox();

        let lineType:any = this.FS.get_line_type_selected();

        let material:MeshBasicMaterial = new MeshBasicMaterial( { color : lineType.COLOR } );

        this.text = new Mesh( geometry, material );

        let LINE_TYPES_REF_SELECTED:number = this.MS.menu_line_editor.LINE_TYPES_REF_SELECTED;
        this.text.userData.LineTypesRef = LINE_TYPES_REF_SELECTED;

        this.text.name = "Text";

        this.text.position.copy( relative )
        this.text.userData.Selected = false;
        this.engServ.addToScene( this.text );

    }

    loadFont() {
        let scope = this;
        const loader = new FontLoader();
        loader.load( 'assets/fonts/helvetiker_regular.typeface.json', function ( font ) {
            scope.font = font;
        } );
    }

}
