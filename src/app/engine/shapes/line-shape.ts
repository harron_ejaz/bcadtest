import { BufferGeometry, BufferAttribute, Vector3 } from 'three';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from './../../services/function.service';
import { EngineService } from '../engine.service';
import { Subscription } from 'rxjs';
import { DimensionsShape } from './dimensions-shape';

export class LineShape {

    private line:any;
    private count:number = 0;
    private MAX_POINTS:number = 2;
    private positions:any;
    private gridSnapPoint:Vector3 = null;
    private geometry:BufferGeometry;

    subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape ) {
    
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if (message == "MENU_LINE_TYPE_CHANGED" || message == "MENU_PITCH_CHANGED") {

                //if line is already being drawn
                if( this.count > 0 ) {
                    this.engServ.update_line_shape( this.line );
                }

            }
        })
    
    }

    onClick( relative:Vector3 ) {

        // on first click add an extra point
        if( this.count === 0 ){
            this.geometry = this.lineGeometry();
            this.line = this.engServ.set_line_shape(this.geometry);
            this.line.name = 'Line';
            this.line.renderOrder = 1;
            this.engServ.addToScene( this.line );
            this.addPoint(relative);
        }
        this.addPoint(relative);  
 
        // when drawing of a line segment is completed 
        // then call this function recursively to start drawing a new line segment
        if(this.count > this.MAX_POINTS) {
            this.count = 0;
            //when intersection is on, the lines which have been drawn already should get intersects
            this.line.userData.Drawn = true;
            this.line.userData.Selected = false;
            //bounding sphere is set so that line can be selected using mouse
            this.line.geometry.computeBoundingSphere();
            
            this.engServ.updateSessionStorage();            

            //if Grid button is enabled in Line Editor page's footer
            if( this.MS.footer_lineEditor.RADIAL || this.MS.footer_lineEditor.GRID )
                this.onClick(this.gridSnapPoint);
            else
                this.onClick(relative);
        }  
    }

    onMouseMove( relative:Vector3, e:any ) {
        if( this.count !== 0 ){
            if (!relative) {
                return;
            }
            this.updateLine(relative);
            this.updateDimensionsOnDrawing();          
        }
        //if any line is already drawn, check if endpoint or midpoint is enabled to show line point square 
        //if any line already exists in group
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {

            if( this.MS.footer_lineEditor.INTERSECTION 
                || this.MS.footer_lineEditor.PERPENDICULAR || this.MS.footer_lineEditor.NEAREST 
                || this.MS.footer_lineEditor.END_POINT || this.MS.footer_lineEditor.MID_POINT ) {
                
                    this.FS.showLinePointSquare(e, this.line, this.engServ);

            }

        }
    }

    onStop(relative:Vector3) {

        //if line is already drawing, only then remove the current line
        if( this.count > 0 ) {

            this.dimensionsShape.remove(this.line);

            //remove last line which is showing with mouse move
            this.engServ.removeFromScene(this.line);

            this.count = 0;
            this.gridSnapPoint = null;
        }       

    }

    lineGeometry():BufferGeometry {
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();
        this.positions = new Float32Array(this.MAX_POINTS * 3);
        geometry.setAttribute('position', new BufferAttribute(this.positions, 3));
        return geometry;
    }

    // update line
    updateLine(relative:Vector3) {

        //if Grid or Radial button is enabled in Line Editor page's footer
        if( this.MS.footer_lineEditor.GRID || this.MS.footer_lineEditor.RADIAL ) {

            let lineLength:number;

            if( this.MS.footer_lineEditor.GRID )
                lineLength = this.FS.getGridSnapLineLength( this.positions[0], this.positions[1], relative.x, relative.y );
            else
                lineLength = this.FS.get_distance_between_points( this.positions[0], this.positions[1], relative.x, relative.y );

            let angleBetweenPoints:number = this.FS.get_angle_between_points( this.positions[0], this.positions[1], relative.x, relative.y )
            
            //if Radial button is enabled in Line Editor page's footer
            if( this.MS.footer_lineEditor.RADIAL )
                angleBetweenPoints = this.FS.get_angle_radial(angleBetweenPoints, this.MS.settings.RADIAL_DEGREES);
       
            this.gridSnapPoint = this.FS.get_point_from_angle_distance( this.positions[0], this.positions[1], angleBetweenPoints,  lineLength )
    
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                this.gridSnapPoint.copy( this.engServ.linePointSquare.position );
            }

            this.positions[this.count * 3 - 3] = this.gridSnapPoint.x;
            this.positions[this.count * 3 - 2] = this.gridSnapPoint.y;
            this.positions[this.count * 3 - 1] = this.gridSnapPoint.z;

        }
        //if no button is enabled, then draw line with exact mouse position
        else {

            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                relative.copy( this.engServ.linePointSquare.position );
            }

            this.positions[this.count * 3 - 3] = relative.x;
            this.positions[this.count * 3 - 2] = relative.y;
            this.positions[this.count * 3 - 1] = relative.z;
        }       

        this.line.geometry.attributes.position.needsUpdate = true;
        this.line.computeLineDistances(); 
    }

    // add point
    addPoint(relative:Vector3){

        //if line end point or mid point is enabled and line point square is visible
        //means mouse is over end point or midpoint
        //then add point will use line point square position
        if( this.engServ.linePointSquare.visible ){
            relative.copy( this.engServ.linePointSquare.position );
        }

        this.positions[this.count * 3 + 0] = relative.x;
        this.positions[this.count * 3 + 1] = relative.y;
        this.positions[this.count * 3 + 2] = relative.z;
  
        this.count++;
        this.line.geometry.setDrawRange(0, this.count);
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    updateDimensionsOnDrawing() {
        this.dimensionsShape.update( this.line, this.engServ );
    }    
    
}
