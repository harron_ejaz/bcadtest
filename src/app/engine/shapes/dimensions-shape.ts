import { Subscription } from 'rxjs';
import { Vector3 } from 'three';
import { CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer';
import { MessageService } from './../../services/message.service';
import { EngineService } from '../engine.service';
import { FunctionService } from 'src/app/services/function.service';

export class DimensionsShape {

    private subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe(message => {

            //input line length changes in measure and align page, 
            //or dragging an object in engine,
            //so change dimensionss if they are already drawn
            if(message == "INPUT_LENGTH_CHANGE") {
                this.removeAllLineDimensions();
                this.addLineDimensions();
            } 
            else if(message == "REMOVE_ALL_DIMENSIONS") {
                this.removeAllDimensions();
            }
            else if(message == "ADD_ALL_DIMENSIONS") {
                this.addLineDimensions();
            }

        }
    )}

    public removeAllDimensions() {
        //remove all dimensions from DimensionsGroup
        let dimensionsGroup = this.engServ.dimensionsGroup; 
        for (let i:number = 0; i < dimensionsGroup.children.length; i++) {
            let dimensions = dimensionsGroup.children[i];
            dimensionsGroup.remove( dimensions );
            i = -1;
        }
    }

    private removeAllGeometryDimensions() {
        //remove all geometry dimensions from DimensionsGroup
        let dimensionsGroup = this.engServ.dimensionsGroup; 
        for (let i:number = 0; i < dimensionsGroup.children.length; i++) {
            let dimensions = dimensionsGroup.children[i];
            if(dimensions.name == "Dimensions") {
                dimensionsGroup.remove( dimensions );
                i = -1;
            } 
        }
    }

    private removeAllLineDimensions() {
        //check if line exists
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {
            //remove all line dimensions from DimensionsGroup
            let dimensionsGroup = this.engServ.dimensionsGroup; 
            for (let i:number = 0; i < dimensionsGroup.children.length; i++) {
                let dimensions = dimensionsGroup.children[i];
                if( dimensions.name == 'Dimensions' ) {
                    dimensionsGroup.remove( dimensions );
                    i = -1;
                }
            }
        }
    }

    private addScaleDimension() {
        //if scale line exists then add its dimensions to DimensionsGroup
        try {
            const scale = this.engServ.commonGroup.getObjectByName('Scale');
            this.attachScaleDimension( scale );
        }
        catch(e) {}

    }

    private addLineDimensions() {
        //check if line exists
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {
            
            //loop through group and check relavant object and add its dimensions to DimensionsGroup
            const group = this.engServ.getSelectedGeometriesGroup();
            for (let i:number = 0; i < group.children.length; i++) {
                const object = group.children[i];

                this.update(object, this.engServ);

            }

        }
    } 

    private attachScaleDimension( line:any ) {
        const shapeArray = line.geometry.attributes.position.array;
        const shapeX1:number = shapeArray[0];
        const shapeY1:number = shapeArray[1];
        const shapeX2:number = shapeArray[3];
        const shapeY2:number = shapeArray[4];

        const midPoint:Vector3 = this.FS.get_mid_point( shapeX1, shapeY1, shapeX2, shapeY2 );
        let length:number = this.MS.input_scaleLength.inputLength;
        let text = this.FS.get_formatted_feet(length);

        let div = document.createElement( 'div' );
        div.className = 'badge badge-pill'
        div.textContent = text;
        div.style.marginTop = '-1em';
        div.style.marginLeft = '-1em';
        div.style.fontSize = '10px';
        div.style.color = 'white';
        div.style.backgroundColor = 'red';

		let dimensions = new CSS2DObject(div);
		dimensions.position.copy( midPoint );
        dimensions.name = 'ScaleDimension'
        let dimensionsGroup = this.engServ.dimensionsGroup; 
        dimensionsGroup.add( dimensions );

    }

    private attachLineDimensions( line:any ) {
        const inputScaleRatio:number = this.FS.get_input_scale_ratio( this.MS.input_scaleLength );

        const shapeArray = line.geometry.attributes.position.array;
        const x1:number = shapeArray[0];
        const y1:number = shapeArray[1];
        const x2:number = shapeArray[3];
        const y2:number = shapeArray[4];

        const shapeLength:number = this.FS.get_distance_between_points( x1,y1,x2,y2 );

        
        
        
        
        let position = this.FS.get_position_parallel_to_line( x1, y1, x2, y2 );
        //if line is moved using drag controls, then its new position is added to place its dimensions to new position
        position.addVectors( position, line.position );

        let angle:number = this.FS.get_angle_between_points( x1,y1,x2,y2 );
      
        let length:number = shapeLength / inputScaleRatio;
        let text = this.FS.get_formatted_feet(length);

        let wrapper:any = document.createElement('div');
        let div = document.createElement( 'div' );
        div.className = 'dimensions';
        div.textContent = text;
        div.style.transform = 'rotate(' + angle*-1 + 'deg)';
        wrapper.appendChild(div); // Add div to the wrapper that inherits transform from CSS2DObject

        let dimensions:any = new CSS2DObject( wrapper );
        dimensions.position.copy( position );

        dimensions.name = "Dimensions";
        //line userData.Dimensions is same as its dimensions id,
        //so while modify this line, only this line's dimensions are updated
        line.userData.Dimensions = dimensions.id

        this.engServ.dimensionsGroup.add( dimensions );  

    }

    //update dimensions while drawing
    update( object:any, engServ:EngineService ) {

        //remove already assigned last dimensions  
        let dimensionsGroup:any = engServ.dimensionsGroup; 
        
        
         this.remove(object);
        

        const inputScaleRatio:number = this.FS.get_input_scale_ratio( this.MS.input_scaleLength );
        let position:Vector3;
        let length:number;
        let angle:number;

        if( object.name == "Line" ) {

            const shapeArray = object.geometry.attributes.position.array;
            const shapeX1:number = shapeArray[0];
            const shapeY1:number = shapeArray[1];
            const shapeX2:number = shapeArray[3];
            const shapeY2:number = shapeArray[4];

            position = this.FS.get_position_parallel_to_line( shapeX1, shapeY1, shapeX2, shapeY2 );
            //if line is moved using drag controls, then its new position is added to place its dimensions to new position
            position.addVectors( position, object.position );

            angle = this.FS.get_angle_between_points( shapeX1, shapeY1, shapeX2, shapeY2 );

            let lineLength:number = this.FS.get_distance_between_points( shapeX1, shapeY1, shapeX2, shapeY2 );

            length = lineLength / inputScaleRatio;

            this.draw(length, angle, position, object, "LineRadius", dimensionsGroup);

        }
        else if( object.name == "Circle" ) {

            let line:any = object.getObjectByName("CircleRadius");
            const shapeArray = line.geometry.attributes.position.array;
            const shapeX1:number = shapeArray[0];
            const shapeY1:number = shapeArray[1];
            const shapeX2:number = shapeArray[3];
            const shapeY2:number = shapeArray[4];
            position = this.FS.get_position_parallel_to_line( shapeX1, shapeY1, shapeX2, shapeY2 );
            //if line is moved using drag controls, then its new position is added to place its dimensions to new position
            position.addVectors( position, object.position );
            angle = this.FS.get_angle_between_points( shapeX1, shapeY1, shapeX2, shapeY2 );
            let radius:number = object.geometry.boundingSphere.radius;

            length = radius / inputScaleRatio;

            this.draw(length, angle, position, object, "CircleRadius", dimensionsGroup);

        }
        else if( object.name == "Arc" ) {
            position = object.geometry.boundingSphere.center;
            //if line is moved using drag controls, then its new position is added to place its dimensions to new position
            position.addVectors( position, object.position );
            let radius:number = object.userData.radius;
            let angle:number = object.userData.angle;
            let arcLength:number = this.FS.get_arc_length(radius,angle);
            length = arcLength / inputScaleRatio;
            
            this.draw(length, 0, position, object, "ArcLength", dimensionsGroup);
        }
        else if( object.name == "Ellipse" ) {

            //remove already assigned last dimensions  
            let dimensionsGroup:any = engServ.dimensionsGroup; 
            dimensionsGroup.remove( dimensionsGroup.getObjectById( object.uuid ) );

            let line1:any = object.getObjectByName("EllipseRadius1");
            let line2:any = object.getObjectByName("EllipseRadius2");

            //line1
            let shapeArray = line1.geometry.attributes.position.array;
            let shapeX1:number = shapeArray[0];
            let shapeY1:number = shapeArray[1];
            let shapeX2:number = shapeArray[3];
            let shapeY2:number = shapeArray[4];
            position = this.FS.get_position_parallel_to_line( shapeX1, shapeY1, shapeX2, shapeY2 );
            //if line is moved using drag controls, then its new position is added to place its dimensions to new position
            position.addVectors( position, object.position );
            angle = this.FS.get_angle_between_points( shapeX1, shapeY1, shapeX2, shapeY2 );
            let radius:number = object.userData.radius.xRadius;

            length = radius / inputScaleRatio;

            this.draw(length, angle, position, object, "EllipseRadius1", dimensionsGroup);

            //line2
            shapeArray = line2.geometry.attributes.position.array;
            shapeX1 = shapeArray[0];
            shapeY1 = shapeArray[1];
            shapeX2 = shapeArray[3];
            shapeY2 = shapeArray[4];
            position = this.FS.get_position_parallel_to_line( shapeX1, shapeY1, shapeX2, shapeY2 );
            //if line is moved using drag controls, then its new position is added to place its dimensions to new position
            position.addVectors( position, object.position );
            angle = this.FS.get_angle_between_points( shapeX1, shapeY1, shapeX2, shapeY2 );
            radius = object.userData.radius.yRadius;

            length = radius / inputScaleRatio;

            this.draw(length, angle, position, object, "EllipseRadius2", dimensionsGroup);

        }
        else {
            return;
        }

         

    }

    draw(length:number, angle:number, position:any, object:any, line:string, dimensionsGroup:any) {
        let text:string;

        //if scale line is not drawn or scale line's length is zero
        if(length == Infinity) {
            text = "";
        }
        else {
            let slopeLength:number = this.FS.get_length_or_area_with_slope_factor( length, this.MS.menu_line_editor.RISE_SELECTED );
            text = this.FS.get_formatted_feet(slopeLength);
        }

        if( Math.abs(angle) < 90 ) {
            angle = angle * -1;
        }
        else {
            angle = 180 - angle;
        }

        let wrapper:any = document.createElement('div');
        let div = document.createElement( 'div' );
        div.className = 'dimensions';
        div.textContent = text;
        div.style.transform = 'rotate(' + angle + 'deg)';
        wrapper.appendChild(div); // Add div to the wrapper that inherits transform from CSS2DObject

        let dimensions:any = new CSS2DObject( wrapper );
        dimensions.position.copy( position );

        dimensions.name = "Dimensions";
      

        //line userData.Dimensions is same as its dimensions id,
        //so while modify this line, only this line's dimensions are updated
        if( line == "EllipseRadius1" ) {
            object.userData.Dimensions1 = dimensions.id;            
        }
        else if( line == "EllipseRadius2" ) {
            object.userData.Dimensions2 = dimensions.id;            
        }
        else {
            object.userData.Dimensions = dimensions.id;
        }
        
        dimensionsGroup.add( dimensions ); 
    }

    //remove already assigned last dimensions  
    remove( object:any ) {

        let dimensionsGroup = this.engServ.dimensionsGroup; 

        if( object.name == "Ellipse") {
            dimensionsGroup.remove( dimensionsGroup.getObjectById( object.userData.Dimensions1 ) );
            dimensionsGroup.remove( dimensionsGroup.getObjectById( object.userData.Dimensions2 ) );
        }
        else {
            dimensionsGroup.remove( dimensionsGroup.getObjectById( object.userData.Dimensions ) );
        }

    }

}
