import { EllipseCurve, LineDashedMaterial, BufferGeometry, Vector3, Line, BufferAttribute, LineBasicMaterial } from 'three';
import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from './../../services/function.service';
import { Subscription } from 'rxjs';
import { DimensionsShape } from './dimensions-shape';

export class CircleShape {

    private circle:any;
    private count:number = 0;
    private MAX_POINTS:number = 1;
    private centerPosition:Vector3;
    private gridSnapPoint:Vector3 = null;
    private line:any;
    private positions:any;

    subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape ) {
    
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if (message == "MENU_LINE_TYPE_CHANGED" || message == "MENU_PITCH_CHANGED") { 

                //if circle is already being drawn
                if( this.count > 0 ) {
                    this.engServ.update_line_shape( this.circle );
                }

            }
        })
    
    }

    onClick( relative:Vector3 ) {

        // on first click add a point
        if( this.count === 0 ){
            let geometry:BufferGeometry = this.circleGeometry( 0.1, relative );
            this.circle = this.engServ.set_line_shape(geometry);

            this.circle.name = 'Circle';
            this.engServ.addToScene(this.circle);

            geometry = this.lineGeometry();
            let material = new LineDashedMaterial({ 
                color: this.MS.color.RADIUS_LINE, 
                dashSize: 1,
                gapSize: 1,
            })
            this.line = new Line(geometry, material);
            this.line.name = 'CircleRadius';
            this.line.renderOrder = 1;
            this.circle.add( this.line );
            this.addLinePoint(relative);
        }
        this.addPoint(relative);
        this.updateLine(relative);

        // when drawing of circle is completed 
        if(this.count > this.MAX_POINTS) {
            this.count = 0;
            this.circle.userData.Selected = false;
            //bounding sphere is set so that line can be selected using mouse
            this.circle.geometry.computeBoundingSphere();

            this.engServ.updateSessionStorage();    
        }

    }

    onMouseMove( relative:Vector3, e:any ) {
        if( this.count !== 0 ){
            if (!relative) {
                return;
            }
            this.updateCircle(relative);   
            this.updateLine(relative);
            this.updateDimensionsOnDrawing();      
        }
        //if any line is already drawn, check if endpoint or midpoint is enabled to show line point square 
        //if any line alerady exists in group
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {

            if( this.MS.footer_lineEditor.INTERSECTION || this.MS.footer_lineEditor.NEAREST 
                || this.MS.footer_lineEditor.END_POINT || this.MS.footer_lineEditor.MID_POINT ) {
                
                    this.FS.showLinePointSquare(e, this.circle, this.engServ);

            }

        }
    }

    onStop( relative:Vector3 ) {

        //if circle is already drawing, only then remove the current circle
        if( this.count > 0 ) {

            this.dimensionsShape.remove(this.circle);

            //remove last circle which is showing with mouse move
            this.engServ.removeFromScene(this.circle);
            this.count = 0;

            
        }   

    }

    circleGeometry( radius:number, position:Vector3 ):BufferGeometry {    
        let curve:EllipseCurve = new EllipseCurve(
            position.x,  position.y,            // ax, aY
            radius, radius,           // xRadius, yRadius
            0,  2 * Math.PI,  // aStartAngle, aEndAngle
            false,            // aClockwise
            0                // aRotation
        );   
        const points = curve.getPoints( 50 );
        // geometry
        const geometry:BufferGeometry = new BufferGeometry().setFromPoints( points );  
        return geometry;
    }

    

    // update circle
    updateCircle(relative:Vector3) {

        let radius:number;

        //if Grid button is enabled in Line Editor page's footer
        if( this.MS.footer_lineEditor.GRID ) {

            let lineLength:number;
            let angleBetweenPoints:number;

            lineLength = this.FS.getGridSnapLineLength( this.centerPosition.x, this.centerPosition.y, relative.x, relative.y );
            angleBetweenPoints = this.FS.get_angle_between_points( this.centerPosition.x, this.centerPosition.y, relative.x, relative.y )     
            this.gridSnapPoint = this.FS.get_point_from_angle_distance( this.centerPosition.x, this.centerPosition.y, angleBetweenPoints,  lineLength )
    
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                this.gridSnapPoint.copy( this.engServ.linePointSquare.position );
            }

            //get the distance from circle center/position to mouse current position
            radius = this.gridSnapPoint.distanceTo( this.centerPosition );

            

        }
        //if no button is enabled, then draw line with exact mouse position
        else {

            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                relative.copy( this.engServ.linePointSquare.position );
            }

            //get the distance from circle center/position to mouse current position
            radius = relative.distanceTo( this.centerPosition );
            if( radius == 0 ) 
                radius = 0.1;

        }

        const geometry:BufferGeometry = this.circleGeometry( radius, this.centerPosition );
        //update circle
        this.circle.geometry.attributes.position.array = geometry.attributes.position.array;
        this.circle.geometry.attributes.position.needsUpdate = true;

        //bounding sphere is set so that line can be selected using mouse
        this.circle.geometry.computeBoundingSphere();
        this.circle.computeLineDistances();
    }

    // add point
    addPoint(relative:Vector3){
        //sets the center position of circle when click first
        if(this.count == 0) {
            this.centerPosition = relative;
        }
        this.count++;  
    }

    updateDimensionsOnDrawing() {
        this.dimensionsShape.update( this.circle, this.engServ );
    }   

    lineGeometry():BufferGeometry {
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();
        this.positions = new Float32Array(6);
        geometry.setAttribute('position', new BufferAttribute(this.positions, 3));
        return geometry;
    }

    // add point
    addLinePoint(relative:Vector3){
        this.positions[0] = relative.x;
        this.positions[1] = relative.y;
        this.positions[2] = relative.z;
        this.line.geometry.setDrawRange(0, 2);
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    // update line
    updateLine(relative:Vector3) {

        this.positions[3] = relative.x;
        this.positions[4] = relative.y;
        this.positions[5] = relative.z;   

        this.line.geometry.attributes.position.needsUpdate = true;
        this.line.computeLineDistances(); 
    }

}
