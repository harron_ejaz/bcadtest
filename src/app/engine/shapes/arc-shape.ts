import { ArcCurve, LineDashedMaterial, BufferAttribute, BufferGeometry, Vector3, Line, Path, CircleGeometry } from 'three';
import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from './../../services/function.service';
import { Subscription } from 'rxjs';
import { DimensionsShape } from './dimensions-shape';

export class ArcShape {

    private line:any;
    private arc:any;
    private count:number = 0;
    private MAX_POINTS:number = 3;
    private positions:any;
    private centerPosition:Vector3;
    private gridSnapPoint:Vector3 = null;
    private radius:number = null;
    private angle:number = null;

    subscription: Subscription;
    
    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape ) {
    
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if (message == "MENU_LINE_TYPE_CHANGED" || message == "MENU_PITCH_CHANGED") { 

                //if line is already being drawn
                if( this.count == 2 ) {
                    this.engServ.update_line_shape( this.line );
                }
                //if arc is already being drawn
                else if( this.count == 3 ) {
                    this.engServ.update_line_shape( this.arc );
                }

            }
        })
    
    }

    onClick( relative:Vector3 ) {

        // on first click add an extra point
        if( this.count === 0 ){
            const geometry:BufferGeometry = this.lineGeometry();
            this.line = this.engServ.set_line_shape(geometry);
            this.line.name = 'Arc';
            this.engServ.addToScene(this.line);
            this.addPoint(relative);
        }
        this.addPoint(relative); 
        // when drawing arc
        if( this.count === 3 ){
            //remove line
            this.engServ.removeFromScene(this.line);

            const geometry:BufferGeometry = this.arcGeometry( relative );
            this.arc = this.engServ.set_line_shape(geometry);
            this.arc.name = 'Arc';
            this.arc.userData.Selected = false;
            this.engServ.addToScene(this.arc);
        }
        // when drawing of arc is completed 
        else if(this.count > this.MAX_POINTS) {
            this.count = 0;
            //bounding sphere is set so that line can be selected using mouse
            this.arc.geometry.computeBoundingSphere();

            this.engServ.updateSessionStorage();  
            this.line = null;
            this.arc = null;
        }
        
    }

    onMouseMove( relative:Vector3, e:any ) {
        if( this.count !== 0 ){
            if (!relative) {
                return;
            }
            //update line when after first click
            if( this.count == 2 )
                this.updateLine(relative);
            //update arc when after second click    
            else if( this.count == 3 ) {
                this.updateArc(relative);    
                this.updateDimensionsOnDrawing();
            }
                
        }
        //if any line is already drawn, check if endpoint or midpoint is enabled to show line point square 
        //if any line alerady exists in group
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {

            if( this.MS.footer_lineEditor.INTERSECTION 
                || this.MS.footer_lineEditor.PERPENDICULAR || this.MS.footer_lineEditor.NEAREST 
                || this.MS.footer_lineEditor.END_POINT || this.MS.footer_lineEditor.MID_POINT ) {
                    
                    this.FS.showLinePointSquare(e, this.line, this.engServ);

            }

        }
        
    }

    onStop( relative:Vector3 ) {
        //if line or arc is already drawing, only then remove the current line and arc
        if( this.count > 0 ) {
            this.dimensionsShape.remove(this.arc);
            //remove last line and arc which is showing with mouse move
            this.engServ.removeFromScene(this.line);
            this.engServ.removeFromScene(this.arc);
            this.count = 0;
        }   
    }

    lineGeometry():BufferGeometry {
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();  
        this.positions = new Float32Array( (this.MAX_POINTS - 1) * 3 );
        geometry.setAttribute('position', new BufferAttribute(this.positions, 3));
        return geometry;        
    }

    arcGeometry( relative:Vector3 ):BufferGeometry {

        //get circle center from 3 points
        const circleCenter:Vector3 = this.FS.get_circle_center( this.positions[0], this.positions[1], this.positions[3], this.positions[4], relative.x, relative.y );
        //get circle radius
        const aRadius:number = this.FS.get_distance_between_points( circleCenter.x, circleCenter.y, relative.x, relative.y );
        //get start angle of the point where line starts
        const aStartAngle:number = this.FS.get_circle_point_angle( circleCenter.x, circleCenter.y, this.positions[3], this.positions[4] );
        //get end angle of the point where line ends
        const aEndAngle:number = this.FS.get_circle_point_angle( circleCenter.x, circleCenter.y, this.positions[0], this.positions[1] );
        //check relative is on what side on line is
        const aClockwise:boolean = this.FS.if_point_is_above_line( this.positions[0], this.positions[1], this.positions[3], this.positions[4], relative.x, relative.y );

        const curve:ArcCurve = new ArcCurve(
            circleCenter.x, circleCenter.y,        // ax, aY
            aRadius,           // aRadius
            aStartAngle,  aEndAngle,  // aStartAngle, aEndAngle
            aClockwise            // aClockwise
        );
        const points = curve.getPoints( 50 );

        this.radius = aRadius;
        let angle = this.FS.get_angle_between_two_lines(this.positions[0], this.positions[1], circleCenter.x, circleCenter.y, this.positions[3], this.positions[4]);

        angle = 360 - angle;
        
        this.angle = angle;
        
        // geometry
        const geometry:BufferGeometry = new BufferGeometry().setFromPoints( points );     
        return geometry;
    }

    // update line
    updateLine(relative:Vector3) {

        //if Grid or Radial button is enabled in Line Editor page's footer
        if( this.MS.footer_lineEditor.GRID || this.MS.footer_lineEditor.RADIAL ) {

            let lineLength:number;

            if( this.MS.footer_lineEditor.GRID )
                lineLength = this.FS.getGridSnapLineLength( this.positions[0], this.positions[1], relative.x, relative.y );
            else
                lineLength = this.FS.get_distance_between_points( this.positions[0], this.positions[1], relative.x, relative.y );

            let angleBetweenPoints:number = this.FS.get_angle_between_points( this.positions[0], this.positions[1], relative.x, relative.y )
            
            //if Radial button is enabled in Line Editor page's footer
            if( this.MS.footer_lineEditor.RADIAL )
                angleBetweenPoints = this.FS.get_angle_radial( angleBetweenPoints, this.MS.settings.RADIAL_DEGREES );
            
            this.gridSnapPoint = this.FS.get_point_from_angle_distance( this.positions[0], this.positions[1], angleBetweenPoints,  lineLength )
    
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                this.gridSnapPoint.copy( this.engServ.linePointSquare.position );
            }

            this.positions[this.count * 3 - 3] = this.gridSnapPoint.x;
            this.positions[this.count * 3 - 2] = this.gridSnapPoint.y;
            this.positions[this.count * 3 - 1] = this.gridSnapPoint.z;

        }
        //if no button is enabled, then draw line with exact mouse position
        else {

            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                relative.copy( this.engServ.linePointSquare.position );
            }

            this.positions[this.count * 3 - 3] = relative.x;
            this.positions[this.count * 3 - 2] = relative.y;
            this.positions[this.count * 3 - 1] = relative.z;
        }       

        this.line.geometry.attributes.position.needsUpdate = true;
        this.line.computeLineDistances();

    }

    // update arc
    updateArc(relative:Vector3) {
        //if Grid button is enabled in Line Editor page's footer
        if( this.MS.footer_lineEditor.GRID ) {

            let lineLength:number;
            let angleBetweenPoints:number;

            lineLength = this.FS.getGridSnapLineLength( this.centerPosition.x, this.centerPosition.y, relative.x, relative.y );
            angleBetweenPoints = this.FS.get_angle_between_points( this.centerPosition.x, this.centerPosition.y, relative.x, relative.y )     
            this.gridSnapPoint = this.FS.get_point_from_angle_distance( this.centerPosition.x, this.centerPosition.y, angleBetweenPoints, lineLength )
    
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                this.gridSnapPoint.copy( this.engServ.linePointSquare.position );
            }

            const geometry:BufferGeometry = this.arcGeometry( this.gridSnapPoint );
            this.arc.userData.radius = this.radius;
            this.arc.userData.angle = this.angle;
            //update arc
            this.arc.geometry.attributes.position.array = geometry.attributes.position.array;

        }
        //if no button is enabled, then draw line with exact mouse position
        else {

            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                relative.copy( this.engServ.linePointSquare.position );
            }

            const geometry:BufferGeometry = this.arcGeometry( relative );
            this.arc.userData.radius = this.radius;
            this.arc.userData.angle = this.angle;
            //update arc
            this.arc.geometry.attributes.position.array = geometry.attributes.position.array;

        }     
        
        this.arc.geometry.attributes.position.needsUpdate = true;
        this.arc.computeLineDistances();

    }

    // add point
    addPoint(relative:Vector3){

        //if line end point or mid point is enabled and line point square is visible
        //means mouse is over end point or midpoint
        //then add point will use line point square position
        if( this.engServ.linePointSquare.visible ){
            relative.copy( this.engServ.linePointSquare.position );
        }

        //if drawing line
        if( this.count < this.MAX_POINTS ) {
            this.positions[this.count * 3 + 0] = relative.x;
            this.positions[this.count * 3 + 1] = relative.y;
            this.positions[this.count * 3 + 2] = relative.z;
        }

        this.count++;

        //if drawing line
        if( this.count <= this.MAX_POINTS ) {
            this.line.geometry.setDrawRange(0, this.count);
            this.line.geometry.attributes.position.needsUpdate = true;
        }

        //sets the center position of ellipse when click second
        if(this.count == this.MAX_POINTS) {

            //get midpoint of line
            this.centerPosition = this.FS.get_mid_point( this.positions[0], this.positions[1], this.positions[3], this.positions[4] );

        }
        
    }

    
    updateDimensionsOnDrawing() {
        this.dimensionsShape.update( this.arc, this.engServ );
    }   


}
