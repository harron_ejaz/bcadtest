import { BufferGeometry, BufferAttribute, LineDashedMaterial, Line, Vector3 } from 'three';
import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from './../../services/function.service';
import { Subscription } from 'rxjs';
import { DimensionsShape } from './dimensions-shape';

export class RectangleShape {

    private lineV1:any; //Vertical Line 1
    private lineV2:any; //Vertical Line 2
    private lineH1:any; //Horizontal Line 1
    private lineH2:any; //Horizontal Line 2
    private count:number = 0;
    private MAX_POINTS:number = 2;
    private positionsV1:any;
    private positionsV2:any;
    private positionsH1:any;
    private positionsH2:any;
    private startPosition:Vector3;
    private gridSnapPointH:Vector3 = null;
    private gridSnapPointV:Vector3 = null;
    
    subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape ) {
    
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if (message == "MENU_LINE_TYPE_CHANGED" || message == "MENU_PITCH_CHANGED") { 

                //if line is already being drawn
                if( this.count > 0 ) {
                    this.engServ.update_line_shape( this.lineV1 );
                    this.engServ.update_line_shape( this.lineV2 );
                    this.engServ.update_line_shape( this.lineH1 );
                    this.engServ.update_line_shape( this.lineH2 );
                }

            }
        })
    
    }

    onClick( relative:Vector3 ) {

        // on first click add an extra point
        if( this.count === 0 ){

            let geometry:BufferGeometry;

            geometry = this.lineGeometry( 'V1' );
            this.lineV1 = this.engServ.set_line_shape(geometry);

            geometry = this.lineGeometry( 'V2' );
            this.lineV2 = this.engServ.set_line_shape(geometry);

            geometry = this.lineGeometry( 'H1' );
            this.lineH1 = this.engServ.set_line_shape(geometry);

            geometry = this.lineGeometry( 'H2' );
            this.lineH2 = this.engServ.set_line_shape(geometry);

            this.lineV1.name = 'Line';
            this.lineV2.name = 'Line';
            this.lineH1.name = 'Line';
            this.lineH2.name = 'Line';

            this.lineV1.renderOrder = 1;
            this.lineV2.renderOrder = 1;
            this.lineH1.renderOrder = 1;
            this.lineH2.renderOrder = 1;

            this.engServ.addToScene(this.lineV1);
            this.engServ.addToScene(this.lineV2);
            this.engServ.addToScene(this.lineH1);
            this.engServ.addToScene(this.lineH2);

            this.addPoint(relative);
            this.startPosition = relative;
    
        }
        this.addPoint(relative);  

        // when drawing of a rectangle is completed, reset so that new rectangle can be drawn 
        if(this.count > this.MAX_POINTS) {
            this.count = 0;
            //when intersection is on, the lines which have been drawn already should get intersects
            this.lineV1.userData.Drawn = true;
            this.lineV2.userData.Drawn = true;
            this.lineH1.userData.Drawn = true;
            this.lineH2.userData.Drawn = true;
            this.lineV1.userData.Selected = false;
            this.lineV2.userData.Selected = false;
            this.lineH1.userData.Selected = false;
            this.lineH2.userData.Selected = false;
            //bounding sphere is set so that line can be selected using mouse
            this.lineV1.geometry.computeBoundingSphere();
            this.lineV2.geometry.computeBoundingSphere();
            this.lineH1.geometry.computeBoundingSphere();
            this.lineH2.geometry.computeBoundingSphere();

            this.engServ.updateSessionStorage();          
        }

    }

    onMouseMove( relative:Vector3, e:any ) {
        if( this.count !== 0 ){
            if (!relative) {
                return;
            }
            this.updateLine(relative);
            this.updateDimensionsOnDrawing();        
        }
        //if any line is already drawn, check if endpoint or midpoint is enabled to show line point square 
        //if any line alerady exists in group
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {

            if( this.MS.footer_lineEditor.INTERSECTION || this.MS.footer_lineEditor.NEAREST 
                || this.MS.footer_lineEditor.END_POINT || this.MS.footer_lineEditor.MID_POINT 
                || this.MS.footer_lineEditor.PERPENDICULAR ) {
                
                    this.FS.showLinePointSquare(e, this.lineH2, this.engServ);

                    //if perpendicular button is enabled, then check verticle line also
                    if( this.MS.footer_lineEditor.PERPENDICULAR && !this.engServ.linePointSquare.visible) {
                           this.FS.showLinePointSquare(e, this.lineV2, this.engServ);
                    }

            }
            

        }
    }

    onStop( relative:Vector3 ) {

        //if rectangle is already drawing, only then remove the current rectangle
        if( this.count > 0 ) {

            //remove already assigned last 4 dimensions  
            this.dimensionsShape.remove(this.lineV1);
            this.dimensionsShape.remove(this.lineV2);
            this.dimensionsShape.remove(this.lineH1);
            this.dimensionsShape.remove(this.lineH2);

            //remove last 4 lines which is showing with mouse move
            this.engServ.removeFromScene(this.lineV1);
            this.engServ.removeFromScene(this.lineV2);
            this.engServ.removeFromScene(this.lineH1);
            this.engServ.removeFromScene(this.lineH2);           

            this.count = 0;
        }     

    }

    lineGeometry(side:string):BufferGeometry {
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();
       
        if( side == 'V1') {
            this.positionsV1 = new Float32Array(this.MAX_POINTS * 3);
            geometry.setAttribute('position', new BufferAttribute(this.positionsV1, 3));
        }
        else if( side == 'V2') {
            this.positionsV2 = new Float32Array(this.MAX_POINTS * 3);
            geometry.setAttribute('position', new BufferAttribute(this.positionsV2, 3));
        }
        else if( side == 'H1') {
            this.positionsH1 = new Float32Array(this.MAX_POINTS * 3);
            geometry.setAttribute('position', new BufferAttribute(this.positionsH1, 3));
        }
        else if( side == 'H2') {
            this.positionsH2 = new Float32Array(this.MAX_POINTS * 3);
            geometry.setAttribute('position', new BufferAttribute(this.positionsH2, 3));
        }     
        
        return geometry;

    }

    // update line
    updateLine(relative:Vector3) {

        //if Grid button is enabled in Line Editor page's footer
        if( this.MS.footer_lineEditor.GRID ) {
            
            let lineLength:number;
            let angleBetweenPoints:number

            //for horizontal lines
            lineLength = this.FS.getGridSnapLineLength( this.positionsH1[0], this.positionsH1[1], relative.x, this.startPosition.y );
            angleBetweenPoints = this.FS.get_angle_between_points( this.positionsH1[0], this.positionsH1[1], relative.x, this.startPosition.y );
            angleBetweenPoints = Math.round(angleBetweenPoints);
            this.gridSnapPointH = this.FS.get_point_from_angle_distance( this.positionsH1[0], this.positionsH1[1], angleBetweenPoints,  lineLength );

            //for verticle lines
            lineLength = this.FS.getGridSnapLineLength( this.positionsV1[0], this.positionsV1[1], this.startPosition.x, relative.y );
            angleBetweenPoints = this.FS.get_angle_between_points( this.positionsV1[0], this.positionsV1[1], this.startPosition.x, relative.y );
            angleBetweenPoints = Math.round(angleBetweenPoints);
            this.gridSnapPointV = this.FS.get_point_from_angle_distance( this.positionsV1[0], this.positionsV1[1], angleBetweenPoints,  lineLength );

            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                this.gridSnapPointH.x =  this.engServ.linePointSquare.position.x;
                this.gridSnapPointV.y =  this.engServ.linePointSquare.position.y;
            }

            //2nd position
            this.positionsH1[this.count * 3 - 3] = this.gridSnapPointH.x;
            this.positionsH1[this.count * 3 - 2] = this.gridSnapPointH.y;
            this.positionsH1[this.count * 3 - 1] = this.gridSnapPointH.z;
            //2nd position
            this.positionsV1[this.count * 3 - 3] = this.gridSnapPointV.x;
            this.positionsV1[this.count * 3 - 2] = this.gridSnapPointV.y;
            this.positionsV1[this.count * 3 - 1] = this.gridSnapPointV.z;

            //1st position
            this.positionsH2[(this.count - 1) * 3 - 3] = this.gridSnapPointV.x;
            this.positionsH2[(this.count - 1) * 3 - 2] = this.gridSnapPointV.y;
            this.positionsH2[(this.count - 1) * 3 - 1] = this.gridSnapPointV.z;
            //2nd position
            this.positionsH2[this.count * 3 - 3] = this.gridSnapPointH.x;
            this.positionsH2[this.count * 3 - 2] = this.gridSnapPointV.y;
            this.positionsH2[this.count * 3 - 1] = this.gridSnapPointH.z; 

            //1st position
            this.positionsV2[(this.count - 1) * 3 - 3] = this.gridSnapPointH.x;
            this.positionsV2[(this.count - 1) * 3 - 2] = this.gridSnapPointH.y;
            this.positionsV2[(this.count - 1) * 3 - 1] = this.gridSnapPointH.z;
            //2nd position
            this.positionsV2[this.count * 3 - 3] = this.gridSnapPointH.x;
            this.positionsV2[this.count * 3 - 2] = this.gridSnapPointV.y;
            this.positionsV2[this.count * 3 - 1] = this.gridSnapPointV.z; 

        }
        //if no button is enabled, then draw rectangle with exact mouse position
        else {

            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                relative.copy( this.engServ.linePointSquare.position );
            }

            //2nd position
            this.positionsH1[this.count * 3 - 3] = relative.x;
            this.positionsH1[this.count * 3 - 2] = this.startPosition.y;
            this.positionsH1[this.count * 3 - 1] = relative.z;
            //2nd position
            this.positionsV1[this.count * 3 - 3] = this.startPosition.x;
            this.positionsV1[this.count * 3 - 2] = relative.y;
            this.positionsV1[this.count * 3 - 1] = relative.z;

            //1st position
            this.positionsH2[(this.count - 1) * 3 - 3] = this.startPosition.x;
            this.positionsH2[(this.count - 1) * 3 - 2] = relative.y;
            this.positionsH2[(this.count - 1) * 3 - 1] = relative.z;
            //2nd position
            this.positionsH2[this.count * 3 - 3] = relative.x;
            this.positionsH2[this.count * 3 - 2] = relative.y;
            this.positionsH2[this.count * 3 - 1] = relative.z;

            //1st position
            this.positionsV2[(this.count - 1) * 3 - 3] = relative.x;
            this.positionsV2[(this.count - 1) * 3 - 2] = this.startPosition.y;
            this.positionsV2[(this.count - 1) * 3 - 1] = relative.z;
            //2nd position
            this.positionsV2[this.count * 3 - 3] = relative.x;
            this.positionsV2[this.count * 3 - 2] = relative.y;
            this.positionsV2[this.count * 3 - 1] = relative.z;
            
        }       

        this.lineV1.geometry.attributes.position.needsUpdate = true;
        this.lineV2.geometry.attributes.position.needsUpdate = true;
        this.lineH1.geometry.attributes.position.needsUpdate = true;
        this.lineH2.geometry.attributes.position.needsUpdate = true;

        this.lineV1.computeLineDistances();
        this.lineV2.computeLineDistances();
        this.lineH1.computeLineDistances();
        this.lineH2.computeLineDistances();
        
    }

    // add point
    addPoint(relative:Vector3){

        this.positionsV1[this.count * 3 + 0] = relative.x;
        this.positionsV1[this.count * 3 + 1] = relative.y;
        this.positionsV1[this.count * 3 + 2] = relative.z;

        this.positionsV2[this.count * 3 + 0] = relative.x;
        this.positionsV2[this.count * 3 + 1] = relative.y;
        this.positionsV2[this.count * 3 + 2] = relative.z;

        this.positionsH1[this.count * 3 + 0] = relative.x;
        this.positionsH1[this.count * 3 + 1] = relative.y;
        this.positionsH1[this.count * 3 + 2] = relative.z;

        this.positionsH2[this.count * 3 + 0] = relative.x;   
        this.positionsH2[this.count * 3 + 1] = relative.y;
        this.positionsH2[this.count * 3 + 2] = relative.z;
  
        this.count++;

        this.lineV1.geometry.setDrawRange(0, this.count);
        this.lineV2.geometry.setDrawRange(0, this.count);
        this.lineH1.geometry.setDrawRange(0, this.count);
        this.lineH2.geometry.setDrawRange(0, this.count);

        this.lineV1.geometry.attributes.position.needsUpdate = true;
        this.lineV2.geometry.attributes.position.needsUpdate = true;
        this.lineH1.geometry.attributes.position.needsUpdate = true;
        this.lineH2.geometry.attributes.position.needsUpdate = true;

    }

    updateDimensionsOnDrawing() {
                           
        let line:any;
        //add dimentions for 4 lines of rectangle
        for(let i=0; i<4; i++) {
            
            if(i==0)
                line = this.lineV1;
            else if(i==1)
                line = this.lineV2;    
            else if(i==2)
                line = this.lineH1;  
            else if(i==3)
                line = this.lineH2;   
            
            this.dimensionsShape.update( line, this.engServ );

        }
        
    }

    

    

    

}
