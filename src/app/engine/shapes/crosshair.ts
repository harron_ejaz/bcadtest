import { relative } from 'path';
import { EngineService } from '../engine.service';
import { Vector3, LineBasicMaterial, BufferGeometry, Line, BufferAttribute } from 'three';
import { MessageService } from 'src/app/services/message.service';
import { Subscription } from 'rxjs';

export class Crosshair {

    lineX:any;
    lineY:any;
    positionsX:any;
    positionsY:any;
    subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService ) {
        this.draw();
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if( message == "CROSSHAIR_COLOR_CHANGED" ) {
                this.changeColor();
            }
            else if( message == "CROSSHAIR_HIDE" ) {
                this.hide();
            }
            else if( message == "CROSSHAIR_SHOW" ) {
                this.show();
            }
        })
    }


    onMouseMove( relative:Vector3 ) { 
        
        this.update( relative );
 
    }


    draw() {
        const material = new LineBasicMaterial({
            color: this.MS.settings.CROSSHAIR_COLOR 
        });
        
        let geometry:BufferGeometry = new BufferGeometry();
        this.positionsX = new Float32Array(6);
        geometry.setAttribute('position', new BufferAttribute(this.positionsX, 3));

        this.lineX = new Line( geometry, material );
        this.lineX.name = "CursorX";
        this.lineX.renderOrder = 10;
        this.engServ.commonGroup.add( this.lineX );

        geometry = new BufferGeometry();
        this.positionsY = new Float32Array(6);
        geometry.setAttribute('position', new BufferAttribute(this.positionsY, 3));
        this.lineY = new Line( geometry, material );
        this.lineY.name = "CursorY";
        this.lineY.renderOrder = 10;
        this.engServ.commonGroup.add( this.lineY );
        
    }

    update( relative:Vector3 ) {

        let relativeX:number = Math.round( relative.x );
        let relativeY:number = Math.round( relative.y );

        let CROSSHAIR_SIZE:number = this.MS.settings.CROSSHAIR_SIZE / this.engServ.camera.zoom;
        
        this.positionsX[0] = relativeX - CROSSHAIR_SIZE;
        this.positionsX[1] = relativeY;
        this.positionsX[2] = 0;
        this.positionsX[3] = relativeX + CROSSHAIR_SIZE;
        this.positionsX[4] = relativeY;
        this.positionsX[5] = 0;
        this.lineX.geometry.attributes.position.needsUpdate = true;

        this.positionsY[0] = relativeX;
        this.positionsY[1] = relativeY - CROSSHAIR_SIZE;
        this.positionsY[2] = 0;
        this.positionsY[3] = relativeX;
        this.positionsY[4] = relativeY + CROSSHAIR_SIZE;
        this.positionsY[5] = 0;
        this.lineY.geometry.attributes.position.needsUpdate = true;

    }

    hide () {
          this.lineX.visible = false;
          this.lineY.visible = false;
    }

    show () {
        this.lineX.visible = true;
        this.lineY.visible = true;        
    }

    changeColor() {
        this.lineX.material.color.setHex(this.MS.settings.CROSSHAIR_COLOR);
        this.lineY.material.color.setHex(this.MS.settings.CROSSHAIR_COLOR);
    }

}
