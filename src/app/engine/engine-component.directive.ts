import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[engine-component-host]'
})
export class EngineComponentDirective {

  constructor( public viewContainerRef: ViewContainerRef ) { }

}
