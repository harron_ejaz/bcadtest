import { WebGLRenderer, OrthographicCamera, Scene, Group, AmbientLight, MOUSE,
  MeshLambertMaterial, PlaneGeometry, Mesh, Raycaster, BoxBufferGeometry, MeshBasicMaterial,
  LineDashedMaterial, BufferGeometry, Line, Object3D, Vector3 } from 'three';
import { OrbitControls } from 'src/assets/CustomOrbitControls';
import { DragControls } from 'three/examples/jsm/controls/DragControls';
import { CSS2DRenderer  } from 'three/examples/jsm/renderers/CSS2DRenderer';
import { SelectionBox } from 'three/examples/jsm/interactive/SelectionBox';
import { SelectionHelper } from 'three/examples/jsm/interactive/SelectionHelper';
import { Injectable, ElementRef, OnDestroy, NgZone } from '@angular/core';
import { FunctionService } from '../services/function.service';
import { MessageService } from '../services/message.service';

@Injectable({
  providedIn: 'root'
})
export class EngineService implements OnDestroy {  
  public canvasRef: ElementRef;
  public canvas: HTMLCanvasElement;
  public renderer: WebGLRenderer;
  public aspect: number;
  public camera: OrthographicCamera;
  public scene: Scene;
  public rootBuildingsGroup: Group;
  public commonGroup: Group;
  public dimensionsGroup: Group;
  private light: AmbientLight;
  public linePointSquare: Mesh;
  public orbitControls: OrbitControls;
  public dragControls: DragControls;
  public dimensionsRenderer: CSS2DRenderer;
  public selectionBox:SelectionBox;
  public selectionHelper: SelectionHelper;
  public sessionStorageKeyIndex: number = -1;
  public defaultRootBuildingsGroup:any;
  private frameId: number = null;
  public canvasWidth:number = 14000;
  public canvasHeight:number = 5000;

  public constructor(private ngZone: NgZone, private FS:FunctionService, private MS:MessageService) {}

  public ngOnDestroy() {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
  }

  createScene(canvas: ElementRef<HTMLCanvasElement>): void { 
    this.canvasRef = canvas;
    // The first step is to get the reference of the canvas element from our HTML document
    this.canvas = canvas.nativeElement;

    this.renderer = new WebGLRenderer({
      canvas: this.canvas,
      antialias: true, // smooth edges,
      preserveDrawingBuffer: true //this flag will help you to get the base64 encoding of the current frame
    });
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( this.canvasWidth/10, this.canvasHeight/10 );
    this.renderer.setClearColor( this.MS.color.BACKGROUND, 1 );

    this.dimensionsRenderer = new CSS2DRenderer();
    this.dimensionsRenderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    this.dimensionsRenderer.domElement.style.position = 'absolute';
    this.dimensionsRenderer.domElement.style.top = '0';
    this.dimensionsRenderer.domElement.style.pointerEvents = 'none';
    this.canvas.parentElement.appendChild( this.dimensionsRenderer.domElement );
    
    // create the scene
    this.scene = new Scene();

    // soft white light
    this.light = new AmbientLight( 0x404040, 5 );
    this.light.position.z = 10;
    this.scene.add(this.light);
    
    //create camera
    this.camera = new OrthographicCamera(
      this.canvasWidth/-10, this.canvasWidth/10, this.canvasHeight/10, this.canvasHeight/-10, 1, 1000
    );
    
    this.camera.position.z = 400; 
    
    this.scene.add(this.camera);

    this.dimensionsGroup = new Group();
    this.dimensionsGroup.name = 'DimensionsGroup';
    this.scene.add( this.dimensionsGroup );

    //line point to be shown at endpoint and midpoint of lines when mouse overs a line for drawing in line editor page
    let geometry = new BoxBufferGeometry( 10, 10, 0);
    let material = new MeshBasicMaterial( {color: this.MS.color.LINE_POINT, wireframe: false, opacity: 0.8, transparent: true} );
    this.linePointSquare = new Mesh( geometry, material );
    this.linePointSquare.name = 'linePointSquare';
    this.linePointSquare.renderOrder = 2;
    this.linePointSquare.visible = false;
    let scale:number = this.FS.get_line_point_scale(this.camera);
    this.linePointSquare.scale.set( scale, scale , 1 ); 
    this.scene.add( this.linePointSquare );

    this.orbitControls = new OrbitControls(this.camera, this.renderer.domElement);
    this.orbitControls.mouseButtons = { LEFT: MOUSE.ROTATE, MIDDLE: MOUSE.PAN, RIGHT: MOUSE.ROTATE }
    this.orbitControls.enabled = true;
    this.orbitControls.enableRotate = false;
    this.orbitControls.autoRotate = false;
    this.orbitControls.enablePan = true;
    this.orbitControls.screenSpacePanning = true;
    this.orbitControls.enableZoom = true;
    this.orbitControls.zoomSpeed = 0.5;
    this.orbitControls.minZoom = 0;
    this.orbitControls.maxZoom = 10;
    var scope = this;
    this.orbitControls.addEventListener( 'change', orbitControls_changeHandler );

    function orbitControls_changeHandler(e) {

      let scale:number = scope.FS.get_line_point_scale(scope.camera);
      scope.linePointSquare.scale.set( scale, scale , 1 ); 

      //check if Line exists
      if( scope.getSelectedGeometriesGroup().getObjectByName('Line') || scope.getSelectedGeometriesGroup().getObjectByName('Circle') || scope.getSelectedGeometriesGroup().getObjectByName('Ellipse') || scope.getSelectedGeometriesGroup().getObjectByName('Arc') ) {
        //scale all line edges in Line Objects
        let engGroup = scope.getSelectedGeometriesGroup(); 
        for (let i:number = 0; i < engGroup.children.length; i++) {
            //if valid object
            if( engGroup.children[i].getObjectByName('Line') || scope.getSelectedGeometriesGroup().getObjectByName('Circle') || scope.getSelectedGeometriesGroup().getObjectByName('Ellipse') || scope.getSelectedGeometriesGroup().getObjectByName('Arc') ) {
                let object:Object3D = engGroup.children[i];
                //if Line Edge exists then scale both edges
                if( object.getObjectByName('LineEdge1') ) { 
                  let lineEdge1 = object.getObjectByName('LineEdge1');
                  lineEdge1.scale.set( scale, scale , 1 );
                }     
                if( object.getObjectByName('LineEdge2') ) { 
                  let lineEdge2 = object.getObjectByName('LineEdge2');
                  lineEdge2.scale.set( scale, scale , 1 );
                } 
                if( object.getObjectByName('LineEdge3') ) { 
                  let lineEdge3 = object.getObjectByName('LineEdge3');
                  lineEdge3.scale.set( scale, scale , 1 );
                }                  
            }

            
        }
      }

    }

    //load canvas background
    this.loadExtentBackground();

    this.selectionBox = new SelectionBox( this.camera, this.scene );
    this.selectionHelper = new SelectionHelper( this.selectionBox, this.renderer, 'selectionBoxHide' );
    
    //group for align, scale, extents, image
    this.commonGroup = new Group();
    this.commonGroup.name = 'CommonGroup';
    this.scene.add( this.commonGroup );

    let extentsGroup = new Group();
    extentsGroup.name = 'ExtentsGroup';
    this.commonGroup.add( extentsGroup );

    //this group is the main root group
    this.rootBuildingsGroup = new Group();
    this.rootBuildingsGroup.name = 'RootBuildingsGroup';
    this.rootBuildingsGroup.renderOrder = 1;
    this.scene.add( this.rootBuildingsGroup );

    this.addBuildingsGroup( null );

    console.log("Engine Service Loaded");
  }

  

  public addBuildingsGroup( type:string ) { 

    let buildingsGroup:Group;
    let geometriesGroup:Group;

    let areasGroup:Group;

    //if button is clicked, 
    //means this function is not called automatically for the first time when project gets loaded
    let index:number;
    if( type == "click" ) index = this.MS.project_information.BUILDINGS.length - 1;
    else if( type == "load_scd" ) index = 0;
    else index = 0;

    for( let i = index; i < this.MS.project_information.BUILDINGS.length; i++ ) {

      //this group is the main group that will store all other groups canvas
      buildingsGroup = new Group();
      buildingsGroup.name = this.MS.project_information.BUILDINGS[i].Name;

      if( type == "click" ) this.MS.project_information.BUILDINGS[i].Id = buildingsGroup.uuid; 
      else if( type == "load_scd" ) buildingsGroup.uuid = this.MS.project_information.BUILDINGS[i].Id;

      //this group will be shown in Line Editor page's canvas
      geometriesGroup = new Group();
      geometriesGroup.name = 'GeometriesGroup';
      buildingsGroup.visible = false;
      buildingsGroup.add( geometriesGroup );

     

      //this group will be shown in Area Editor page's canvas
      areasGroup = new Group();
      areasGroup.name = 'AreasGroup';
      buildingsGroup.add( areasGroup );

      this.rootBuildingsGroup.add( buildingsGroup );

    }

  }

  // add drawing to selected structure group
  public addToScene( obj:any ) {
    let group:any = this.getSelectedGroup(obj);
    group.add( obj ); 
  }

  // remove drawing from selected structure group
  public removeFromScene( obj:any ) {
    let group:any = this.getSelectedGroup(obj);
    group.remove( obj ); 
  }
  
  private getSelectedGroup( obj:any ) {
    let index:any = this.MS.menu_line_editor.BUILDING_GROUP_SELECTED;
    let buildingsGroup:any = this.rootBuildingsGroup.children[ index ];

    let group:any;

    try {

      if( obj.name == "Line" || obj.name == "Arc" || 
          obj.name == "Circle" || obj.name == "Ellipse" || 
          obj.name == "Text" || obj.name == "EllipseRadius" ) {
        group = buildingsGroup.getObjectByName( "GeometriesGroup" );
      }
      else if( obj.name == "Polygon" || obj.name == "PolygonAlphabet" ) {
        group = buildingsGroup.getObjectByName( "AreasGroup" );
      }
      //in case of editing geometries
      else if( obj.name == "" ) {
        group = buildingsGroup.getObjectByName( "GeometriesGroup" );
      }

    }
    catch(e) { //when obj.name is null
      group = buildingsGroup.getObjectByName( "GeometriesGroup" );
    }
    
    return group; 
  }

  public getSelectedBuildingsGroup() {
    let index:any = this.MS.menu_line_editor.BUILDING_GROUP_SELECTED;
    let buildingsGroup:any = this.rootBuildingsGroup.children[ index ];
    return buildingsGroup;
  }

  public getSelectedGeometriesGroup() {
    let index:any = this.MS.menu_line_editor.BUILDING_GROUP_SELECTED;
    let buildingsGroup:any = this.rootBuildingsGroup.children[ index ];
    let geometriesGroup:any = buildingsGroup.getObjectByName( "GeometriesGroup" );
    return geometriesGroup;
  }

  public getSelectedAreasGroup() {
    let index:any = this.MS.menu_line_editor.BUILDING_GROUP_SELECTED;
    let buildingsGroup:any = this.rootBuildingsGroup.children[ index ];
    let areasGroup:any = buildingsGroup.getObjectByName( "AreasGroup" );
    return areasGroup;
  }

  showSelectedBuildingsGroup( index:number ) {
    let buildingsGroup:any = this.rootBuildingsGroup.children[ index ];
    buildingsGroup.visible = true;
  }

  hideAllBuildingsGroups() {
    let buildingsGroup:any = this.rootBuildingsGroup.children;
    for( let i = 0; i < buildingsGroup.length; i++ ) {
      buildingsGroup[i].visible = false;
    }
    this.MS.sendMessage("REMOVE_ALL_DIMENSIONS");
    this.MS.sendMessage("ADD_ALL_DIMENSIONS");
  }

  animate(): void {
    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }
    });
  }



  render() {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });
    this.renderer.render(this.scene, this.camera);

    this.rePositionDimenions();
    this.dimensionsRenderer.render(this.scene, this.camera);
  }

  private rePositionDimenions() {
    this.dimensionsRenderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);

    //Area Editor page
    if( this.MS.COMPONENT_ID == 4 ) {
      this.dimensionsRenderer.domElement.style.left = '34%';
    }
    else {
      this.dimensionsRenderer.domElement.style.left = '0';
    }
    
  }

  resize() {
    const width = this.canvas.clientWidth;
    const height = this.canvas.clientHeight;

    this.camera.updateProjectionMatrix();

    this.renderer.setSize( width, height );
  }

  changeExtentBackgroundColor() {
    let background:any = this.scene.getObjectByName("Background");
    background.material.color.set( this.MS.color.EXTENT );
  }

  //load canvas background
  loadExtentBackground() {

    // Load a custom material
    var material = new MeshLambertMaterial({
      opacity: 1,
      transparent: false,
      color: this.MS.color.EXTENT
    });

    // create a plane geometry
    var geometry = new PlaneGeometry(this.canvasWidth*10, this.canvasHeight*10);

    // combine our image geometry and material into a mesh
    let background = new Mesh(geometry, material);

    // set the position of the background mesh in the x,y,z dimensions
    background.position.set(0, 0, 0);
    background.name = "Background";

    // add the image to the scene
    this.scene.add(background);
  }


  get3DPosition(clientX:number, clientY:number) { 
    const position = {
        x: ( ( clientX - this.getCurrentOffset( this.canvasRef ).left ) / this.canvasWidth*10 ) * 2 - 1,
        y: -( ( clientY - this.getCurrentOffset( this.canvasRef ).top ) / this.canvasHeight*10 ) * 2 + 1
    };
    const rayCaster:Raycaster = new Raycaster();
    rayCaster.setFromCamera(position, this.camera);
    const intersects = rayCaster.intersectObjects(this.scene.children, true);

    if (intersects.length > 0) { 
        //if canvas has multiple Object, then find the background object to get exact mouse position
        for( let i = 0; i < intersects.length; i++) {
          if( intersects[i].object.name == "Background" ) {
            return intersects[i].point;
          }
        }
    }
    return null;

  }

  get2DPosition(clientX:number, clientY:number) { 
    return new Vector3( clientX, clientY * -1, 0 );
  }


  getIntersects(clientX:number, clientY:number, group:Group) {
    const position = {
      x: ( ( clientX - this.getCurrentOffset( this.canvasRef ).left ) / this.canvasWidth*10 ) * 2 - 1,
      y: -( ( clientY - this.getCurrentOffset( this.canvasRef ).top ) / this.canvasHeight*10 ) * 2 + 1
    };
    let rayCaster:Raycaster = new Raycaster();
    rayCaster.params.Line.threshold = this.FS.get_threshold( this.camera );
    rayCaster.setFromCamera(position, this.camera);
    const intersects = rayCaster.intersectObject( group, true );

    if ( intersects.length > 0 ) {
      let res = intersects.filter( function ( res ) {
          return res && res.object;
      } )[ 0 ];
      if ( res && res.object ) {
        return res.object;
      }
    }
    else
      return null;

  }

  getIntersectsArea(clientX:number, clientY:number, group:Group) {
    let width:number = this.canvasWidth - ( 34 / 100 * this.canvasWidth );
    const position = {
      x: ( ( clientX - this.getCurrentOffset( this.canvasRef ).left ) / width*10 ) * 2 - 1,
      y: -( ( clientY - this.getCurrentOffset( this.canvasRef ).top ) / this.canvasHeight*10 ) * 2 + 1
    };
    const rayCaster:Raycaster = new Raycaster();
    rayCaster.params.Points.threshold = this.FS.get_threshold( this.camera );
    rayCaster.setFromCamera(position, this.camera);
    const intersects = rayCaster.intersectObject( group, true );

    if ( intersects.length > 0 ) {
      let res = intersects.filter( function ( res ) {
          return res && res.object;
      } )[ 0 ];
      if ( res && res.object ) {
        return res.object;
      }
    }
    else
      return null;

  }

  get3DPosition_Intersects(clientX:number, clientY:number) { 

    const position = {
        x: ( ( clientX - this.getCurrentOffset( this.canvasRef ).left ) / this.canvasWidth*10 ) * 2 - 1,
        y: -( ( clientY - this.getCurrentOffset( this.canvasRef ).top ) / this.canvasHeight*10 ) * 2 + 1
    };

    const rayCaster:Raycaster = new Raycaster();
    rayCaster.params.Points.threshold = this.FS.get_threshold( this.camera );
    rayCaster.params.Line.threshold = this.FS.get_threshold( this.camera );
    rayCaster.setFromCamera(position, this.camera);
    const intersects = rayCaster.intersectObjects(this.scene.children, true);
        
    var point = [ { mousePosition:null, selectedObject:null } ];

    if (intersects.length > 0) { 

      var lineCount:number = 0;

      for( let i = 0; i < intersects.length; i++) {
        //only get intersects of line object and line that already have been drawn, not the line that is being drawn
        if( intersects[i].object.name == "Line" && intersects[i].object.userData.Drawn 
          && intersects[i].object.children.length != 2 ) { //if line has no children, means line has no edges, so line is not in stretch mode

          //only two lines are required to get intersection
          if( lineCount > 1 ) break;

          if( lineCount == 0 )
            point.pop();

          let res:any = intersects.filter( function ( res ) {
            return res && res.object;
          } )[ i ];
          if ( res && res.object ) {

            const pointObject = { mousePosition : res.point, selectedObject : res.object };
   
            point.push( pointObject );

            lineCount++;

          }

        }
        
      }
      return point;   

    }

  }

  getCurrentOffset(element: ElementRef) {
    const rect = element.nativeElement.getBoundingClientRect();
    let top:number = rect.top + window.pageYOffset - document.documentElement.clientTop;
    let left:number = rect.left + window.pageXOffset - document.documentElement.clientLeft;
    return {left:left, top:top};
  }

  setDefaultRootBuildingsGroup() {
    const groupJSON = this.rootBuildingsGroup.toJSON();
    this.defaultRootBuildingsGroup = JSON.stringify( groupJSON ); 
  }

  updateSessionStorage() {
    this.sessionStorageKeyIndex++;
    const groupJSON = this.rootBuildingsGroup.toJSON();
    try {
      sessionStorage.setItem( this.sessionStorageKeyIndex.toString(), JSON.stringify( groupJSON ) );
    }
    catch( e ) {
      console.log( "Session storage is full. Clearing session storage" );
      this.clearSession();
    }
  }

  clearSession() {
    sessionStorage.clear();
    this.sessionStorageKeyIndex = -1; 
  }

  enableDragControls( objects:any[] ) {
    this.dragControls = new DragControls( objects, this.camera, this.renderer.domElement );
    this.dragControls.activate();
    this.dragControls.enabled = true;
  }

  disableDragControls() {
    if( this.dragControls.enabled ) {
      this.dragControls.deactivate();
      this.dragControls.enabled = false;
      this.dragControls.dispose();
      this.dragControls = null;
    }    
  }

  private get_line_material():LineDashedMaterial {
    let lineType:any = this.FS.get_line_type_selected();
    // material
    const material:LineDashedMaterial = new LineDashedMaterial(
        { color: lineType.COLOR, dashSize: 1, gapSize: lineType.SOLID, depthTest: false }
    );
    return material;
  }

  set_line_shape(geometry:BufferGeometry ) {
    let material:LineDashedMaterial = this.get_line_material();
    // line
    let line:Line = new Line(geometry, material);
    line.userData.LineTypesRef = this.MS.menu_line_editor.LINE_TYPES_REF_SELECTED;
    line.userData.Rise = this.MS.menu_line_editor.RISE_SELECTED;
    return line;
  }

  //update line material which is being drawn, when line type menu is changed
  update_line_shape(line:any) {
    let material:LineDashedMaterial = this.get_line_material();
    // line
    line.material = material;
    line.userData.LineTypesRef = this.MS.menu_line_editor.LINE_TYPES_REF_SELECTED;
    line.userData.Rise = this.MS.menu_line_editor.RISE_SELECTED;
  }
 
}
