import { Vector3, BufferGeometry, LineBasicMaterial, Line } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from '../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Selection } from '../selection';
import { DimensionsShape } from '../../shapes/dimensions-shape';

export class ModifyFillet {

    private object:any = null;
    private filletObject:any = null;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection ) {
    }

    onClick( relative:Vector3 ) {
        //if line is already highlighted, and object is not already selected (dashed line)
        //then convert it to a dash line when clicked on it
        if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.filletObject )
            this.fillet();
    }

    onMouseMove( relative:Vector3, e:any ) {
        //if no object has already been selected (dashed line)
        //then highlight a selected object on canvas
        if( !this.selection.selected ) {
            this.highlightObject( e );
        }
        //if object is already selected, then highlight the extending object, but not already selected object
        else if( this.selection.selected ) {
            this.highlightFilletObject( e );
        }
    }

    onStop() {
        //if line is already selected, un-select it
        if( this.selection.selected ) { 
           this.selection.unselect( this.object );
           this.selection.selectedObjects = [];
        }
    }

    fillet() {
        const objectArray = this.object.geometry.attributes.position.array;
        const objectX1:number = objectArray[0];
        const objectY1:number = objectArray[1];
        const objectX2:number = objectArray[3];
        const objectY2:number = objectArray[4];
    
        let filletObjectArray = this.filletObject.geometry.attributes.position.array;
        const filletObjectX1:number = filletObjectArray[0];
        const filletObjectY1:number = filletObjectArray[1];
        const filletObjectX2:number = filletObjectArray[3];
        const filletObjectY2:number = filletObjectArray[4];
        
        let intersection:any = this.FS.get_line_intersection( objectX1, objectY1, objectX2, objectY2, filletObjectX1, filletObjectY1, filletObjectX2, filletObjectY2 );

        if( intersection.x == null || intersection.y == null ) {
            return;
        }

        //check which point of fillet line is closer to intersecting point
        const filletDistance1:number = this.FS.get_distance_between_points( filletObjectX1, filletObjectY1, intersection.x, intersection.y );
        const filletDistance2:number = this.FS.get_distance_between_points( filletObjectX2, filletObjectY2, intersection.x, intersection.y );
        
        if( filletDistance1 < filletDistance2 ) {
            filletObjectArray[0] = intersection.x;
            filletObjectArray[1] = intersection.y;
        }
        else if( filletDistance2 < filletDistance1 ) {
            filletObjectArray[3] = intersection.x;
            filletObjectArray[4] = intersection.y;
        }

        this.filletObject.geometry.attributes.position.needsUpdate = true;
        //bounding sphere is set so that line can be selected using mouse
        this.filletObject.geometry.computeBoundingSphere(); 

        //check which point of object line is closer to intersecting point
        const objectDistance1:number = this.FS.get_distance_between_points( objectX1, objectY1, intersection.x, intersection.y );
        const objectDistance2:number = this.FS.get_distance_between_points( objectX2, objectY2, intersection.x, intersection.y );
        
        if( objectDistance1 < objectDistance2 ) {
            objectArray[0] = intersection.x;
            objectArray[1] = intersection.y;
        }
        else if( objectDistance2 < objectDistance1 ) {
            objectArray[3] = intersection.x;
            objectArray[4] = intersection.y;
        }
    
        this.object.geometry.attributes.position.needsUpdate = true;
        //bounding sphere is set so that line can be selected using mouse
        this.object.geometry.computeBoundingSphere(); 
        
        this.dimensionsShape.update( this.object, this.engServ );
        this.dimensionsShape.update( this.filletObject, this.engServ );

        this.selection.unselect( this.filletObject )
    }

    private highlightObject( e:any ) {
        if ( this.object ) {
            if( this.isValidObject( this.object.name ) ) {
                //un-highlight object
                let color:any = this.FS.get_line_type( this.object.userData.LineTypesRef ).COLOR;
                this.object.material.color.set( color );
                this.object = null;
            }
        }
        this.object = this.engServ.getIntersects( e.clientX, e.clientY, this.engServ.getSelectedGeometriesGroup() );
        if( this.object ) {
            if( this.isValidObject( this.object.name ) ) {
                //highlight object
                this.object.material.color.set( this.MS.color.HIGHLIGHT_LINE );
            }
        }
    }

    private highlightFilletObject( e:any ) {
    
        if ( this.filletObject ) {

            if( this.filletObject.id == this.object.id ) {
                this.filletObject = null;
                return;
            }
                

            if( this.isValidObject( this.filletObject.name ) ) {
                //un-highlight object
                this.filletObject.material.color.set( this.MS.color.LINE );
                this.filletObject = null;
            }
        }
        this.filletObject = this.engServ.getIntersects( e.clientX, e.clientY, this.engServ.getSelectedGeometriesGroup() );

        if( this.filletObject ) {

            if( this.filletObject.id == this.object.id )
                return;
                
            if( this.isValidObject( this.filletObject.name ) ) {
                //highlight object
                this.filletObject.material.color.set( this.MS.color.HIGHLIGHT_LINE );
            }
        }
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineBasicMaterial = new LineBasicMaterial(
            { color: this.MS.color.REF_LINE }
        );
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" )
            return true;
        else    
            return false;    
    }

}
