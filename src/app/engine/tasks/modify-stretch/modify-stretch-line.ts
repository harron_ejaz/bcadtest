import { Vector3, Object3D } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Selection } from '../selection';


export class ModifyStretchLine {

    object:any;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private selection:Selection ) {

    }

    // update line
    update(relative:Vector3) {
        var linePosition = this.object.geometry.attributes.position.array;

        let edge1:Vector3 = this.object.getObjectByName('LineEdge1').position;
        let edge2:Vector3 = this.object.getObjectByName('LineEdge2').position;

        var dragging:string;

        //if edge1 is dragging
        if( Math.floor( relative.x) == Math.floor( edge1.x ) && Math.floor( relative.y) == Math.floor( edge1.y) ) {
            dragging = "edge1";
        }
        //if edge2 is dragging
        else if( Math.floor( relative.x) == Math.floor( edge2.x ) && Math.floor( relative.y) == Math.floor( edge2.y) ) {
            dragging = "edge2";
        }

        //if Grid or Radial button is enabled in Line Editor page's footer
        if( this.MS.footer_lineEditor.GRID || this.MS.footer_lineEditor.RADIAL ) {
            let lineLength:number;

            if( this.MS.footer_lineEditor.GRID )
                lineLength = this.FS.getGridSnapLineLength( edge1.x, edge1.y, edge2.x, edge2.y );
            else
                lineLength = this.FS.get_distance_between_points( edge1.x, edge1.y, edge2.x, edge2.y );

            var angleBetweenPoints:number;    
            //if edge1 is dragging
            if( dragging == "edge1" ) {
                angleBetweenPoints = this.FS.get_angle_between_points( edge2.x, edge2.y, edge1.x, edge1.y );
            }
            //if edge2 is dragging
            else if( dragging == "edge2" ) {
                angleBetweenPoints = this.FS.get_angle_between_points( edge1.x, edge1.y, edge2.x, edge2.y );
            }       
            
            //if Radial button is enabled in Line Editor page's footer
            if( this.MS.footer_lineEditor.RADIAL )
                angleBetweenPoints = this.FS.get_angle_radial(angleBetweenPoints, this.MS.settings.RADIAL_DEGREES);
            
            var gridSnapPoint:any;

            //if edge1 is dragging
            if( dragging == "edge1" ) {
                gridSnapPoint = this.FS.get_point_from_angle_distance( edge2.x, edge2.y, angleBetweenPoints,  lineLength );
            }
            //if edge2 is dragging
            else if( dragging == "edge2" ) {
                gridSnapPoint = this.FS.get_point_from_angle_distance( edge1.x, edge1.y, angleBetweenPoints,  lineLength )
            }    
            
    
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                
                //if edge1 is dragging
                if( dragging == "edge1" ) {
                    gridSnapPoint.copy( this.engServ.linePointSquare.position );
                }
                //if edge2 is dragging
                else if( dragging == "edge2" ) {
                    gridSnapPoint.copy( this.engServ.linePointSquare.position );
                }
            }

            //if edge1 is dragging
            if( dragging == "edge1" ) {
                linePosition[0] = gridSnapPoint.x;
                linePosition[1] = gridSnapPoint.y;
                linePosition[2] = gridSnapPoint.z;

                linePosition[3] = edge2.x;
                linePosition[4] = edge2.y;
                linePosition[5] = edge2.z;
            }
            //if edge2 is dragging
            else if( dragging == "edge2" ) {
                linePosition[0] = edge1.x;
                linePosition[1] = edge1.y;
                linePosition[2] = edge1.z;

                linePosition[3] = gridSnapPoint.x;
                linePosition[4] = gridSnapPoint.y;
                linePosition[5] = gridSnapPoint.z;
            }

        }
        else {

            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){

                //if edge1 is dragging
                if( dragging == "edge1" ) {
                    edge1.copy( this.engServ.linePointSquare.position );
                }
                //if edge2 is dragging
                else if( dragging == "edge2" ) {
                    edge2.copy( this.engServ.linePointSquare.position );
                }
                
            }

            //update line
            linePosition[0] = edge1.x;
            linePosition[1] = edge1.y;
            linePosition[2] = edge1.z;

            linePosition[3] = edge2.x;
            linePosition[4] = edge2.y;
            linePosition[5] = edge2.z;

        }
    
        this.object.geometry.attributes.position.needsUpdate = true;

        //bounding sphere is set so that line can be selected using mouse
        this.object.geometry.computeBoundingSphere();
    }

    updateMultiLines(relative: Vector3) {
        if( this.object.name == "LineEdge1" || this.object.name == "LineEdge2" ) {
            let touch = this.object.userData.touch;
            for(let i=0; i<touch.length; i++) {
                let distance:number = touch[i][2].distance;
                let line1_id = touch[i][2].line1_id;
                let line2_intersection_id = touch[i][2].line2_intersection_id;
                let line2_touch_id = touch[i][2].line2_touch_id;
                //line1 edge has a line2 touching
                if( distance == 0 ) {
                    if( line2_touch_id == null ) {
console.log(1)
                    }
                    else {
                        console.log(2)
                    }
                }
                //line1 edge has no line2 touching
                else {
                    if( line2_touch_id == null ) {
                        console.log(3)
                    }
                    else {
                        console.log(4)
                    }
                }
            }
        }
    }

    updateLines(relative: Vector3) {
 
        let object:any;
        let dragging:any;

        for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {

            object = this.selection.selectedObjects[i];

            dragging = null;

            var objectEdge1:Vector3 = object.children[0].position;
            var objectEdge2:Vector3 = object.children[1].position;

            //if edge1 is dragging
            if( Math.floor( relative.x) == Math.floor( objectEdge1.x ) && Math.floor( relative.y) == Math.floor( objectEdge1.y) ) {
                dragging = "edge1";
                break;
            }
            //if edge2 is dragging
            else if( Math.floor( relative.x) == Math.floor( objectEdge2.x ) && Math.floor( relative.y) == Math.floor( objectEdge2.y) ) {
                dragging = "edge2";
                break;
            }

        }

        if( dragging == null) return;

        var objectPosition = object.geometry.attributes.position.array;

        //if Grid or Radial button is enabled in Line Editor page's footer
        if( this.MS.footer_lineEditor.GRID || this.MS.footer_lineEditor.RADIAL ) {
            
        }
        else {
        
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){

                //if edge1 is dragging
                if( dragging == "edge1" ) {
                    objectEdge1.copy( this.engServ.linePointSquare.position );
                }
                //if edge2 is dragging
                else if( dragging == "edge2" ) {
                    objectEdge2.copy( this.engServ.linePointSquare.position );
                }
                    
            }

            //update line
            objectPosition[0] = objectEdge1.x;
            objectPosition[1] = objectEdge1.y;
            objectPosition[2] = objectEdge1.z;

            objectPosition[3] = objectEdge2.x;
            objectPosition[4] = objectEdge2.y;
            objectPosition[5] = objectEdge2.z;

        }

        object.geometry.attributes.position.needsUpdate = true;

        //bounding sphere is set so that line can be selected using mouse
        object.geometry.computeBoundingSphere();

        let objectTouch:any = object.userData.touch;

        let engGroup:any = this.engServ.getSelectedGeometriesGroup();

        for( let i = 0; i < objectTouch.length; i++) {

            let objectTouchX:number = objectTouch[i][0];
            let objectTouchY:number = objectTouch[i][1];

            let object_touch_id:any = objectTouch[i][2].line_touch_id;
            let object_intersection_id:any = objectTouch[i][2].line_intersection_id;
            let object_touch_edge:number = objectTouch[i][2].line_touch_edge;

            if( object_touch_id != null ) {

                let line:any = engGroup.getObjectById( object_touch_id );

                //if touching line is also selected
                if( line.userData.Selected == true ) {

                    let linePosition = line.geometry.attributes.position.array;

                    let lineEdge1:Vector3 = line.children[0].position;
                    let lineEdge2:Vector3 = line.children[1].position;

                    let lineTouch:any = line.userData.touch;

                    let j;

                    for( j = 0; j < lineTouch.length; j++) {

                        let lineTouchX:number = lineTouch[j][0];
                        let lineTouchY:number = lineTouch[j][1];

                        let line_touch_edge:number = lineTouch[j][2].line_touch_edge;

                        if( line_touch_edge == object_touch_edge ) {
                            console.log(line_touch_edge, dragging)
                            
                            
                            break;
                        }


                    }
                    
                    
                    
                    
                    

                    //update line
                    linePosition[0] = lineEdge1.x;
                    linePosition[1] = lineEdge1.y;
                    linePosition[2] = lineEdge1.z;

                    linePosition[3] = lineEdge2.x;
                    linePosition[4] = lineEdge2.y;
                    linePosition[5] = lineEdge2.z;

                    

                    line.geometry.attributes.position.needsUpdate = true;

                    //bounding sphere is set so that line can be selected using mouse
                    line.geometry.computeBoundingSphere();
        
                }

                
                
            }

            

        }


    }

    

    
}
