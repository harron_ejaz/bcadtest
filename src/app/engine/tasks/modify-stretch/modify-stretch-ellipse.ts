import { Vector3, BufferGeometry, EllipseCurve } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';

export class ModifyStretchEllipse {

    object:any;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {

    }

    // update Ellipse
    update(relative:Vector3) {

        let edge1:Vector3 = this.object.getObjectByName('LineEdge1').position;
        let edge2:Vector3 = this.object.getObjectByName('LineEdge2').position;

        //if line end point or mid point is enabled and line point square is visible
        //means mouse is over end point or midpoint
        //then add point will use line point square position
        if( this.engServ.linePointSquare.visible ){

            //if edge1 is dragging
            if( Math.floor( relative.x) == Math.floor( edge1.x ) && Math.floor( relative.y) == Math.floor( edge1.y) ) {
                edge1.copy( this.engServ.linePointSquare.position );
            }
            //if edge2 is dragging
            else if( Math.floor( relative.x) == Math.floor( edge2.x ) && Math.floor( relative.y) == Math.floor( edge2.y) ) {
                edge2.copy( this.engServ.linePointSquare.position );
            }
            
        }

        const center:Vector3 = new Vector3( this.object.geometry.boundingSphere.center.x, this.object.geometry.boundingSphere.center.y, 0 );

        let angle:number = this.FS.get_angle_between_points( center.x, center.y, edge1.x, edge1.y );
        let aRotation:number = this.FS.degrees_to_radians( angle );

        //get the distance from ellipse center/position position to ellipse edge1
        let xRadius:number = center.distanceTo( edge1 );
        if( xRadius == 0 ) 
            xRadius = 0.1;

        //get the distance from ellipse center/position position to ellipse edge2
        let yRadius:number = center.distanceTo( edge2 );
        if( yRadius == 0 ) 
            yRadius = 0.1;    

        const geometry:BufferGeometry = this.ellipseGeometry( xRadius, yRadius, aRotation );
        //update ellipse
        this.object.geometry.attributes.position.array = geometry.attributes.position.array;
        this.object.geometry.attributes.position.needsUpdate = true;

        //bounding sphere is set so that line can be selected using mouse
        this.object.geometry.computeBoundingSphere();
            
    }

    private ellipseGeometry( xRadius:number, yRadius:number, aRotation:number ):BufferGeometry {
        let curve:EllipseCurve = new EllipseCurve(
            0,  0,            // ax, aY
            xRadius, yRadius,           // xRadius, yRadius
            0,  2 * Math.PI,  // aStartAngle, aEndAngle
            false,            // aClockwise
            aRotation                // aRotation
        );
        const points = curve.getPoints( 50 );
        // geometry
        const geometry:BufferGeometry = new BufferGeometry().setFromPoints( points );
        return geometry;
    }
}
