import { Vector3, BoxBufferGeometry, MeshBasicMaterial, Mesh, Object3D } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from './../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { ModifyStretchLine } from './modify-stretch-line';
import { ModifyStretchCircle } from './modify-stretch-circle';
import { ModifyStretchEllipse } from './modify-stretch-ellipse';
import { ModifyStretchArc } from './modify-stretch-arc';
import { Selection } from '../selection';
import { DimensionsShape } from '../../shapes/dimensions-shape';

export class ModifyStretch {


    private dragging:boolean = false;
    private dragend:boolean = false;

    private modifyStretchLine:ModifyStretchLine;
    private modifyStretchCircle:ModifyStretchCircle;
    private modifyStretchEllipse:ModifyStretchEllipse;
    private modifyStretchArc:ModifyStretchArc;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection ) {
        this.modifyStretchLine = new ModifyStretchLine( this.engServ, this.MS, this.FS, this.selection );
        this.modifyStretchCircle = new ModifyStretchCircle( this.engServ, this.MS, this.FS );
        this.modifyStretchEllipse = new ModifyStretchEllipse( this.engServ, this.MS, this.FS );
        this.modifyStretchArc = new ModifyStretchArc( this.engServ, this.MS, this.FS );
    }

    onMouseDown( relative:Vector3 ) {
        //if line is not selected
        if( !this.selection.selected ) {
            this.selection.enable();
        }
    }

    onClick( relative:Vector3 ) {
        
    
        //single object is selected using selection
       if( this.selection.selectedObjects.length == 1 && this.selection.selected && this.dragging == false ) {

            //if mouseup after dragging a line edge
            if( this.dragend == true ) {
                this.dragend = false;
            }
            else if( this.dragend == false ) {
                //add edges
                this.addEdges();
            }
            
        }
        //multiple objects are selected using selection
        else if( this.selection.selectedObjects.length > 1 && this.selection.selected && this.dragging == false ) {

            //if mouseup after dragging a line edge
            if( this.dragend == true ) {
                this.dragend = false;
            }
            else if( this.dragend == false ) {
                //add edges
                this.addEdgesforMultipleObjects();
             
            }
            
        }

    }

    onMouseMove( relative:Vector3, e:any ) {
        //single object is selected
        if( this.selection.selectedObjects.length == 1 && this.selection.selected && this.dragging ) {

            let object:any = this.selection.selectedObjects[0];

            if( object.name == 'Line' ) {
                this.modifyStretchLine.object = object;
                this.modifyStretchLine.update(relative);
                
            }
            else if( object.name == 'Circle' ) {
                this.modifyStretchCircle.object = object;
                this.modifyStretchCircle.update(relative);
            }
            else if( object.name == 'Ellipse' ) {
                this.modifyStretchEllipse.object = object;
                this.modifyStretchEllipse.update(relative);
            }
            else if( object.name == 'Arc' ) {
                this.modifyStretchArc.object = object;
                this.modifyStretchArc.update(relative);
            }

            this.dimensionsShape.update( object, this.engServ );

            //if any line is already drawn, check if endpoint or midpoint is enabled to show line point square 
            //if any line alerady exists in group
            if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {

                if( this.MS.footer_lineEditor.INTERSECTION 
                    || this.MS.footer_lineEditor.PERPENDICULAR || this.MS.footer_lineEditor.NEAREST 
                    || this.MS.footer_lineEditor.END_POINT || this.MS.footer_lineEditor.MID_POINT ) {
                    
                        this.FS.showLinePointSquare(e, object, this.engServ);

                }

            }
            
        }
        //multiple objects are selected
        else if( this.selection.selectedObjects.length > 1 && this.selection.selected && this.dragging ) {

            this.modifyStretchLine.updateMultiLines(relative);
            
        }
    }

    onStop() {

        //if line of group of objects is already selected, un-select it
        if( this.selection.selected && this.selection.selectedObjects.length > 0) {
            this.removeEdges();
            this.engServ.disableDragControls();
            this.engServ.updateSessionStorage();
        } 


    }



    private addEdgesforMultipleObjects() {

        
        let edgeX1:number;
        let edgeY1:number;
        let edgeX2:number = null;
        let edgeY2:number = null;
        let edgeX3:number = null;
        let edgeY3:number = null;

        //make an array of edges objects and pass them to DragControls
        //Only two objects are going to move
        let objects:any = [];
            

        for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {

            let object:any = this.selection.selectedObjects[i];

            //object already has edges, skip current object
            if( object.getObjectByName('LineEdge1') ) {
                continue;
            }

            const shapeArray:any = object.geometry.attributes.position.array;

            //show edges on line ends
            if( object.name == 'Line' ) {
                edgeX1 = shapeArray[0];
                edgeY1 = shapeArray[1];
                edgeX2 = shapeArray[3];
                edgeY2 = shapeArray[4];
            }
            //show one edge on circle and center 
            else if( object.name == 'Circle' ) {
                edgeX1 = shapeArray[0];
                edgeY1 = shapeArray[1];
                edgeX2 = object.geometry.boundingSphere.center.x;
                edgeY2 = object.geometry.boundingSphere.center.y;
            }
            //show one edge on ellipse length and one on width 
            else if( object.name == 'Ellipse' ) {
                edgeX1 = shapeArray[0];
                edgeY1 = shapeArray[1];

                const quarter:number = 3 * Math.round( ( shapeArray.length / 4 ) / 3 );

                edgeX2 = shapeArray[ quarter ];
                edgeY2 = shapeArray[ quarter + 1 ];
            }
            //show edges on arc ends and one arc quarter 
            else if( object.name == 'Arc' ) {
                edgeX1 = shapeArray[0];
                edgeY1 = shapeArray[1];

                const quarter:number = 3 * Math.round( ( shapeArray.length / 4 ) / 3);

                edgeX2 = shapeArray[ quarter ];
                edgeY2 = shapeArray[ quarter + 1 ];

                edgeX3 = shapeArray[ shapeArray.length - 3 ];
                edgeY3 = shapeArray[ shapeArray.length - 2 ];        
            }

            const edge1Position:Vector3 = new Vector3(edgeX1,edgeY1,0);

            //show line point square
            //line point to be shown at edges of line
            let edge1:Mesh = this.getEdge();
            
            edge1.position.copy( edge1Position ); 

            edge1.userData.touch = object.userData.touch;
            
            object.add( edge1 );
            objects.push( edge1 );  

            //if object has 2 edges
            if( ( edgeX2 != null ) && ( edgeY2 != null ) ) {
                const edge2Position:Vector3 = new Vector3(edgeX2,edgeY2,0);
                let edge2:Mesh = edge1.clone();
                edge2.name = 'LineEdge2';
                edge2.position.copy( edge2Position );
                edge2.userData.touch = object.userData.touch;

                object.add( edge2 );
                objects.push( edge2 );
            }
        
            //if object has 3 edges
            if( ( edgeX3 != null ) && ( edgeY3 != null ) ) {
                const edge3Position:Vector3 = new Vector3(edgeX3,edgeY3,0);
                let edge3:Mesh = edge1.clone();
                edge3.name = 'LineEdge3';
                edge3.position.copy( edge3Position );
                object.add( edge3 );
                objects.push( edge3 );
            }

        }

        this.engServ.enableDragControls( objects );  

        let scope = this;
        //update line when line edge is dragging
        this.engServ.dragControls.addEventListener( 'dragstart', function ( event ) {
            if( event.object.name == 'LineEdge1' || event.object.name == 'LineEdge2' || event.object.name == 'LineEdge3' ) {
                scope.modifyStretchLine.object = event.object;
                scope.dragging = true;
                scope.selection.disable();
            }
        } );

        //update line when line edge is dragged
        this.engServ.dragControls.addEventListener( 'dragend', function ( event ) {
            if( event.object.name == 'LineEdge1' || event.object.name == 'LineEdge2' || event.object.name == 'LineEdge3' ) {
                scope.modifyStretchLine.object = null;
                scope.dragend = true;
                scope.dragging = false;
                scope.selection.enable();
            }
        } );

    }

    private setCommonEdgesForMultipleObjects() {

        for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {

        }

    }

    //add edges for a single object
    private addEdges() {

        let edgeX1:number;
        let edgeY1:number;
        let edgeX2:number = null;
        let edgeY2:number = null;
        let edgeX3:number = null;
        let edgeY3:number = null;

        let object:any = this.selection.selectedObjects[0];

        const shapeArray:any = object.geometry.attributes.position.array;

        //show edges on line ends
        if( object.name == 'Line' ) {
            edgeX1 = shapeArray[0];
            edgeY1 = shapeArray[1];
            edgeX2 = shapeArray[3];
            edgeY2 = shapeArray[4];
        }
        //show one edge on circle and center 
        else if( object.name == 'Circle' ) {
            edgeX1 = shapeArray[0];
            edgeY1 = shapeArray[1];
            edgeX2 = object.geometry.boundingSphere.center.x;
            edgeY2 = object.geometry.boundingSphere.center.y;
        }
        //show one edge on ellipse length and one on width 
        else if( object.name == 'Ellipse' ) {
            edgeX1 = shapeArray[0];
            edgeY1 = shapeArray[1];

            const quarter:number = 3 * Math.round( ( shapeArray.length / 4 ) / 3 );

            edgeX2 = shapeArray[ quarter ];
            edgeY2 = shapeArray[ quarter + 1 ];
        }
        //show edges on arc ends and one arc quarter 
        else if( object.name == 'Arc' ) {
            edgeX1 = shapeArray[0];
            edgeY1 = shapeArray[1];

            const quarter:number = 3 * Math.round( ( shapeArray.length / 4 ) / 3);

            edgeX2 = shapeArray[ quarter ];
            edgeY2 = shapeArray[ quarter + 1 ];

            edgeX3 = shapeArray[ shapeArray.length - 3 ];
            edgeY3 = shapeArray[ shapeArray.length - 2 ];        
        }
        
        const edge1Position:Vector3 = new Vector3(edgeX1,edgeY1,0);
        
        
        //show line point square
        //line point to be shown at edges of line
        let edge1:Mesh = this.getEdge();
        
        edge1.position.copy( edge1Position );
       
        object.add( edge1 );
        
        //make an array of edges objects and pass them to DragControls
        //Only two objects are going to move
        let objects:any = [];
        objects.push( edge1 );       

        //if object has 2 edges
        if( ( edgeX2 != null ) && ( edgeY2 != null ) ) {
            const edge2Position:Vector3 = new Vector3(edgeX2,edgeY2,0);
            let edge2:Mesh = edge1.clone();
            edge2.name = 'LineEdge2';
            edge2.position.copy( edge2Position );
            object.add( edge2 );
            objects.push( edge2 );
        }
    
        //if object has 3 edges
        if( ( edgeX3 != null ) && ( edgeY3 != null ) ) {
            const edge3Position:Vector3 = new Vector3(edgeX3,edgeY3,0);
            let edge3:Mesh = edge1.clone();
            edge3.name = 'LineEdge3';
            edge3.position.copy( edge3Position );
            object.add( edge3 );
            objects.push( edge3 );
        }


        this.engServ.enableDragControls( objects );  

        let scope = this;
        //update line when line edge is dragging
        this.engServ.dragControls.addEventListener( 'dragstart', function ( event ) {
            if( event.object.name == 'LineEdge1' || event.object.name == 'LineEdge2' || event.object.name == 'LineEdge3' ) {
                scope.dragging = true;
                scope.selection.disable();
            }
        } );

        //update line when line edge is dragged
        this.engServ.dragControls.addEventListener( 'dragend', function ( event ) {
            if( event.object.name == 'LineEdge1' || event.object.name == 'LineEdge2' || event.object.name == 'LineEdge3' ) {
                scope.dragend = true;
                scope.dragging = false;
                scope.selection.enable();
            }
        } );


    } 

    private removeEdges() {
        //check if Line exists
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') || this.engServ.getSelectedGeometriesGroup().getObjectByName('Circle') || this.engServ.getSelectedGeometriesGroup().getObjectByName('Ellipse') || this.engServ.getSelectedGeometriesGroup().getObjectByName('Arc') ) {
            //remove all line edges from Line Objects
            let engGroup = this.engServ.getSelectedGeometriesGroup(); 
            for (let i:number = 0; i < engGroup.children.length; i++) {
                //if valid object
                if( engGroup.children[i].getObjectByName('Line') || this.engServ.getSelectedGeometriesGroup().getObjectByName('Circle') || this.engServ.getSelectedGeometriesGroup().getObjectByName('Ellipse') || this.engServ.getSelectedGeometriesGroup().getObjectByName('Arc') ) {
                    let object:Object3D = engGroup.children[i];
                    //if Line Edge exists then remove both edges
                    let lineEdge1:Object3D = object.getObjectByName('LineEdge1');
                    object.remove( lineEdge1 );
                    let lineEdge2:Object3D = object.getObjectByName('LineEdge2');
                    object.remove( lineEdge2 );
                    //remove 1 more edge
                    if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Arc') ) {
                        let lineEdge3:Object3D = object.getObjectByName('LineEdge3');
                        object.remove( lineEdge3 );
                    }
               
                }

                
            }
        }

    }

    getEdge():Mesh {
        const geometry:BoxBufferGeometry = new BoxBufferGeometry( 10, 10, 0);
        const material:MeshBasicMaterial = new MeshBasicMaterial( {color: this.MS.color.LINE_POINT, wireframe: false, opacity: 0.5, transparent: true} );
        let edge:Mesh = new Mesh( geometry, material );
        edge.name = 'LineEdge1';
        edge.renderOrder = 2;
        let scale:number = this.FS.get_line_point_scale(this.engServ.camera);
        edge.scale.set( scale, scale , 1 ); 
        return edge;
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" || name == "Circle" || name == "Ellipse" || name == "Arc" )
            return true;
        else    
            return false;    
    }

}
