import { BufferGeometry, BufferAttribute, LineBasicMaterial, Line, Vector3 } from 'three';
import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Subscription } from 'rxjs';

export class Align {

    line:any;
    count:number = 0;
    MAX_POINTS:number = 2;
    positions:any;

    subscription: Subscription;

    constructor( private engServ:EngineService, private MS:MessageService, private FS:FunctionService ) {
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if (message == "ALIGN_IMAGE") {
              this.alignImage();
            }
        });
    }

    onMouseDown( relative:any ) {

        if(this.count > this.MAX_POINTS) {
            this.removeLine();
        }        
       
        // on first click add an extra point
        if( this.count === 0 ){
            const geometry:BufferGeometry = this.lineGeometry();
            this.line = this.lineShape( geometry );
            this.line.name = 'Align';
            this.line.renderOrder = 1;
            this.engServ.commonGroup.add(this.line);
            this.addPoint(relative);
        }
        this.addPoint(relative);
        
        if(this.count == 3) {

            this.alignImage();
        }
            
    }

    loadAlignmentLineFromSCD( relative:Vector3 ) {  
  
        if( this.count === 0 ){
            const geometry:BufferGeometry = this.lineGeometry();
            this.line = this.lineShape( geometry );
            this.line.name = 'Align';
            this.line.renderOrder = 1;
            this.engServ.commonGroup.add(this.line);
            this.addPoint(relative);
        }
        else if( this.count == 1 ) {
            this.addPoint(relative);
        }
        
        if( this.count == 2 ) {
    
            this.alignImage();
            this.count = 3;
        }
                   
    }


    onMouseMove( relative:any ) {
        if( this.count !== 0 ){
            if (!relative) {
                return;
            }
            this.updateLine(relative);
        }
    }

    alignImage() {

        let image:any = this.engServ.commonGroup.getObjectByName('Image');

        if( image == undefined ) { 
            return;
        }

        // angle in degrees
        let scaleLineAngleDeg:number = this.FS.get_angle_between_points(this.positions[0], this.positions[1], this.positions[3], this.positions[4])

        //if scale line rotation is greater that 360 or less than -360, then get different from 360 or -360 and set rotation with subject to 0 
        if( scaleLineAngleDeg >= 360 ) {
            const diff = scaleLineAngleDeg - 360;
            image.rotateZ( this.FS.degrees_to_radians(diff * -1) );
        }
        else if ( scaleLineAngleDeg <= -360 ) {
            const diff = scaleLineAngleDeg + 360;
            image.rotateZ( this.FS.degrees_to_radians(diff * -1) );
        }
        else {
            image.rotateZ( this.FS.degrees_to_radians(scaleLineAngleDeg * -1) );
        }

        this.line.material.color.setHex( this.MS.color.ALIGN_LINE );

        this.engServ.updateSessionStorage();
    }

    

    lineGeometry():BufferGeometry {
        // geometry
        let geometry = new BufferGeometry();
        this.positions = new Float32Array(this.MAX_POINTS * 3);
        geometry.addAttribute('position', new BufferAttribute(this.positions, 3));
        return geometry;
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        var material = new LineBasicMaterial({
            color: this.MS.color.HIGHLIGHT_LINE
        });
        // line
        const line = new Line(geometry, material);
        return line;
    }

    // update line
    updateLine(relative) {
        this.positions[this.count * 3 - 3] = relative.x;
        this.positions[this.count * 3 - 2] = relative.y + 0.0;
        this.positions[this.count * 3 - 1] = relative.z;
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    //remove line
    removeLine() {
        this.count = 0;
        //remove Align line if already drawn
        try{
            this.engServ.commonGroup.remove(this.line);
        }
        catch(e){}
    }
 
    // add point
    addPoint( relative:Vector3 ){
        this.positions[this.count * 3 + 0] = relative.x;
        this.positions[this.count * 3 + 1] = relative.y + 0.0;
        this.positions[this.count * 3 + 2] = relative.z;
        this.count++;
        this.line.geometry.setDrawRange(0, this.count);
        this.updateLine(relative);
        
    }

  

    show() {
        if( this.line )
            this.line.visible = true;
    }

    hide() {
        if( this.line )
            this.line.visible = false;
    }

}
