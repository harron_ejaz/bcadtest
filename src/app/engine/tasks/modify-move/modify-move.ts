import { Vector3, BufferGeometry, LineBasicMaterial, Line, Group } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from '../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Selection } from '../selection';
import { DimensionsShape } from '../../shapes/dimensions-shape';

export class ModifyMove {

    private movingObjects:any = null;
    private objectOffset:Vector3;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection  ) {
    }

    onMouseDown( relative:Vector3 ) {
        //if line is not selected
        if( !this.selection.selected ) {
            this.selection.enable();
        }
    }

    onClick( relative:Vector3 ) {
        
        //object is selected using selection
        if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.movingObjects == null ) {
            //add a moving line object
            this.movingObjects = new Group();
            //add objects in a group
            for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {
                //add a moving line object
                const geometry:BufferGeometry = this.selection.selectedObjects[i].geometry;
                let movingObject:any = this.lineShape( geometry );
                movingObject.copy( this.selection.selectedObjects[i] );
                let color:any = this.FS.get_line_type( movingObject.userData.LineTypesRef ).COLOR;
                movingObject.material.color.set( color ); 
                this.movingObjects.add( movingObject );
            }
            this.engServ.addToScene(this.movingObjects);
            //offset position is the difference from (0,0,0) cordinate to mouse position
            this.objectOffset = relative.clone();
            
        }
        
        //movement completed of group of objects
        else if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.movingObjects != null ) {
            for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {
                let object:any = this.selection.selectedObjects[i];
                object.position.copy( this.movingObjects.position );
                this.dimensionsShape.update( object, this.engServ );
            }
            this.engServ.removeFromScene(this.movingObjects);
            this.movingObjects = null;
            this.engServ.updateSessionStorage();
        }
    }

    onMouseMove( relative:Vector3, e:any ) {
    
        //move object
        if( this.selection.selected && this.movingObjects != null ) 
            this.move( relative ); 
            
        //if any line is already drawn, check if endpoint or midpoint is enabled to show line point square 
        //if any line already exists in group
        if( this.engServ.getSelectedGeometriesGroup().getObjectByName('Line') ) {
            //this.object is required only for this.MS.footer_lineEditor.PERPENDICULAR
            //otherwise null is as a parameter
            if( this.MS.footer_lineEditor.INTERSECTION || this.MS.footer_lineEditor.NEAREST 
                || this.MS.footer_lineEditor.END_POINT || this.MS.footer_lineEditor.MID_POINT ) {
                
                    this.FS.showLinePointSquare(e, null, this.engServ);

            }

        }    
    }

    onStop() {
         
         this.engServ.removeFromScene(this.movingObjects);
         this.movingObjects = null;

    }

    move( relative:Vector3 ) {

        //if Grid or Radial button is enabled in Line Editor page's footer
        if( this.MS.footer_lineEditor.GRID || this.MS.footer_lineEditor.RADIAL ) {
            let movementLength:number;

            if( this.MS.footer_lineEditor.GRID )
                movementLength = this.FS.getGridSnapLineLength( this.objectOffset.x, this.objectOffset.y, relative.x, relative.y );
            else
                movementLength = this.FS.get_distance_between_points( this.objectOffset.x, this.objectOffset.y, relative.x, relative.y );

            var angleBetweenPoints:number;    
            angleBetweenPoints = this.FS.get_angle_between_points( this.objectOffset.x, this.objectOffset.y, relative.x, relative.y );     
            
            //if Radial button is enabled in Line Editor page's footer
            if( this.MS.footer_lineEditor.RADIAL )
                angleBetweenPoints = this.FS.get_angle_radial(angleBetweenPoints, this.MS.settings.RADIAL_DEGREES);
            
            var gridSnapPoint:Vector3;
            gridSnapPoint = this.FS.get_point_from_angle_distance( this.objectOffset.x, this.objectOffset.y, angleBetweenPoints,  movementLength );
        
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                gridSnapPoint.copy( this.engServ.linePointSquare.position );
            }

            this.movingObjects.position.copy( gridSnapPoint.sub( this.objectOffset ) );

        }
        else {
            //if line end point or mid point is enabled and line point square is visible
            //means mouse is over end point or midpoint
            //then add point will use line point square position
            if( this.engServ.linePointSquare.visible ){
                relative.copy( this.engServ.linePointSquare.position );
            }

            this.movingObjects.position.copy( relative.sub( this.objectOffset ) );
        }
        
    }

    

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineBasicMaterial = new LineBasicMaterial(
            { color: this.MS.color.REF_LINE }
        );
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" || name == "Circle" || name == "Ellipse" || name == "Arc" || name == "Text" )
            return true;
        else    
            return false;    
    }


}
