import { Vector3, BufferGeometry, LineBasicMaterial, Line } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from '../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Selection } from '../selection';
import { DimensionsShape } from '../../shapes/dimensions-shape';

export class ModifyExtend {

   
    private extendObject:any = null;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection ) {
    }

    onClick( relative:Vector3 ) {
        //if more than one objct is selected, unselect all and only select currently clicked
     	if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.extendObject )
            this.extend();
    }

    onMouseMove( relative:Vector3, e:any ) {
        //if object is already selected, then highlight the extending object, but not already selected object
        if( this.selection.selected ) {
            this.highlightExtendindObject( e );
        }
    }



   extend() {
    const objactArray = this.selection.selectedObjects[0].geometry.attributes.position.array;
    const objectX1:number = objactArray[0];
    const objectY1:number = objactArray[1];
    const objectX2:number = objactArray[3];
    const objectY2:number = objactArray[4];

    let extendObjectArray = this.extendObject.geometry.attributes.position.array;
    const extendObjectX1:number = extendObjectArray[0];
    const extendObjectY1:number = extendObjectArray[1];
    const extendObjectX2:number = extendObjectArray[3];
    const extendObjectY2:number = extendObjectArray[4];
    
    let intersection:any = this.FS.get_line_intersection( objectX1, objectY1, objectX2, objectY2, extendObjectX1, extendObjectY1, extendObjectX2, extendObjectY2 );

    if( intersection.x == null || intersection.y == null ) {
        return;
    }
     
    //check which point of extending line is closer to intersecting point
    const distance1:number = this.FS.get_distance_between_points( extendObjectX1, extendObjectY1, intersection.x, intersection.y );
    const distance2:number = this.FS.get_distance_between_points( extendObjectX2, extendObjectY2, intersection.x, intersection.y );
    
    if( distance1 < distance2 ) {
        extendObjectArray[0] = intersection.x;
        extendObjectArray[1] = intersection.y;
    }
    else if( distance2 < distance1 ) {
        extendObjectArray[3] = intersection.x;
        extendObjectArray[4] = intersection.y;
    }

    this.extendObject.geometry.attributes.position.needsUpdate = true;
    //bounding sphere is set so that line can be selected using mouse
    this.extendObject.geometry.computeBoundingSphere(); 
    this.dimensionsShape.update( this.extendObject, this.engServ );
   }



    private highlightExtendindObject( e:any ) {
    
        if ( this.extendObject ) {

            if( this.extendObject.id == this.selection.selectedObjects[0].id ) {
                this.extendObject = null;
                return;
            }
                

            if( this.isValidObject( this.extendObject.name ) ) {
                //un-highlight object
                this.extendObject.material.color.set( this.MS.color.LINE );
                this.extendObject = null;
            }
        }
        this.extendObject = this.engServ.getIntersects( e.clientX, e.clientY, this.engServ.getSelectedGeometriesGroup() );

        if( this.extendObject ) {

            if( this.extendObject.id == this.selection.selectedObjects[0].id )
                return;
                
            if( this.isValidObject( this.extendObject.name ) ) {
                //highlight object
                this.extendObject.material.color.set( this.MS.color.HIGHLIGHT_LINE );
            }
        }
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineBasicMaterial = new LineBasicMaterial(
            { color: this.MS.color.REF_LINE }
        );
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" )
            return true;
        else    
            return false;    
    }
    
}
