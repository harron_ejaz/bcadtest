import { EngineService } from '../../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Vector3 } from 'three';
import { Subscription } from 'rxjs';

export class ModifyDimensions {

    private object:any = null;
    private selected:boolean = false;

    subscription:Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {
        
        this.subscription = this.MS.getMessage().subscribe((message) => {

            if (message == "DISABLE_ALL_MENU_BUTTONS" || message == "DISABLE_EDIT_DIMS") {
                if( this.object || this.selected ) {
                    this.disable();
                }
            }
    
        });

    }

    onClick( relative:Vector3 ) {
        
        //if dimension is already highlighted, and object is not already selected
        //then convert it to a dash line when clicked on it
        if( this.object && !this.selected ) {
            if( this.isValidObject( this.object.name ) ) {
                this.selected = true;
            } 
        }
        //movement completed
        else if( this.object && this.selected ) {
            this.object.position.copy( relative );
            this.object = null;
            this.selected = false;
        }
    }

    onMouseMove( relative:Vector3, e:any ) {
        //if no dimension has already been selected
        //then highlight a selected dimension on canvas
        if( !this.selected ) {
            this.highlight( relative );
        }
        //move dimension
        else if( this.selected && this.object ) 
            this.move( relative ); 
    }

    move( relative:Vector3 ) {
        this.object.position.copy( relative );
    }

     private highlight( relative:Vector3 ) {
        if ( this.object ) {
            if( this.isValidObject( this.object.name ) ) {
                //un-highlight object
                this.object.element.lastChild.style.backgroundColor = null;
                this.object = null;
            }
        }
        this.object = this.getIntersects( relative );
        if( this.object ) {
            if( this.isValidObject( this.object.name ) ) {
              //highlight object
              this.object.element.lastChild.style.backgroundColor = 'rgb(' + 255 + ',' + 0 + ',' + 0 + ',' + 0.5 +')';
            }
        }
    }

    getIntersects( relative:Vector3 ) {
        let group:any = this.engServ.dimensionsGroup;
        for(let i=0; i<group.children.length; i++) {
            let object = group.children[i];
            let x1:number = object.position.x;
            let y1:number = object.position.y;
            x1 = parseInt(x1.toString());
            y1 = parseInt(y1.toString());
            let x2 = parseInt(relative.x.toString());
            let y2 = parseInt(relative.y.toString());
      
            //check if mouse touches dimension
            if( Math.abs( x1 - x2 ) < 2 && Math.abs( y1 - y2 ) < 2 ) {
                return object;
            }
        }
        return null;
    }

    disable() {
        this.object.element.lastChild.style.backgroundColor = null; 
        this.selected = false;
        this.object = null;
    }

    isValidObject( name:string ):boolean {
        if( name == "Dimensions" )
            return true;
        else    
            return false;    
    }

}
