import { MessageService } from 'src/app/services/message.service';
import { EngineService } from '../engine.service';
import { Subscription } from 'rxjs';
import { Selection } from './selection';
import { Vector3, Group, BufferGeometry, LineBasicMaterial, Line, LineDashedMaterial } from 'three';
import { FunctionService } from 'src/app/services/function.service';
import { DimensionsShape } from '../shapes/dimensions-shape';

export class Clipboard {

    subscription: Subscription;
    
    enabled:boolean = false;
    clipboad_cut:boolean = false;
    clipboad_copy:boolean = false;
    
    private movingObjects:any = null;

    constructor( private engServ:EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection ) { 
        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe((message) => {
    
            if (message == "CLIPBOARD_CUT") {
                this.cut();
            }
            else if (message == "CLIPBOARD_COPY") {
                this.copy();
            }
            else if (message == "CLIPBOARD_DELETE") {
                this.delete();
            }
            else if (message == "CLIPBOARD_PASTE") {
                this.paste();
            }
    
        });
    }

    onClick( relative:Vector3 ) {

        if( this.enabled && this.movingObjects != null ) {
            
            if( this.clipboad_cut || this.clipboad_copy ) {

                for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {

                    let object:any = this.selection.selectedObjects[i];
                    

                    if( this.clipboad_cut ) {
                        object.position.copy( this.movingObjects.position );
                        //bounding sphere is set so that line can be selected using mouse
                        
                        this.dimensionsShape.update( object, this.engServ );
                        
                    }
                    else if( this.clipboad_copy ) {
                        //add a moving line object
                        const geometry:BufferGeometry = object.geometry.clone();
                        let newObject:any = this.lineShape( geometry );
                        newObject.copy( object );
                        this.selection.unselect( newObject );
                        newObject.position.copy( this.movingObjects.position );
                        
                        this.engServ.addToScene(newObject);
                        this.dimensionsShape.update( object, this.engServ );
                        this.dimensionsShape.update( newObject, this.engServ );
                    }
                    
                }    
                
                let scope=this;
                setTimeout(function(){
                    scope.falseAll();
                },100);
            }
    
        }

    }

    onMouseMove( relative:Vector3 ) {

        if( this.movingObjects != null ) {

            if( this.clipboad_cut || this.clipboad_copy ) {
                this.movingObjects.position.copy( relative );
                this.movingObjects.position.setZ( 0 );
            }
    
        }
        
    }

    onStop() {
        if( this.clipboad_cut || this.clipboad_copy ) {
            this.falseAll();
        }
    }

    private cut() {
        if(this.selection.selected) {
            this.enabled = true;
            this.clipboad_cut = true;
            this.clipboad_copy = false;
        }
    }
    
    private copy() {
        if(this.selection.selected) {
            this.enabled = true;
            this.clipboad_copy = true;
            this.clipboad_cut = false;
        }
    }

    private paste() {
        if(this.selection.selected) {

            if( this.clipboad_cut || this.clipboad_copy ) {
                this.addMovingObject();
            }

        }
    }
    
    private delete() {
        if(this.selection.selected) {
            for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {
                let object:any = this.selection.selectedObjects[i];
                this.engServ.removeFromScene( object );
            }
            this.selection.selected = false;
            this.selection.selectedObjects = [];
            this.MS.sendMessage("REMOVE_ALL_DIMENSIONS");
            this.MS.sendMessage("ADD_ALL_DIMENSIONS");
            this.falseAll();
        }
    }

    private addMovingObject() {
        if(this.movingObjects) {
            this.engServ.removeFromScene(this.movingObjects);
            this.movingObjects = null;
        }
        //add a moving line object
        this.movingObjects = new Group();
        //add objects in a group
        for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {
            let object:any = this.selection.selectedObjects[i];
            //add a moving line object
            const geometry:BufferGeometry = object.geometry;
            let movingObject:any = this.lineShape( geometry );
            movingObject.copy( object );
            this.movingObjects.add( movingObject );
        }
        this.engServ.addToScene(this.movingObjects);
    }

    private falseAll() {
        this.enabled = false;
        this.clipboad_cut = false;
        this.clipboad_copy = false;
        this.engServ.removeFromScene(this.movingObjects);
        this.movingObjects = null;
        for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {
            let object:any = this.selection.selectedObjects[i];
            this.selection.unselect( object );
        }
        this.selection.selectedObjects = [];
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineDashedMaterial = new LineDashedMaterial(
            { color: this.MS.color.REF_LINE }
        );
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }

}
