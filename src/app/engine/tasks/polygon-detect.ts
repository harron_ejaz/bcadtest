import * as turf from '@turf/turf';
import { Shape, ShapeGeometry, MeshBasicMaterial, Mesh, Path, BufferGeometry, LineBasicMaterial, Line } from 'three';
import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Subscription } from 'rxjs';

export class PolygonDetect {

    subscription: Subscription;
    private GeoJSONPolyLine:any;
    public GeoJSONPolygon:any;
    
    constructor( private engServ: EngineService,private MS: MessageService, private FS:FunctionService ) {
        this.subscription = this.MS.getMessage().subscribe((message) => {

            if ( message == "HIDE_ALL_POLYGONS" ) {
                //hide all polygon groups
                this.showHideAllPolgygons(false);
            }
            else if ( message == "POLYGON_DETECT_INIT" ) {
                //show all polygon groups
                this.showHideAllPolgygons(true);
                this.init();
            }
            else if( message == "DETECT_LINE_TOUCH" ) {
                this.detectLineTouch();
            }

        });
    }

    private init() {
        
        this.clearAll();
        this.detectLineTouch();
        this.GeoJSONPolyLine = this.createGeoJSONPolyLine(); 

        this.GeoJSONPolygon = turf.polygonize( this.GeoJSONPolyLine );

        let polygonArray:any = this.GeoJSONPolygon.features;
        if( polygonArray.length > 1 ) {
            polygonArray = this.discardSimilarPolygons( polygonArray );
            polygonArray = this.detectHole( polygonArray );
        }
        if( polygonArray.length > 0 ) {
    
            this.showPolygonWithoutHole( polygonArray );
        }
        
    }

    //remove all polygons from polygon group from all structure groups
    private clearAll() {
        let buildingsGroup:any = this.engServ.rootBuildingsGroup;
        for( let i = 0; i < buildingsGroup.children.length; i++ ) {
            let areasGroup:any = buildingsGroup.children[i].getObjectByName( "AreasGroup" );

            for( let j = 0; j < areasGroup.children.length; j++ ) {

                let polygon:any = areasGroup.children[j].getObjectByName("Polygon");
                if( polygon ) {
                    areasGroup.remove( polygon );
                    j--;
                }
                
            }

        }
        //polygon_id array is used to get polygons when polygons are dropped in left plane of area editor page
        this.MS.area_editor.POLYGON_ID = new Array();
    }

    //if polygon has a hole, polygonize functions create 2 similar polygons for a hole, 
    //1 should be discarded
    private discardSimilarPolygons( polygonArray ):any {

        for( let i = 0; i < polygonArray.length; i++ ) {

            let coordinates1:any = polygonArray[i].geometry.coordinates[0];

            for( let j = 0; j < polygonArray.length; j++ ) {

                //dont check for same polygon
                if( i != j ) {

                    let coordinates2:any = polygonArray[j].geometry.coordinates[0];

                    // create polygon using turf
                    var polygon1 = turf.polygon( [coordinates1] );
                    var polygon2 = turf.polygon( [coordinates2] );

                    // Compare two polygon using turf's booleanEqual method
                    if (turf.booleanEqual(polygon1, polygon2)) {
                        polygonArray = this.FS.remove_from_array(polygonArray, polygonArray[j]);
                        
                    }

                }

            }

        }

        return polygonArray;

    }

    private detectHole( polygonArray ):any {

        for( let i = 0; i < polygonArray.length; i++ ) {

            let coordinates1:any = polygonArray[i].geometry.coordinates[0];
            let hole:boolean = null;

            for( let j = 0; j < polygonArray.length; j++ ) {

                hole = null;

                //dont check for same polygon
                if( i != j ) {

                    hole = true;

                    let coordinates2:any = polygonArray[j].geometry.coordinates[0];

                    for( let k = 0; k < coordinates2.length; k++ ) {

                        let x:number = coordinates2[k][0];
                        let y:number = coordinates2[k][1];

                        let inside:boolean = this.FS.is_point_inside_polygon( [x, y], coordinates1 );
                        
                        //if even 1 point of polygon2 is outside polygon1, 
                        //then polygon1 does not have a hole
                        if( inside == false ) {
                            hole = false;
                            break;
                        }   

                    }

                    if( hole == true ) {

                        polygonArray[i].geometry.coordinates.push( coordinates2 );
                        polygonArray = this.FS.remove_from_array(polygonArray, polygonArray[j]);
                        j--;

                    }

                }

            }

        }

        return polygonArray;

    }

    private showPolygonWithoutHole( polygonArray:any ) {

        for( let i = 0; i < polygonArray.length; i++ ) {

            let shape = new Shape();
            let hole;
            let holeID = [];
            let coordinates:any = polygonArray[i].geometry.coordinates;

            for( let j = 0; j < coordinates.length; j++ ) {
            
                let coordinate = coordinates[j];

                for( let k = 0; k < coordinate.length; k++ ) { 

                    let x:number = coordinate[k][0];
                    let y:number = coordinate[k][1];

                    //main polygon path
                    if( j == 0 ) {

                        if( k == 0) {
                            shape.moveTo( x, y );
                        }
                        else {
                            shape.lineTo( x, y );
                        }

                    }
                    //polygon hole path, draw hole as a new polygon (not as a hole)
                    else if( j > 0 ) {
                      
                        if( k == 0) {
                            hole = new Shape();
                            hole.moveTo( x, y );
                        }
                        else {
                            hole.lineTo( x, y );
                        }

                        //last hole coordinate
                        if( k == coordinate.length - 1 ) {
                            
                            let geometry = new ShapeGeometry( hole );

                            this.setPolygonProperties( hole );

                            let material = new MeshBasicMaterial( { 
                                color: this.MS.color.POLYGON,
                                transparent: true,
                                opacity: 0.2,
                            } );
                            

                            let polygon:Mesh = new Mesh( geometry, material ) ;
                            polygon.name = "Polygon";
                            polygon.userData.shape = hole;
                            polygon.uuid = i + "-" + j;
                            holeID.push( polygon.uuid );
                            this.engServ.addToScene( polygon );

                        }

                    }

                }

            }
  
            let geometry = new ShapeGeometry( shape );

            this.setPolygonProperties( shape );

            let material = new MeshBasicMaterial( { 
                color: this.MS.color.POLYGON,
                transparent: true,
                opacity: 0.2,
            } );

            let polygon:Mesh = new Mesh( geometry, material ) ;
            polygon.name = "Polygon";
            polygon.userData.shape = shape;
            polygon.userData.holeID = holeID;
            polygon.uuid = i.toString();

            this.engServ.addToScene( polygon );

        }

    }

    //detect polygon line with all polylines and set id for polygon lines
    //check if polygon's line is eave. It will be used to calculate IWB
    setPolygonProperties( polygonShape:any ) {

        let curves:any = polygonShape.curves;
        

        let polylineArray:any = this.GeoJSONPolyLine.features;

        for( let i = 0; i < polylineArray.length; i++ ) {
               
                let coordinates1:any = polylineArray[i].geometry.coordinates[0];
                let coordinates2:any = polylineArray[i].geometry.coordinates[1];
                let x1:number = coordinates1[0];
                let y1:number = coordinates1[1];
                let x2:number = coordinates2[0];
                let y2:number = coordinates2[1];
                let line1 = turf.lineString([[x1, y1], [x2, y2]]);

                for( let j = 0; j < curves.length; j++ ) {
                    let x3:number = curves[j].v1.x;
                    let y3:number = curves[j].v1.y;
                    let x4:number = curves[j].v2.x;
                    let y4:number = curves[j].v2.y;
                    let line2 = turf.lineString([[x3, y3], [x4, y4]]);
                    let overlapping = turf.lineOverlap(line2, line1);
               
                    if(overlapping.features.length > 0) { 
                    	curves[j].properties = polylineArray[i].properties;
                	}
                 
                 
                 
                 
                   

            }
        }                
       
    }

    

    //create polyline with all valid touch points in an order of closest 2 touch points
    private createGeoJSONPolyLine() {

        var data = new Array();

        let engGroup:any = this.engServ.getSelectedGeometriesGroup();

        for( let i = 0; i < engGroup.children.length; i++ ) {

            //if line
            if( engGroup.children[i].getObjectByName('Line') ) {
                let line:any = engGroup.children[i];
                //check if a line touch array is valid so that line could be a part of a polygon
                if( this.isValidTouch( line.userData.touch ) ) {
                   
                    this.sortTouch( line );
                    
                    this.addLineStringPoints( line, data );
                }

            }

        }

        return GeoJSON.parse(data, {'LineString': 'line'});

    }

    private addLineStringPoints(line:any, data:any) {

        let touch:any = line.userData.touch;

        for( let i = 0; i < touch.length - 1; i++ ) {

            let touchX1:number = touch[i][0];
            let touchY1:number = touch[i][1];

            let touchX2:number = touch[i+1][0];
            let touchY2:number = touch[i+1][1];

            if( touchX1 != touchX2 || touchY1 != touchY2 ) {
                data.push( { 
                    line : [ [touchX1, touchY1], [touchX2, touchY2] ], 
                    lineUUID : line.uuid,
                    LineTypesRef : line.userData.LineTypesRef 
                } );
            }
            
        }
    }

    //sort userData -> touch
    //sort points starting from one end of line
    //if a line has 2 corner touch and 1 intersection points, 
    //then arrangement is fist edge, intersection point and then second touch point 
    private sortTouch( line:any ) {

        let touch:any = line.userData.touch;
        
        let sortedTouch:any = touch.sort((a, b) => (a[2].distance < b[2].distance ? -1 : 1));
         
        touch = sortedTouch;
    }

    //check if a line touch array is valid so that line could be a part of a polygon
    private isValidTouch( touch:any ) {

        //if line has 2 or more touch points, then it can be part of a polygon
        if( touch.length >= 2 ) {

            //if all line points are same, line can't be a part of a polygon
            for( let i = 1; i < touch.length; i++ ) {
                //if a single touch point is mismatched, then line can be a part of a polygon
                if( touch[0][0] != touch[i][0] || touch[0][1] != touch[i][1] ) {
                    return true;
                }
            }

            return false;
        }
        else
            return false;

    }

    //detect each line's intersection and touch against all other lines and save data in userData -> touch of each line
    private detectLineTouch() {

        var fractionDigits:number = 10;
        let engGroup:any = this.engServ.getSelectedGeometriesGroup();

        for( let i = 0; i < engGroup.children.length; i++) {
            
            //if line
            if( engGroup.children[i].getObjectByName('Line') ) {
                let line1:any = engGroup.children[i];
                        
                //get each line's first and last points
                let line1_Array = line1.geometry.attributes.position.array;

                let line1_X1:number = line1_Array[0];
                let line1_Y1:number = line1_Array[1];
                let line1_X2:number = line1_Array[3];
                let line1_Y2:number = line1_Array[4];

                line1_X1 = parseFloat( line1_X1.toFixed(fractionDigits) );
                line1_Y1 = parseFloat( line1_Y1.toFixed(fractionDigits) );
                line1_X2 = parseFloat( line1_X2.toFixed(fractionDigits) );
                line1_Y2 = parseFloat( line1_Y2.toFixed(fractionDigits) );
    
                var touch = new Array();

                for( let j = 0; j < engGroup.children.length; j++) {

                    let line2:any = engGroup.children[j];

                    //dont check for same line
                    if( line1.id != line2.id ) {console.log(line2)

                        //get each line's first and last points
                        let line2_Array = line2.geometry.attributes.position.array;

                        let line2_X1:number = line2_Array[0];
                        let line2_Y1:number = line2_Array[1];
                        let line2_X2:number = line2_Array[3];
                        let line2_Y2:number = line2_Array[4];

                        line2_X1 = parseFloat( line2_X1.toFixed(fractionDigits) );
                        line2_Y1 = parseFloat( line2_Y1.toFixed(fractionDigits) );
                        line2_X2 = parseFloat( line2_X2.toFixed(fractionDigits) );
                        line2_Y2 = parseFloat( line2_Y2.toFixed(fractionDigits) );

                        //distance of a touch point from line1 first edge point
                        //this distance will help to arrange touch point to make a polygon in order
                        let distance:number;

                        
                        //check if 1st line touches 2nd line from any side (4 possibilities)
                        if( this.FS.check_numbers_equal_nearly(line1_X1, line2_X1) && this.FS.check_numbers_equal_nearly(line1_Y1, line2_Y1) ) {
                            distance = this.FS.get_distance_between_points( line1_X1, line1_Y1, line1_X1, line1_Y1 );
                            touch.push([line1_X1, line1_Y1, {"distance":distance,"line_touch_id":line2.id,"line_intersection_id":null}]);
                        }
                        else if( this.FS.check_numbers_equal_nearly(line1_X2, line2_X2) && this.FS.check_numbers_equal_nearly(line1_Y2, line2_Y2) ) {
                            distance = this.FS.get_distance_between_points( line1_X1, line1_Y1, line1_X2, line1_Y2 );
                            touch.push([line1_X2, line1_Y2, {"distance":distance,"line_touch_id":line2.id,"line_intersection_id":null}]);
                        }
                        else if( this.FS.check_numbers_equal_nearly(line1_X1, line2_X2) && this.FS.check_numbers_equal_nearly(line1_Y1, line2_Y2) ) {
                            distance = this.FS.get_distance_between_points( line1_X1, line1_Y1, line1_X1, line1_Y1 );
                            touch.push([line1_X1, line1_Y1, {"distance":distance,"line_touch_id":line2.id,"line_intersection_id":null}]);
                        }
                        else if( this.FS.check_numbers_equal_nearly(line1_X2, line2_X1) && this.FS.check_numbers_equal_nearly(line1_Y2, line2_Y1) ) {
                            distance = this.FS.get_distance_between_points( line1_X1, line1_Y1, line1_X2, line1_Y2 );
                            touch.push([line1_X2, line1_Y2, {"distance":distance,"line_touch_id":line2.id,"line_intersection_id":null}]);
                        }
                        //when lines edges dont touch, then check if lines intersect
                        else {
                            let intersection:any = this.FS.get_line_intersection( line1_X1, line1_Y1, line1_X2, line1_Y2, line2_X1, line2_Y1, line2_X2, line2_Y2 );
                                       
                            if( intersection.x != null ) {
                                intersection.x = parseFloat( intersection.x.toFixed( fractionDigits ) );
                            } 
                            if( intersection.y != null ) {
                                intersection.y = parseFloat( intersection.y.toFixed( fractionDigits ) );
                            } 
                            
                            //if lines intersect
                            if( intersection.onLine1 == true && intersection.onLine2 == true ) {
                                distance = this.FS.get_distance_between_points( line1_X1, line1_Y1, intersection.x, intersection.y );
                                touch.push([intersection.x, intersection.y, {"distance":distance,"line_touch_id":null,"line_intersection_id":line2.id}]);
                            }

                            

                        }
                        
                    }
                        
                    

                }

                //if line1 has an intersection or line2 touch
                line1.userData.touch = touch;                         
              
            }
            
        }
   
    }

    //show or hide polygon group in all structure groups
    private showHideAllPolgygons( visible:boolean ) {
        let buildingsGroup:any = this.engServ.rootBuildingsGroup.children;
        for( let i = 0; i < buildingsGroup.length; i++ ) {
            try{
                let areasGroup:any = buildingsGroup[i].getObjectByName( "AreasGroup" );
                if(areasGroup)
                    areasGroup.visible = visible; 
            }
            catch(e){
                console.log(e)
            }
            
        }
    }


}
