import { EngineService } from '../../engine.service';
import { MessageService } from './../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Vector3, BufferGeometry, BufferAttribute, LineBasicMaterial, Line, LineDashedMaterial } from 'three';
import { Selection } from '../selection';
import { DimensionsShape } from '../../shapes/dimensions-shape';

export class ModifyOffset {



    private line:any;
    private count:number = 0;
    private MAX_POINTS:number = 3;
    private positions:any;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection ) {
    }

    onMouseDown( relative:Vector3 ) {
        //if line is not selected
        if( !this.selection.selected ) {
            this.selection.enable();
        }
    }

    onClick( relative:Vector3 ) {
        //line is selected, now draw a line for rotation point and reference point on object's geometry
        if( this.selection.selected ) {

            // on first click add an extra point
            if( this.count === 0 ){
                const geometry:BufferGeometry = this.lineGeometry();
                this.line = this.lineShape( geometry );
                this.line.name = 'Line';
                this.line.renderOrder = 1;
                this.engServ.addToScene(this.line);
                this.addPoint(relative);
            }
            this.addPoint(relative);  
            // when offset is completed 
            if(this.count == this.MAX_POINTS) {
                this.count = 0;
                
                this.offset();
            }
        }

    }

    onMouseMove( relative:Vector3, e:any ) {

        //update line when after first click
        if( this.count == 2 )
            this.updateLine(relative);
    }

    onStop() {
            this.engServ.removeFromScene(this.line);
            this.count = 0;
 
    }

    offset() {
        const shapeArray:any = this.line.geometry.attributes.position.array;
        let edgeX1:number = shapeArray[0];
        let edgeY1:number = shapeArray[1];
        let edgeX2:number = shapeArray[3];
        let edgeY2:number = shapeArray[4];
        let offset:Vector3 = new Vector3( edgeX1, edgeY1, 0 ).sub( new Vector3( edgeX2, edgeY2, 0 ) );

        //remove line
        this.engServ.removeFromScene(this.line);

        //offset completed of group of objects
        if( this.selection.selectedObjects.length > 0 && this.selection.selected ) {
            for( let i=0; i < this.selection.selectedObjects.length; i++) {
                const geometry:BufferGeometry = this.selection.selectedObjects[i].geometry.clone();
                const material:LineDashedMaterial = this.selection.selectedObjects[i].material.clone();
                let offsetObject:any = new Line(geometry, material);
                offsetObject.copy( this.selection.selectedObjects[i] );
                let lineType:any = this.FS.get_line_type( offsetObject.userData.LineTypesRef );
                offsetObject.material.color.set( lineType.COLOR ); 
                offsetObject.material.dashSize = 1; 
                offsetObject.material.gapSize = lineType.SOLID; 
                offsetObject.userData.Selected = false;
                //offset position is the difference of reference line
                offsetObject.position.sub( offset );
                this.engServ.addToScene( offsetObject );
                //this.FS.set_world_position( offsetObject );
                this.dimensionsShape.update( offsetObject, this.engServ );
                this.dimensionsShape.update( this.selection.selectedObjects[i], this.engServ );
            }
        }


    }

    

    // update line
    updateLine(relative:Vector3) {
        this.positions[this.count * 3 - 3] = relative.x;
        this.positions[this.count * 3 - 2] = relative.y;
        this.positions[this.count * 3 - 1] = relative.z;
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    // add point
    addPoint(relative:Vector3){
        this.positions[this.count * 3 + 0] = relative.x;
        this.positions[this.count * 3 + 1] = relative.y;
        this.positions[this.count * 3 + 2] = relative.z;
  
        this.count++;
        this.line.geometry.setDrawRange(0, this.count);
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    lineGeometry():BufferGeometry {
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();
        this.positions = new Float32Array( (this.MAX_POINTS - 1) * 3 );
        geometry.setAttribute('position', new BufferAttribute(this.positions, 3));
        return geometry;
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineBasicMaterial = new LineBasicMaterial(
            { color: this.MS.color.REF_LINE }
        );
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" || name == "Circle" || name == "Ellipse" || name == "Arc" )
            return true;
        else    
            return false;    
    }

}
