import { Subscription } from 'rxjs';
import { SelectionHelper } from 'three/examples/jsm/interactive/SelectionHelper';
import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';

export class Selection {

    enabled:boolean = false;
    private selectionBox:any;
    private selectionHelper:SelectionHelper;
    public selected:boolean = false;
    public selectedObjects: Array<any> = [];
    private object:any = null;
    private stopMouseMove:boolean = false;

    subscription: Subscription;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {

        // subscribe to component messages
        this.subscription = this.MS.getMessage().subscribe(message => {


            if(message == "MENU_SELECTION") {

                this.menu_selection();
                
                if( this.MS.menu_selection.SELECT_ALL )
                    this.selectAll();
                else if( this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH )
                    this.selectByLineTypeAndPitch();
                else if( this.MS.menu_selection.SELECT_BY_LINE_TYPE )
                    this.selectByLineType(); 
                else  if( this.MS.menu_selection.SELECT_BY_PITCH )
                    this.selectByPitch(); 
                else
                    this.unselectAll();

            }
            //buildingsGroup_changeHandler in line editor page
            else if (message == "MENU_CURRENT_VALUES_BUILDINGS_GROUP_CHANGE") {
                this.unselectAll();
            }
            else if (message == "ENABLE_MENU_SELECTION_BUTTON") {
                this.MS.menu_selection.SELECTION = true;
                this.menu_selection();
            }
            else if (message == "TO_FRONT") {
                this.toFront();
            }
            else if (message == "TO_BACK") {
                this.toBack();
            }
            else if (message == "HOTKEY_ESC_LINE_EDITOR") {
                this.selection();
            } 


        })

        this.selectionBox = this.engServ.selectionBox;
        this.selectionHelper = this.engServ.selectionHelper;    

    }

    selection() {
        this.onStop();
        this.unselectAll();
        this.MS.sendMessage("SELECTION");
    }

    onMouseDown( e:any ) { 

        this.engServ.selectionHelper.element.className = "selectionBox";

        const position = {
            x: ( ( e.clientX - this.engServ.getCurrentOffset( this.engServ.canvasRef ).left ) / this.engServ.canvasWidth*10 ) * 2 - 1,
            y: -( ( e.clientY - this.engServ.getCurrentOffset( this.engServ.canvasRef ).top ) / this.engServ.canvasHeight*10 ) * 2 + 1
        };

        this.selectionBox.startPoint.set( position.x, position.y, 0.5 );

    }

    onClick( e:any ) {
        //if line is already highlighted, and object is not already selected (dashed line)
        //then convert it to a dash line when clicked on it
        if( this.object && this.object.userData.Selected == false ) {
            if( this.isValidObject( this.object.name ) ) {
                this.select( this.object );  
            }
            return true;
        }
        else if( this.selectionHelper.isDown ) {
            const position = {
                x: ( ( e.clientX - this.engServ.getCurrentOffset( this.engServ.canvasRef ).left ) / this.engServ.canvasWidth*10 ) * 2 - 1,
                y: -( ( e.clientY - this.engServ.getCurrentOffset( this.engServ.canvasRef ).top ) / this.engServ.canvasHeight*10 ) * 2 + 1
            };
    
            this.selectionBox.endPoint.set( position.x, position.y, 0.5 );  

            var allSelected = this.selectionBox.select();
    
            for ( var i = 0; i < allSelected.length; i++ ) {
    
                let object:any = allSelected[ i ];
    
                if( this.isValidObject( object.name ) ) {
    
                    //select object
                    this.select( object );
                }            
    
            }
    
            if (allSelected.length > 0 ) {
                return true;    
            }
            else {
                return false;
            }    

            
        }
        
    }

    onMouseMove( e:any ) { 
        //onMouseMove is called after onStop runs
        //onMouseMove will not be run when onStop is called
        if( this.stopMouseMove ) {

            let scope = this;
            setTimeout(() => {
                scope.stopMouseMove = false;
            }, 500);
            
            return;
        }
             

        //if no object has already been selected (dashed line)
        //then highlight a selected object on canvas
        if( this.enabled && !this.selectionHelper.isDown ) {
            this.highlight( e );
        }

        else if ( this.selectionHelper.isDown ) {

            for ( var i = 0; i < this.selectionBox.collection.length; i++ ) {

                this.unhighlightObject( this.selectionBox.collection[ i ] );
                
            }

            const position = {
                x: ( ( e.clientX - this.engServ.getCurrentOffset( this.engServ.canvasRef ).left ) / this.engServ.canvasWidth*10 ) * 2 - 1,
                y: -( ( e.clientY - this.engServ.getCurrentOffset( this.engServ.canvasRef ).top ) / this.engServ.canvasHeight*10 ) * 2 + 1
            };
        
            this.selectionBox.endPoint.set( position.x, position.y, 0.5 );    

            var allSelected:any = this.selectionBox.select();

            for ( var i = 0; i < allSelected.length; i++ ) {
              
                this.highlightObject( allSelected[ i ] );

            }

        }
 

    }

    onStop() {
        this.stopMouseMove = true;
        //if object is already selected, un-select it

        for( let i = 0; i < this.selectedObjects.length; i++ ) {
            //unselect object
            this.unselect( this.selectedObjects[i] );
        }
        this.selectedObjects = [];
    }

    

    private unhighlightObject( object:any ) {
        //object is not already selected
        if( this.isValidObject( object.name ) &&  object.material.dashSize == 0 ) {       
            //un-highlight object
            let color:any = this.FS.get_line_type( object.userData.LineTypesRef ).COLOR;
            object.material.color.set( color );    
        }

    }

    private highlightObject( object:any ) { 
        //object is not already selected
        if( this.isValidObject( object.name ) && object.material.dashSize == 1 ) {
            //highlight object
            object.material.color.set( this.MS.color.HIGHLIGHT_LINE );  
        }

    }

    private highlight( e:any ) {
        if ( this.object && this.object.userData.Selected == false ) {
            if( this.isValidObject( this.object.name ) ) {
                //un-highlight object
                let color:any = this.FS.get_line_type( this.object.userData.LineTypesRef ).COLOR;
                this.object.material.color.set( color );
                this.object = null;
            }
        }
        this.object = this.engServ.getIntersects( e.clientX, e.clientY, this.engServ.getSelectedGeometriesGroup() );
        if( this.object && this.object.userData.Selected == false ) {
            if( this.isValidObject( this.object.name ) ) {
              //highlight object
              this.object.material.color.set( this.MS.color.HIGHLIGHT_LINE );
            }
        }
    }

    public select( object:any ) {
        object.material.color.set( this.MS.color.SELECT_LINE ); 
        object.material.dashSize = 0.3; 
        object.material.gapSize = 0.3; 
        object.userData.Selected = true;
        object.computeLineDistances(); 
        this.selected = true;
        this.selectedObjects.push( object );
        
        
        
    }

    public unselect( object:any ) {
        let lineType:any = this.FS.get_line_type( object.userData.LineTypesRef );
        object.material.color.set( lineType.COLOR );
        object.material.dashSize = 1; 
        object.material.gapSize = lineType.SOLID; 
        object.userData.Selected = false;
        this.FS.set_world_position( object );
        //bounding sphere is set so that line can be selected using mouse
        object.computeLineDistances(); 
        
        this.selected = false;

        this.MS.menu_selection.SELECT_ALL = false;
        this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH = false;
        this.MS.menu_selection.SELECT_BY_LINE_TYPE = false;
        this.MS.menu_selection.SELECT_BY_PITCH = false;
        
        
    }

    private menu_selection() {
        
        if( this.MS.menu_selection.SELECTION ) {
            this.enable();
        }
        else {
            this.disable();
        }

    }

    private selectAll() {

        this.unselectAll();

        let group = this.engServ.getSelectedGeometriesGroup();
        for ( let i = 0; i < group.children.length; i++ ) {
            let object:any = group.children[ i ];
            if( this.isValidObject( object.name ) ) {
                //select object
                this.select( object );
            }   
        }
        this.MS.menu_selection.SELECT_ALL = true;

    }

    private selectByLineTypeAndPitch() {

        let alreadySelected:boolean = this.ifAlreadySelected();

        let group = this.engServ.getSelectedGeometriesGroup();

        for ( let i = 0; i < group.children.length; i++ ) {
            let object:any = group.children[ i ];
            if( this.isValidObject( object.name ) ) {

                //With one specific line type and slope selected, this will select all other lines of the same line type and slope
                if( alreadySelected == true ) {
                    if( object.userData.LineTypesRef == this.selectedObjects[0].userData.LineTypesRef
                        && object.userData.Rise == this.selectedObjects[0].userData.Rise ) {
                        //select object
                        this.select( object );
                    }
                    else {
                        this.unselect( object );
                    }
                }
                else {
                    if( object.userData.LineTypesRef == this.MS.menu_line_editor.LINE_TYPES_REF_SELECTED
                        && object.userData.Rise == this.MS.menu_line_editor.RISE_SELECTED ) {
                        //select object
                        this.select( object );
                    }
                }
             
            }   
        }
        this.MS.menu_selection.SELECT_BY_LINE_TYPE_AND_PITCH = true;
    }

    private selectByLineType() {

        let alreadySelected:boolean = this.ifAlreadySelected();

        let group = this.engServ.getSelectedGeometriesGroup();

        for ( let i = 0; i < group.children.length; i++ ) {
            let object:any = group.children[ i ];
            if( this.isValidObject( object.name ) ) {

                //With one specific line type selected, this will select all other lines of the same line type
                if( alreadySelected == true ) {
                    if( object.userData.LineTypesRef == this.selectedObjects[0].userData.LineTypesRef ) {
                        //select object
                        this.select( object );
                    }
                }
                else {
                    if( object.userData.LineTypesRef == this.MS.menu_line_editor.LINE_TYPES_REF_SELECTED ) {
                        //select object
                        this.select( object );
                    }
                }
             
            }   
        }
        this.MS.menu_selection.SELECT_BY_LINE_TYPE = true;

    }

    private selectByPitch() {

        let alreadySelected:boolean = this.ifAlreadySelected();

        let group = this.engServ.getSelectedGeometriesGroup();

        for ( let i = 0; i < group.children.length; i++ ) {
            let object:any = group.children[ i ];
            if( this.isValidObject( object.name ) ) {

                //With one specific slope selected, this will select all other lines of the same slope
                if( alreadySelected == true ) {
                    if( object.userData.Rise == this.selectedObjects[0].userData.Rise ) {
                        //select object
                        this.select( object );
                    }
                }
                else {
                    if( object.userData.Rise == this.MS.menu_line_editor.RISE_SELECTED ) {
                        //select object
                        this.select( object );
                    }
                }
                
            }   
        }
        this.MS.menu_selection.SELECT_BY_PITCH = true;

    }

    private ifAlreadySelected() {
        let alreadySelected:boolean;
        //With one specific line type selected, this will select all other lines of the same line type
        if( this.selectedObjects.length > 0 ) {
            alreadySelected = true;
        }
        else {
            alreadySelected = false;
            this.unselectAll();
        }
        return alreadySelected;
    }
    
    public unselectAll() {
        if( !this.selected ) return;

        let rootGroup:any = this.engServ.rootBuildingsGroup;
        for ( let i = 0; i < rootGroup.children.length; i++ ) {
            let buildingsGroup:any = rootGroup.children[ i ];
            let geometriesGroup:any = buildingsGroup.getObjectByName( "GeometriesGroup" );
      
            for ( let j = 0; j < geometriesGroup.children.length; j++ ) {
                if( this.isValidObject( geometriesGroup.children[ j ].name ) ) {
                    //unselect object
                    this.unselect( geometriesGroup.children[ j ] );
                }   
            }
        }
        this.selectedObjects = [];
        
    } 
    
    enable() {
        
        this.enabled = true;
    }
    
    disable() {
        this.engServ.selectionHelper.element.className = "selectionBoxHide";
        this.enabled = false;
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" || name == "Circle" || name == "Ellipse" || name == "Arc" )
            return true;
        else    
            return false;    
    }

    toFront() {
        if( this.selectedObjects.length > 0 ) {
            for ( let i = 0; i < this.selectedObjects.length; i++ ) {
                this.selectedObjects[i].renderOrder = this.selectedObjects[i].renderOrder + 1;
            }
            this.unselectAll();
        }
    }

    toBack() {
        if( this.selectedObjects.length > 0 ) {
            for ( let i = 0; i < this.selectedObjects.length; i++ ) {
                if( this.selectedObjects[i].renderOrder > 0 ) {
                    this.selectedObjects[i].renderOrder = this.selectedObjects[i].renderOrder - 1;
                }   
            }
            this.unselectAll();
        }
    }

}
