import { Vector3, BufferGeometry, ArcCurve, LineDashedMaterial, Line } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from '../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';

export class ModifyTrimCircle {

    object:any;
    trimObject:any;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {
    }

    trim( relative:Vector3 ) {

        //trim circle
        if( this.object.name == 'Line' && this.trimObject.name == 'Circle' ) {

            const objectArray = this.object.geometry.attributes.position.array;
            const objectX1:number = objectArray[0];
            const objectY1:number = objectArray[1];
            const objectX2:number = objectArray[3];
            const objectY2:number = objectArray[4];

            const trimObjectBoundingSphere = this.trimObject.geometry.boundingSphere;

            let intersection:any = this.get_line_circle_intersection( trimObjectBoundingSphere, objectX1, objectY1, objectX2, objectY2 );
            
            //if line intersects circle at 2 points
            if( intersection.length == 2 ) {
                
                //replace circle with arc
                this.engServ.removeFromScene( this.trimObject );
                const geometry:BufferGeometry = this.arcGeometry( trimObjectBoundingSphere, intersection, relative );
                this.trimObject = this.arcShape( geometry );
                this.trimObject.name = 'Arc';

                this.engServ.addToScene(this.trimObject);

            }

        }
        //trim line
        else if( this.object.name == 'Circle' && this.trimObject.name == 'Line' ) {

            const objectBoundingSphere = this.object.geometry.boundingSphere;

            const trimObjectArray = this.trimObject.geometry.attributes.position.array;
            const trimObjectX1:number = trimObjectArray[0];
            const trimObjectY1:number = trimObjectArray[1];
            const trimObjectX2:number = trimObjectArray[3];
            const trimObjectY2:number = trimObjectArray[4];

            let intersection:any = this.get_line_circle_intersection( objectBoundingSphere, trimObjectX1, trimObjectY1, trimObjectX2, trimObjectY2 );

            //if circle intersects line at 1 point
            if( intersection.length == 1 ) {

                //trim right side of line
                if( this.FS.if_point_is_on_line( trimObjectX1, trimObjectY1, intersection[0].x, intersection[0].y, relative.x, relative.y ) == true ) {
                    trimObjectArray[3] = intersection[0].x;
                    trimObjectArray[4] = intersection[0].y;      
                }
                //trim left side of line
                else if( this.FS.if_point_is_on_line( trimObjectX2, trimObjectY2, intersection[0].x, intersection[0].y, relative.x, relative.y ) == true ) {
                    trimObjectArray[0] = intersection[0].x;
                    trimObjectArray[1] = intersection[0].y;
                }
                
            }
            //if circle intersects line at 2 points
            else if( intersection.length == 2 ) {

                //check an edge of line is closer to which intersting point
                let distance1:number = this.FS.get_distance_between_points( trimObjectX1, trimObjectY1, intersection[0].x, intersection[0].y );
                let distance2:number = this.FS.get_distance_between_points( trimObjectX1, trimObjectY1, intersection[1].x, intersection[1].y );
                var pointOnLine:Vector3 = this.FS.get_closest_point_to_line( trimObjectX1, trimObjectY1, trimObjectX2, trimObjectY2, relative.x, relative.y );
                
                if( distance1 < distance2 ) {

                    //trim left side of line
                    if( this.FS.if_point_is_between_two_points( trimObjectX1, trimObjectY1, intersection[0].x, intersection[0].y, pointOnLine.x, pointOnLine.y ) == true ) {
                        trimObjectArray[0] = intersection[0].x;
                        trimObjectArray[1] = intersection[0].y; 
 
                    }
                    //trim right side of line
                    else if( this.FS.if_point_is_between_two_points( trimObjectX2, trimObjectY2, intersection[1].x, intersection[1].y, pointOnLine.x, pointOnLine.y ) == true ) {
                        trimObjectArray[3] = intersection[1].x;
                        trimObjectArray[4] = intersection[1].y;

                    }
                    //trim middle of line
                    else if( this.FS.if_point_is_between_two_points( intersection[0].x, intersection[0].y, intersection[1].x, intersection[1].y, pointOnLine.x, pointOnLine.y ) == true ) {
                        
                        //create a new line object
                        const geometry:BufferGeometry = this.trimObject.geometry.clone();
                        let newTrimObject:any = this.lineShape( geometry );
                        newTrimObject.name = 'Line';
                        this.engServ.addToScene(newTrimObject);

                        const newTrimObjectArray = newTrimObject.geometry.attributes.position.array;
                        const newTrimObjectX1:number = newTrimObjectArray[0];
                        const newTrimObjectY1:number = newTrimObjectArray[1];
                        const newTrimObjectX2:number = newTrimObjectArray[3];
                        const newTrimObjectY2:number = newTrimObjectArray[4];


                        newTrimObjectArray[0] = intersection[1].x;
                        newTrimObjectArray[1] = intersection[1].y; 

                        newTrimObject.geometry.attributes.position.needsUpdate = true;
                        //bounding sphere is set so that line can be selected using mouse
                        newTrimObject.geometry.computeBoundingSphere();
                  
                        trimObjectArray[3] = intersection[0].x;
                        trimObjectArray[4] = intersection[0].y; 
                        
                    }

                }
                //trimObjectX1,trimObjectY1 -> intersection[1].x,intersection[1].y -> intersection[0].x, intersection[0].y, trimObjectX2, trimObjectY2
                else if( distance2 < distance1 ) {
                    
                    //trim right side of line
                    if( this.FS.if_point_is_between_two_points( trimObjectX1, trimObjectY1, intersection[1].x, intersection[1].y, pointOnLine.x, pointOnLine.y ) == true ) {
                   
                        console.log(4)     
                    }
                    //trim left side of line
                    else if( this.FS.if_point_is_between_two_points( trimObjectX2, trimObjectY2, intersection[0].x, intersection[0].y, pointOnLine.x, pointOnLine.y ) == true ) {
                      
                        console.log(5)
                    }
                    //trim middle of line
                    else if( this.FS.if_point_is_between_two_points( intersection[1].x, intersection[1].y, intersection[0].x, intersection[0].y, pointOnLine.x, pointOnLine.y ) == true ) {
                        console.log(6)
                    }

                }
                
            }

            this.trimObject.geometry.attributes.position.needsUpdate = true;
            //bounding sphere is set so that line can be selected using mouse
            this.trimObject.geometry.computeBoundingSphere();

        }
        
    }

    arcGeometry( circle, intersection, relative:Vector3 ):BufferGeometry {

        const point1:any = intersection[0];
        const point2:any = intersection[1];

        const circleCenter:Vector3 = circle.center;
        const aRadius:number = circle.radius;
        //get start angle of the point where line starts
        const aStartAngle:number = this.FS.get_circle_point_angle( circleCenter.x, circleCenter.y, point2.x, point2.y );
        //get end angle of the point where line ends
        const aEndAngle:number = this.FS.get_circle_point_angle( circleCenter.x, circleCenter.y, point1.x, point1.y );
        //check relative is on what side on line is
        const aClockwise:boolean = this.FS.if_point_is_above_line( point2.x, point2.y, point1.x, point1.y, relative.x, relative.y );

        const curve:ArcCurve = new ArcCurve(
            circleCenter.x, circleCenter.y,        // ax, aY
            aRadius,           // aRadius
            aStartAngle,  aEndAngle,  // aStartAngle, aEndAngle
            aClockwise            // aClockwise
        );
        const points = curve.getPoints( 50 );
  
        // geometry
        const geometry:BufferGeometry = new BufferGeometry().setFromPoints( points );     
        return geometry;
    }

    arcShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineDashedMaterial = new LineDashedMaterial(
            { color: this.MS.color.LINE, dashSize: 0, gapSize: 0 }
        );
        // arc
        const arc:Line = new Line( geometry, material );
           
        return arc;
    }

    //Thus the function to find the intercept of a line segment width a circle
    //The function returns an array of up to two point on the line segment. 
    //If no points found returns an empty array.
    get_line_circle_intersection(circle, lineStartX, lineStartY, lineEndX, lineEndY){
        var a, b, c, d, u1, u2, ret, retP1, retP2, v1, v2;
        v1 = {};
        v2 = {};
        v1.x = lineEndX - lineStartX;
        v1.y = lineEndY - lineStartY;
        v2.x = lineStartX - circle.center.x;
        v2.y = lineStartY - circle.center.y;
        b = (v1.x * v2.x + v1.y * v2.y);
        c = 2 * (v1.x * v1.x + v1.y * v1.y);
        b *= -2;
        d = Math.sqrt(b * b - 2 * c * (v2.x * v2.x + v2.y * v2.y - circle.radius * circle.radius));
        if(isNaN(d)){ // no intercept
            return [];
        }
        u1 = (b - d) / c;  // these represent the unit distance of point one and two on the line
        u2 = (b + d) / c;    
        retP1 = {};   // return points
        retP2 = {}  
        ret = []; // return array
        if(u1 <= 1 && u1 >= 0){  // add point if on the line segment
            retP1.x = lineStartX + v1.x * u1;
            retP1.y = lineStartY + v1.y * u1;
            ret[0] = retP1;
        }
        if(u2 <= 1 && u2 >= 0){  // second add point if on the line segment
            retP2.x = lineStartX + v1.x * u2;
            retP2.y = lineStartY + v1.y * u2;
            ret[ret.length] = retP2;
        }       
        return ret;
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineDashedMaterial = new LineDashedMaterial(
            { color: this.MS.color.LINE, dashSize: 0, gapSize: 0 }
        ); 
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }
}
