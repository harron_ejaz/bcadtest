import { Vector3 } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from '../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';

export class ModifyTrimLine {

    object:any;
    trimObject:any;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService ) {
    }

    trim( relative:Vector3 ) {
        const objectArray = this.object.geometry.attributes.position.array;
        const objectX1:number = objectArray[0];
        const objectY1:number = objectArray[1];
        const objectX2:number = objectArray[3];
        const objectY2:number = objectArray[4];

        let trimObjectArray = this.trimObject.geometry.attributes.position.array;
        const trimObjectX1:number = trimObjectArray[0];
        const trimObjectY1:number = trimObjectArray[1];
        const trimObjectX2:number = trimObjectArray[3];
        const trimObjectY2:number = trimObjectArray[4];
        
        let intersection:any = this.FS.get_line_intersection( objectX1, objectY1, objectX2, objectY2, trimObjectX1, trimObjectY1, trimObjectX2, trimObjectY2 );

        //if both selected line object and trim line object, both intersect each other
        if( intersection.onLine1 == true && intersection.onLine2 == true ) {

            if( this.FS.if_point_is_on_line( trimObjectX1, trimObjectY1, intersection.x, intersection.y, relative.x, relative.y ) == true ) {
                trimObjectArray[3] = intersection.x;
                trimObjectArray[4] = intersection.y;      
            }
            else if( this.FS.if_point_is_on_line( trimObjectX2, trimObjectY2, intersection.x, intersection.y, relative.x, relative.y ) == true ) {
                trimObjectArray[0] = intersection.x;
                trimObjectArray[1] = intersection.y;
            }

            this.trimObject.geometry.attributes.position.needsUpdate = true;
            //bounding sphere is set so that line can be selected using mouse
            this.trimObject.geometry.computeBoundingSphere();

            
        }
    }
}
