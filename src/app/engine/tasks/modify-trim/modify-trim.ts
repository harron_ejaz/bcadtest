import { ModifyTrimEllipse } from './modify-trim-ellipse';
import { Vector3, BufferGeometry, LineBasicMaterial, Line } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from '../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { ModifyTrimLine } from './modify-trim-line';
import { ModifyTrimArc } from './modify-trim-arc';
import { ModifyTrimCircle } from './modify-trim-circle';
import { Selection } from '../selection';
import { DimensionsShape } from '../../shapes/dimensions-shape';

export class ModifyTrim {

   
    private trimObject:any = null;

    private modifyTrimLine:ModifyTrimLine;
    private modifyTrimArc:ModifyTrimArc;
    private modifyTrimCircle:ModifyTrimCircle;
    private modifyTrimEllipse:ModifyTrimEllipse;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection ) {
        this.modifyTrimLine = new ModifyTrimLine( this.engServ, this.MS, this.FS );
        this.modifyTrimArc = new ModifyTrimArc( this.engServ, this.MS, this.FS );
        this.modifyTrimCircle = new ModifyTrimCircle( this.engServ, this.MS, this.FS );
        this.modifyTrimEllipse = new ModifyTrimEllipse( this.engServ, this.MS, this.FS );
    }

    onMouseDown( relative:Vector3 ) {
        //if line is not selected
        if( !this.selection.selected ) {
            this.selection.enable();
        }
    }

    onClick( relative:Vector3 ) {
        //if more than one objct is selected, unselect all and only select currently clicked
        if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.trimObject ) { 

            let object:any = this.selection.selectedObjects[0]

            if( object.name == 'Line' && this.trimObject.name == 'Line' ) {
                this.modifyTrimLine.object = object;
                this.modifyTrimLine.trimObject = this.trimObject;
                this.modifyTrimLine.trim( relative );
            }
            else if( ( object.name == 'Line' && this.trimObject.name == 'Arc' ) || ( object.name == 'Arc' && this.trimObject.name == 'Line' ) ) {
                this.modifyTrimArc.object = object
                this.modifyTrimArc.trimObject = this.trimObject;
                this.modifyTrimArc.trim( relative );
            }
            else if( ( object.name == 'Line' && this.trimObject.name == 'Circle' ) || ( object.name == 'Circle' && this.trimObject.name == 'Line' ) ) {
                this.modifyTrimCircle.object = object
                this.modifyTrimCircle.trimObject = this.trimObject;
                this.modifyTrimCircle.trim( relative );
            }
            else if( object.name == 'Ellipse' && this.trimObject.name == 'Line' ) {
                this.modifyTrimEllipse.object = object
                this.modifyTrimEllipse.trimObject = this.trimObject;
                this.modifyTrimEllipse.trim( relative );
            }

            this.dimensionsShape.update( this.trimObject, this.engServ );

        }
            
    }

    onMouseMove( relative:Vector3, e:any ) {
        //if object is already selected, then highlight the triming object, but not already selected object
        if( this.selection.selected ) {
            this.highlightTrimObject( e );
        }
    }



    

   

    private highlightTrimObject( e:any ) {
    
        if ( this.trimObject ) {

            if( this.trimObject.id == this.selection.selectedObjects[0].id ) {
                this.trimObject = null;
                return;
            }
                

            if( this.isValidObject( this.trimObject.name ) ) {
                //un-highlight object
                this.trimObject.material.color.set( this.MS.color.LINE );
                this.trimObject = null;
            }
        }
        this.trimObject = this.engServ.getIntersects( e.clientX, e.clientY, this.engServ.getSelectedGeometriesGroup() );

        if( this.trimObject ) {

            if( this.trimObject.id == this.selection.selectedObjects[0].id )
                return;
                
            if( this.isValidObject( this.trimObject.name ) ) {
                //highlight object
                this.trimObject.material.color.set( this.MS.color.HIGHLIGHT_LINE );
            }
        }
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineBasicMaterial = new LineBasicMaterial(
            { color: this.MS.color.REF_LINE }
        );
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" || name == "Circle" || name == "Ellipse" || name == "Arc" )
            return true;
        else    
            return false;    
    }
    
}
