import { Vector3, BufferGeometry, BufferAttribute, LineBasicMaterial, Line, Object3D, Group } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from './../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Selection } from '../selection';
import { DimensionsShape } from '../../shapes/dimensions-shape';

export class ModifyRotate {

 
    private rotatingObjects:any = null;

    private line:any;
    private count:number = 0;
    private MAX_POINTS:number = 3;
    private positions:any;

    private rotationPoint:Vector3;
    private axis:Vector3 = new Vector3(0, 0, 1);
    private offsetAngle:number;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection ) {
    }

    onMouseDown( relative:Vector3 ) {
        //if line is not selected
        if( !this.selection.selected ) {
            this.selection.enable();
        }
    }

    onClick( relative:Vector3 ) {   
        //line is selected, now draw a line for rotation point and reference point on object's geometry
         if( this.selection.selected ) {

            // on first click add an extra point
            if( this.count === 0 ){
                const geometry:BufferGeometry = this.lineGeometry();
                this.line = this.lineShape( geometry );
                this.line.name = 'Line';
                this.line.renderOrder = 1;
                this.engServ.addToScene(this.line);
                this.addPoint(relative);
            }
            this.addPoint(relative);  
            // when rotating
            if( this.count === 3 ){

                //remove line
                this.engServ.removeFromScene(this.line);

                this.offsetAngle = this.FS.get_angle_between_points( this.positions[0], this.positions[1], this.positions[3], this.positions[4] );             

                this.rotationPoint = new Vector3( this.positions[0], this.positions[1], 0 );

                //object is selected
                if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.rotatingObjects == null ) {
                    //add a rotating line object
                    this.rotatingObjects = new Group();
                    //add objects in a group
                    for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {
                        //add a rotating line object
                        const geometry:BufferGeometry = this.selection.selectedObjects[i].geometry;
                        let rotatingObject:any = this.lineShape( geometry );
                        rotatingObject.copy( this.selection.selectedObjects[i] );
                        let color:any = this.FS.get_line_type( rotatingObject.userData.LineTypesRef ).COLOR;
                        rotatingObject.material.color.set( color ); 
                        this.rotatingObjects.add( rotatingObject );
                    }
                    this.engServ.addToScene(this.rotatingObjects);
                }
               

            }
            // when rotation is completed 
            else if(this.count > this.MAX_POINTS) {
                this.count = 0;

       
                //movement completed of group of objects
                if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.rotatingObjects != null ) {
                    for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {
                        let object:any = this.selection.selectedObjects[i];
                        /*let quaternion = new Quaternion();
                        quaternion = this.rotatingObjects.children[i].getWorldQuaternion(quaternion).normalize();
                        let euler:Euler = new Euler().setFromQuaternion( quaternion ); 
                        console.log(this.FS.radians_to_degrees(euler.z));
                        console.log(this.FS.radians_to_degrees(this.rotatingObjects.rotation.z));
                        console.log("---")*/
                        object.copy( this.rotatingObjects.children[i] );
                        this.FS.set_world_position( object );
                        this.dimensionsShape.update( object, this.engServ );
                    }
                    this.engServ.removeFromScene(this.rotatingObjects);
                    this.rotatingObjects = null;
                    this.engServ.updateSessionStorage();
                }

            }
        }
    }

    onMouseMove( relative:Vector3, e:any ) {
     
        //update line when after first click
        if( this.count == 2 )
            this.updateLine(relative);
        //rotate object
        else if( this.count == 3 )
            this.rotate(relative);
    }

    onStop() {
        this.engServ.removeFromScene(this.line);
        this.engServ.removeFromScene(this.rotatingObjects);
        this.count = 0;
    }

    rotate( relative:Vector3 ) {

        let rotatingObjectAngle:number = this.rotatingObjects.rotation.z;
        rotatingObjectAngle = this.FS.radians_to_degrees( rotatingObjectAngle );
        rotatingObjectAngle = this.FS.degrees_to_radians( rotatingObjectAngle * -1 );

        //reset to default rotation, that is zero
        this.rotateAboutPoint( this.rotatingObjects, this.rotationPoint, this.axis, rotatingObjectAngle, false );

        //get angles between refernce point and mouse position
        let relativeAngle:number = this.FS.get_angle_between_points( this.rotationPoint.x, this.rotationPoint.y, relative.x, relative.y );            
        relativeAngle = this.FS.degrees_to_radians( relativeAngle - this.offsetAngle );
        
        this.rotateAboutPoint( this.rotatingObjects, this.rotationPoint, this.axis, relativeAngle, false );

    }



    // update line
    updateLine(relative:Vector3) {
        this.positions[this.count * 3 - 3] = relative.x;
        this.positions[this.count * 3 - 2] = relative.y;
        this.positions[this.count * 3 - 1] = relative.z;
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    // add point
    addPoint(relative:Vector3){
        this.positions[this.count * 3 + 0] = relative.x;
        this.positions[this.count * 3 + 1] = relative.y;
        this.positions[this.count * 3 + 2] = relative.z;
  
        this.count++;
        this.line.geometry.setDrawRange(0, this.count);
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    lineGeometry():BufferGeometry {
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();
        this.positions = new Float32Array( (this.MAX_POINTS - 1) * 3 );
        geometry.setAttribute('position', new BufferAttribute(this.positions, 3));
        return geometry;
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineBasicMaterial = new LineBasicMaterial(
            { color: this.MS.color.REF_LINE }
        );
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }

    //https://stackoverflow.com/questions/42812861/three-js-pivot-point/42866733#42866733
    // obj - your object (THREE.Object3D or derived)
    // point - the point of rotation (THREE.Vector3)
    // axis - the axis of rotation (normalized THREE.Vector3)
    // theta - radian value of rotation
    // pointIsWorld - boolean indicating the point is in world coordinates (default = false)
    private rotateAboutPoint( obj:Object3D, point:Vector3, axis:Vector3, theta:number, pointIsWorld:boolean ){
        pointIsWorld = (pointIsWorld === undefined)? false : pointIsWorld;
    
        if(pointIsWorld){
            obj.parent.localToWorld(obj.position); // compensate for world coordinate
        }
    
        obj.position.sub(point); // remove the offset
        obj.position.applyAxisAngle(axis, theta); // rotate the POSITION
        obj.position.add(point); // re-add the offset
    
        if(pointIsWorld){
            obj.parent.worldToLocal(obj.position); // undo world coordinates compensation
        }
    
        obj.rotateOnAxis(axis, theta); // rotate the OBJECT
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" || name == "Circle" || name == "Ellipse" || name == "Arc" )
            return true;
        else    
            return false;    
    }
}
