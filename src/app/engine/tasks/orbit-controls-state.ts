import { EngineService } from '../engine.service';
import { Vector3 } from 'three';

export class OrbitControlsState {

    private previousComponentID:number = null;

    private MeasureAndAlign_target:Vector3;
    private MeasureAndAlign_position:Vector3;
    private MeasureAndAlign_zoom:number;

    private LineEditor_target:Vector3;
    private LineEditor_position:Vector3;
    private LineEditor_zoom:number;

    constructor(private engServ: EngineService){}

    change(newComponentID:number){

        if( this.previousComponentID != null && this.previousComponentID != newComponentID ) {

            //save current component
            this.saveState( this.previousComponentID );

            //reset new compoenent
            this.reset( newComponentID );

        }
        
        this.previousComponentID = newComponentID;
          
    }

    private saveState(componentID:number){
        if(componentID == 2)    this.saveMeasureAndALign();
        else if(componentID == 3)    this.saveLineEditor();
    }

    private reset(componentID:number){
        if(componentID == 2)    this.resetMeasureAndALign();
        else if(componentID == 3)    this.resetLineEditor();
    }

    private saveMeasureAndALign(){

        let orbitControls:any = this.engServ.orbitControls;

        this.MeasureAndAlign_target = new Vector3();
        this.MeasureAndAlign_position = new Vector3();

        this.MeasureAndAlign_target.copy( orbitControls.target );
		this.MeasureAndAlign_position.copy( orbitControls.object.position );
		this.MeasureAndAlign_zoom = orbitControls.object.zoom;

    }

    private resetMeasureAndALign(){

        let orbitControls:any = this.engServ.orbitControls;

        orbitControls.target.copy( this.MeasureAndAlign_target );
		orbitControls.object.position.copy( this.MeasureAndAlign_position );
		orbitControls.object.zoom = this.MeasureAndAlign_zoom;

		orbitControls.object.updateProjectionMatrix();

		orbitControls.update();

    }

    private saveLineEditor(){

        let orbitControls:any = this.engServ.orbitControls;

        this.LineEditor_target = new Vector3();
        this.LineEditor_position = new Vector3();

        this.LineEditor_target.copy( orbitControls.target );
		this.LineEditor_position.copy( orbitControls.object.position );
		this.LineEditor_zoom = orbitControls.object.zoom;

    }

    private resetLineEditor(){

        if( this.LineEditor_target == undefined ) return;

        let orbitControls:any = this.engServ.orbitControls;

        orbitControls.target.copy( this.LineEditor_target );
		orbitControls.object.position.copy( this.LineEditor_position );
		orbitControls.object.zoom = this.LineEditor_zoom;

		orbitControls.object.updateProjectionMatrix();

		orbitControls.update();

    }


}
