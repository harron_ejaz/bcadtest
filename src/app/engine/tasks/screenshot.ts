import { EngineService } from '../engine.service';
import { Box3, Vector3, Vector2 } from 'three';
import { MessageService } from 'src/app/services/message.service';

export class Screenshot {

    constructor( private engServ: EngineService, private MS: MessageService ) {
    }

    init(topLeft: Vector2, bottomRight: Vector2) {
        

        const topLeftBottonRightObject = {
            topLeftX: topLeft.x,
            topLeftY: topLeft.y,
            bottomRightX: bottomRight.x,
            bottomRightY: bottomRight.y
        };

        this.MS.area_editor.SCREENSHOT_POSITION = topLeftBottonRightObject;

        let offsetLeft: number = this.engServ.getCurrentOffset(this.engServ.canvasRef).left;
        let offsetTop: number = this.engServ.getCurrentOffset(this.engServ.canvasRef).top;


        offsetLeft = offsetLeft;
        offsetTop = offsetTop;

        const positionTopLeft = {
            x: topLeft.x - offsetLeft,
            y: topLeft.y - offsetTop
        };
      
        const positionBottomRight = {
            x: bottomRight.x - offsetLeft,
            y: bottomRight.y - offsetTop
        };

        let box:Box3 = new Box3( new Vector3( positionTopLeft.x, positionTopLeft.y, 0 ), 
            new Vector3( positionBottomRight.x, positionBottomRight.y, 0 ) );
        //Returns the width, height and depth of this box
        //the result will be copied into this Vector3, i.e., size
        let size:Vector3 = box.getSize( new Vector3() );

        this.getImage( size.x, size.y, box.min.x, box.min.y );

    }
    
    private getImage( w:number, h:number, x:number, y:number ) {

        var oldCanvas = this.engServ.renderer.domElement;
        var newCanvas = document.createElement('canvas');
        newCanvas.width = w;
        newCanvas.height = h;
        var newContext = newCanvas.getContext('2d');
        newContext.drawImage(oldCanvas, x, y, w, h, 0, 0, w, h);
    
        var strMime = "image/jpeg";

        let img = new Image();
        img.src = newCanvas.toDataURL( strMime );
        this.MS.area_editor.SCREENSHOT = img;
        this.MS.area_editor.SCREENSHOT_POSITION = "";
    }
    
}
