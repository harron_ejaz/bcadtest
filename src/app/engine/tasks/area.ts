import { Vector3, Line } from 'three';
import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Subscription } from 'rxjs';


export class Area {

    subscription: Subscription;

    
    private object:any = null;
    private alphabet:any = null;
    private alphabetSelected:boolean = false;
    private ctrlKey =  false;
    
    constructor( private engServ: EngineService, private MS: MessageService, private FS:FunctionService ) {
        
        this.subscription = this.MS.getMessage().subscribe((message) => {
            if ( message ) {
                const msg:string = message.split(':')[0];   
                if( msg == "AREA_EDITOR_SELECT_SVG" || msg == "AREA_EDITOR_UNSELECT_SVG" ) {
                    let polygonID:string = message.split(':')[1];
           
                    let polygon:any = this.engServ.getSelectedAreasGroup().getObjectByProperty( "uuid", polygonID );

                    if( msg == "AREA_EDITOR_SELECT_SVG" )
                        this.select( polygon );
                    else if( msg == "AREA_EDITOR_UNSELECT_SVG" )   
                        this.unselect( polygon ); 

                }
            }
        });        

    }

    onKeyDown(e:KeyboardEvent) { 
        e.preventDefault();
        if( e.ctrlKey && e.code=="ControlLeft" ) {
              this.ctrlKey = e.ctrlKey;
              document.body.style.cursor = "copy";
        }
        else if( e.ctrlKey && e.code=="KeyA" ) {
            //if one or more polygons are selected for dragging, drag drop polygon to left plane
            if( this.MS.area_editor.POLYGON_ID.length > 0 ) {
                this.MS.area_editor.DRAGGING_POLYGON = true;
                this.MS.sendMessage("DRAG_DROP_POLYGON");
            }
        }
    }

    onKeyUp(e:KeyboardEvent) { 
        this.ctrlKey = e.ctrlKey;
        document.body.style.cursor = "default";
    }

    onClick( relative:Vector3 ) {
        //remove polygon from aray, as it was added on mouse down
        this.MS.area_editor.POLYGON_ID = this.FS.remove_from_array( this.MS.area_editor.POLYGON_ID, this.object.uuid );
        //if polygon is highlighted, and polygon is already selected
        if( this.object && this.object.selected ) {
            if( this.object.name == "Polygon" ) {
                this.unselect( this.object );
            }
        }
        //if polygon is already highlighted, and polygon is not already selected,
        //select only one polygon at a time
        else if( this.object && !this.object.selected && this.ctrlKey==false ) {
            if( this.object.name == "Polygon" ) {
                this.unselectAll();
                this.select( this.object );
            }
        }
        //if polygon is already highlighted, and polygon is not already selected, 
        //and control key is pressed, to select multiple polygons
        else if( this.object && !this.object.selected && this.ctrlKey==true ) {
            if( this.object.name == "Polygon" ) {
                this.select( this.object );
            }
        }
        this.alphabetSelected = false;
        document.body.style.cursor = "default";
    }

    onMouseDown( relative:Vector3 ) {
        //if polygon is highlighteed and not selected
        if( this.object && !this.object.selected ) { 
            if( this.object.name == "Polygon" ) {
                this.MS.area_editor.POLYGON_ID.push( this.object.uuid );
                this.MS.area_editor.DRAGGING_POLYGON = true;
                document.body.style.cursor = "grabbing";
                
            }
        }
        //if polygon is already selected
        else if( this.object && this.object.selected ) {
            if( this.object.name == "Polygon" ) {
                this.MS.area_editor.DRAGGING_POLYGON = true;
                document.body.style.cursor = "grabbing";
            }
        }
        
        else {
            this.MS.area_editor.DRAGGING_POLYGON = false;
            document.body.style.cursor = "default";
        }

        //if polygon's alphabet is already highlighted
        if( this.alphabet ) {
            if( this.alphabet.name == "PolygonAlphabet" ) {
                this.alphabetSelected = true;
            }
        }
    }

    onMouseMove( relative:Vector3, e:any ) {
        this.highlight( e );

        if( this.alphabetSelected == false )
            this.highlightPolygonAlphabet( e );
        //move polygon's alphabet
        else if( this.alphabetSelected == true ) 
            this.moveAlphabet( relative ); 
            
        if( this.MS.area_editor.DRAGGING_POLYGON ) {
            document.body.style.cursor = "grabbing";
        }
    }

    private moveAlphabet( relative:Vector3 ) {
        if( this.alphabet ) {
            this.alphabet.position.copy( relative );
        }
    }

    private highlightPolygonAlphabet( e:any ) {
        if ( this.alphabet ) {
            if( this.alphabet.name == "PolygonAlphabet" ) {
                //un-highlight object
                this.alphabet.material.color.set( this.MS.color.LINE );
                this.alphabet = null;
            }
        }
        this.alphabet = this.engServ.getIntersects( e.clientX, e.clientY, this.engServ.getSelectedAreasGroup() );
        if( this.alphabet ) {
            if( this.alphabet.name == "PolygonAlphabet" ) {
                //highlight object
                this.alphabet.material.color.set( this.MS.color.HIGHLIGHT_LINE );
            }            
        }
    }

    private highlight( e:any ) {
        if ( this.object && !this.object.selected ) {
            if( this.object.name == "Polygon" ) {
                //un-highlight object
                this.object.material.color.set( this.MS.color.POLYGON );
                this.object = null;
            }
        }
        this.object = this.engServ.getIntersectsArea( e.clientX, e.clientY, this.engServ.getSelectedAreasGroup() );
        if( this.object && !this.object.selected ) {
            if( this.object.name == "Polygon" ) {
              //highlight object
              this.object.material.color.set( this.MS.color.HIGHLIGHT_POLYGON );
            }
        }
    }

    private select( object:any ) {
        object.selected = true; 
        object.material.color.set( this.MS.color.SELECT_POLYGON );
        this.MS.area_editor.POLYGON_ID.push( object.uuid );
    }  

    private unselect( object:any ) {
        object.selected = false;  
        object.material.color.set( this.MS.color.HIGHLIGHT_POLYGON );
        this.MS.area_editor.POLYGON_ID = this.FS.remove_from_array( this.MS.area_editor.POLYGON_ID, object.uuid );
    }

    private unselectAll() {
        let areasGroup:any = this.engServ.getSelectedAreasGroup();
        
        for( let i = 0; i < areasGroup.children.length; i++ ) {
            let polygon:any = areasGroup.children[i].getObjectByName("Polygon");
            if( polygon ) {
                polygon.selected = false;  
                polygon.material.color.set( this.MS.color.POLYGON );
                this.MS.area_editor.POLYGON_ID = this.FS.remove_from_array( this.MS.area_editor.POLYGON_ID, polygon.uuid );
            }
        }
        
    }


    

}
