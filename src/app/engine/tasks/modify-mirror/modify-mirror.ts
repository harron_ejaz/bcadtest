import { Vector3, BufferGeometry, BufferAttribute, LineBasicMaterial, Line, BoxBufferGeometry, MeshBasicMaterial, Mesh, LineDashedMaterial, Group } from 'three';
import { EngineService } from '../../engine.service';
import { MessageService } from './../../../services/message.service';
import { FunctionService } from 'src/app/services/function.service';
import { Selection } from '../selection';
import { DimensionsShape } from '../../shapes/dimensions-shape';

export class ModifyMirror {


    private mirrorObjects:any = null;

    private line:any;
    private count:number = 0;
    private MAX_POINTS:number = 3;
    private positions:any;

    constructor( private engServ: EngineService, private MS:MessageService, private FS:FunctionService, private dimensionsShape:DimensionsShape, private selection:Selection ) {
    }

    onMouseDown( relative:Vector3 ) {
        //if line is not selected
        if( !this.selection.selected ) {
            this.selection.enable();
        }
    }

    onClick( relative:Vector3 ) {
        //line is selected, now draw a line for first mirror axis point and second mirror axis point on object's geometry
        if( this.selection.selected ) {
            // on first click add an extra point
            if( this.count === 0 ){
                const geometry:BufferGeometry = this.lineGeometry();
                this.line = this.lineShape( geometry );
                this.line.name = 'Line';
                this.line.renderOrder = 1;
                this.engServ.addToScene(this.line);
                this.addPoint(relative);
            }
            this.addPoint(relative);  
            // when mirroring
            if( this.count === 2 ){

                //object is selected using selection
                if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.mirrorObjects == null ) {
                    //add a mirror line object
                    this.mirrorObjects = new Group();
                    //add objects in a group
                    for( let i = 0; i < this.selection.selectedObjects.length; i++ ) {
                        //add a mirror object
                        const geometry:BufferGeometry = this.selection.selectedObjects[i].geometry.clone();
                        const material:LineDashedMaterial = this.selection.selectedObjects[i].material.clone();
                        let mirrorObject:any = new Line(geometry, material);
                        mirrorObject.copy( this.selection.selectedObjects[i] );
                        let lineType:any = this.FS.get_line_type( mirrorObject.userData.LineTypesRef );
                        mirrorObject.material.color.set( lineType.COLOR ); 
                        mirrorObject.material.dashSize = 1; 
                        mirrorObject.material.gapSize = lineType.SOLID; 
                        mirrorObject.userData.Selected = false;
                        this.mirrorObjects.add( mirrorObject );
                    }
                    this.engServ.addToScene(this.mirrorObjects);
                }


            }
            // when mirror completed
            else if(this.count === this.MAX_POINTS) {
                this.count = 0;
               

                
				
				
				
				
				
				
				
				
          
				//movement completed of group of objects
                if( this.selection.selectedObjects.length > 0 && this.selection.selected && this.mirrorObjects != null ) {
                    for( let i = 0; i < this.mirrorObjects.children.length; i++ ) {
                        let mirrorObject = this.mirrorObjects.children[i].clone();
                        this.engServ.addToScene( mirrorObject );
                        this.FS.set_world_position( mirrorObject );
                        this.dimensionsShape.update( this.selection.selectedObjects[i], this.engServ );
                        this.dimensionsShape.update( mirrorObject, this.engServ );
                    }
                    this.engServ.removeFromScene(this.line);
                    this.engServ.removeFromScene(this.mirrorObjects);
                    this.mirrorObjects = null;
                    this.engServ.updateSessionStorage();
                }
            }
        }
    }

    onMouseMove( relative:Vector3, e:any ) {
    
        //update line when after first click
        if( this.count == 2 ) {
            this.updateLine(relative);
            this.mirror(relative)
        }
            
    }

    onStop() {
        this.engServ.removeFromScene(this.line);
        this.engServ.removeFromScene(this.mirrorObjects);
        this.count = 0;
    }

    mirror( relative:Vector3 ) {

        //get reference line angle
        const lineArray = this.line.geometry.attributes.position.array;
        const lineX2:number = lineArray[3];
        const lineY2:number = lineArray[4];

        for( let i=0; i < this.selection.selectedObjects.length; i++) {
            const object:any = this.selection.selectedObjects[i];

            const objectArray = object.geometry.attributes.position.array;
            const objectX1:number = objectArray[0];
            const objectY1:number = objectArray[1];
            const objectX2:number = objectArray[3];
            const objectY2:number = objectArray[4];

            const normal:Vector3 = new Vector3( lineX2, lineY2, 0 ).normalize();
            const reflectedPoint1:Vector3 = new Vector3( objectX1, objectY1, 0 ).reflect( normal );
            const reflectedPoint2:Vector3 = new Vector3( objectX2, objectY2, 0 ).reflect( normal );

            let mirrorObject = this.mirrorObjects.children[i];

            let mirrorObjectArray = mirrorObject.geometry.attributes.position.array;
            mirrorObjectArray[0] = reflectedPoint1.x;
            mirrorObjectArray[1] = reflectedPoint1.y;

            mirrorObjectArray[3] = reflectedPoint2.x;
            mirrorObjectArray[4] = reflectedPoint2.y;

            mirrorObject.geometry.attributes.position.needsUpdate = true;
        }

        

    }

    

    // update line
    updateLine(relative:Vector3) {
        this.positions[this.count * 3 - 3] = relative.x;
        this.positions[this.count * 3 - 2] = relative.y;
        this.positions[this.count * 3 - 1] = relative.z;
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    // add point
    addPoint(relative:Vector3){
        this.positions[this.count * 3 + 0] = relative.x;
        this.positions[this.count * 3 + 1] = relative.y;
        this.positions[this.count * 3 + 2] = relative.z;
  
        this.count++;
        this.line.geometry.setDrawRange(0, this.count);
        this.line.geometry.attributes.position.needsUpdate = true;
    }

    lineGeometry():BufferGeometry {
        // geometry
        const geometry:BufferGeometry = new BufferGeometry();
        this.positions = new Float32Array( (this.MAX_POINTS - 1) * 3 );
        geometry.setAttribute('position', new BufferAttribute(this.positions, 3));
        return geometry;
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        const material:LineBasicMaterial = new LineBasicMaterial(
            { color: this.MS.color.REF_LINE }
        );
        // line
        const line:Line = new Line(geometry, material);
        return line;
    }

    isValidObject( name:string ):boolean {
        if( name == "Line" || name == "Circle" || name == "Ellipse" || name == "Arc" )
            return true;
        else    
            return false;    
    }
}
