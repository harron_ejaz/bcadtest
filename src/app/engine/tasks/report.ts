import { EngineService } from '../engine.service';
import { MessageService } from 'src/app/services/message.service';
import { FunctionService } from 'src/app/services/function.service';

export class Report {


    constructor( private engServ: EngineService, private MS: MessageService, private FS:FunctionService ) {
        
    }

    init() {
        //this.takeScreenshot();
    }

    //take screen shot of canvas with polygons from area editor page
    //https://jsfiddle.net/2pha/art388yv/
    private takeScreenshot() {
        let img = new Image();
        let strMime = "image/jpeg";
        img.src = this.engServ.renderer.domElement.toDataURL( strMime );
        this.MS.area_editor.SCREENSHOT = img;
        
    }

    //https://discourse.threejs.org/t/screenshot-a-object-to-its-height-and-width/14312/3
    private getExtent() {

    }
}
