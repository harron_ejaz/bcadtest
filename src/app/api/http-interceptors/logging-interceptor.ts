import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";

import { finalize, tap } from "rxjs/operators";
import { MessageService } from "src/app/services/message.service";
import { LoadingBarService } from '@ngx-loading-bar/core';
import { NgxSpinnerService } from "ngx-spinner";

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
  constructor(private messenger: MessageService, private loadingBar: LoadingBarService, private spinner: NgxSpinnerService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const started = Date.now();
    let ok: string;
    this.loadingBar.start();
    if(this.messenger.saveAndSubmit.ignoreSpinnerAutoSave == true) {
      this.spinner.show();
    }
    // extend server response observable with logging
    return next.handle(req).pipe(
      tap(
        // Succeeds when there is a response; ignore other events
        (event) => (ok = event instanceof HttpResponse ? "succeeded" : ""),
        // Operation failed; error is an HttpErrorResponse
        (error) => (ok = "failed")
      ),
      // Log when response observable either completes or errors
      finalize(() => {
        this.loadingBar.complete();
        this.spinner.hide();
        const elapsed = Date.now() - started;
        const msg = `${req.method} "${req.urlWithParams}"
             ${ok} in ${elapsed} ms.`;
        this.messenger.add(msg);
      })
    );
  }
}
