import { Injectable } from "@angular/core";
import { MessageService } from "src/app/services/message.service";

/** Mock client-side authentication/authorization service */
@Injectable()
export class AuthService {
  constructor(public MS: MessageService){

  }
  getAuthorizationToken() {
    return this.MS.basicAuthToken.BASIC_AUTH_TOKEN;
  }
}
