import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class GeneralService {
  ip = environment.apiEndPoint;

  constructor(private http: HttpClient) {}

  public getAll(url) {
    let u = this.ip + url;

    return this.http.get(u);
  }
  public getByID(url) {
    let u = this.ip + url;
    return this.http.get(u);
  }

  public postData(url, data) {
    let u = this.ip + url;
    return this.http.post(u, data);
  }
  public deleteData(url, id) {
    let u = this.ip + url + id;
    return this.http.delete(u);
  }

  public updateData(url, data) {
    let u = this.ip + url;
    return this.http.put(u, data);
  }

  public isEmptyObject(obj) {
    return obj && Object.keys(obj).length === 0;
  }
}