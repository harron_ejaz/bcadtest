import { TestBed } from '@angular/core/testing';

import { ScdFileService } from './scd-file.service';

describe('ScdFileService', () => {
  let service: ScdFileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScdFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
