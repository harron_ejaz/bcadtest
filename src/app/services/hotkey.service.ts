import { Injectable } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';


@Injectable({
  providedIn: 'root'
})
export class HotkeyService {

  constructor( private MS:MessageService ) { }

  keyDownLineEditor(e:KeyboardEvent) { 
    e.preventDefault();
    if( e.key == "Escape" || e.code == "Escape" ) {
      this.MS.sendMessage("HOTKEY_ESC_LINE_EDITOR");
    }
    else if( e.ctrlKey && e.code=="KeyA" ) {
      this.MS.sendMessage("HOTKEY_CTRLA_SELECTALL_LINE_EDITOR");
    }
    else if( e.ctrlKey && e.code=="KeyG" ) {
      this.MS.sendMessage("HOTKEY_CTRLG_MOVE_LINE_EDITOR");
    }
    else if( e.ctrlKey && e.code=="KeyZ" ) {
      this.MS.sendMessage("HOTKEY_CTRLZ_UNDO_LINE_EDITOR");
    }
    else if( e.ctrlKey && e.code=="KeyY" ) {
      this.MS.sendMessage("HOTKEY_CTRLY_REDO_LINE_EDITOR");
    }
    else if( e.ctrlKey && e.code=="Space" ) {
      this.MS.sendMessage("HOTKEY_CTRLSPACE_LINE_LINE_EDITOR");
    }

  }

}
