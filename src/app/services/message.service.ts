//https://jasonwatmore.com/post/2019/02/07/angular-7-communicating-between-components-with-observable-subject

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  messages: string[] = [];
  private subject = new Subject<string>();
  public engineGroupJSON: JSON;
  public save_project_json;
  
  public basicAuthToken = {
    BASIC_AUTH_TOKEN: "",
  }

  public COMPONENT_ID:number = 0;

  public color = {
    BACKGROUND: 0x999999,
    EXTENT: 0x000000,
    EXTENTS: 0xFDBF26,
    LINE: 0x800000,
    ALIGN_LINE: 0xfff000,
    HIGHLIGHT_LINE: 0xFFF000,
    SELECT_LINE: 0xCBC800,
    LINE_POINT: 0xFFB900,
    LINE_EDGE: 0xFFF300,
    REF_LINE: 0xB9B9B9,
    POLYGON: 0xBBBBBB,
    HIGHLIGHT_POLYGON: 0xFFF92C,
    SELECT_POLYGON: 0xFF4A4A,
    VIEWPORT: 0xF3FF00,
    TEXT: 0xFE2C2C,
    RADIUS_LINE: 0x8A8A8A
  }

  //setting buttons in settings modal page
  public settings = {
    GRID_SNAP_Length: 12, //inches,
    RADIAL_DEGREES: 90, //degrees
    BACKGROUND_COLOR: "#000000",
    SELECTED_INDEX_BACKGROUND_COLOR: 6,
    CROSSHAIR_SIZE: 100,
    CROSSHAIR_COLOR: "#FFFFFF",
    AUTO_SAVE: true,
    DIMENSIONS_STANDARD: false,
    DIMENSIONS_METRIC: false,
    OBJECT_SNAPS: false,
  };
  
  //measure & align menu buttons in measure & align page
  public measure_align = {
    ALIGN: false, MEASURE: false,
  };

  //zoom menu buttons status
  public menu_zoom = {
    WINDOW: false,
    PAN: false,
    EXTENTS: false
  };

  //menu buttons in imagey page (image acquisition) 
  public menu_select_image = {
    IS_SINGLE_OVERHEAD_IMAGE: true,
    COPYRIGHT_TEXT: 'Nearmaps'
  }


  //current values of menu buttons status in line editor page
  public menu_line_editor = {
    LINE_TYPES_REF_SELECTED: 1, //{ LINE_TYPES_REF : 1, NAME : "Eave", COLOR : 0xf73e2d, SOLID : 0 }
    RISE_SELECTED: 0, //pitch
    BUILDING_GROUP_SELECTED: 0,
    STRUCTURE_COUNT: [],
    TEXT: "",
    SHOW_IMAGE: true,
    SHOW_DIMS: true,
    EDIT_DIMS: false
  }

  //{ LINE_TYPES_REF : 0, NAME : "Eave", COLOR : 0xf73e2d, SOLID : 0 }
  public LINE_TYPES = [];

  public menu_selection = {
    SELECTION: false,
    SELECT_ALL: false,
    SELECT_BY_LINE_TYPE: false,
    SELECT_BY_PITCH: false,
    SELECT_BY_LINE_TYPE_AND_PITCH: false
  }
  
  public components = {

      MENU : {
        single_image : {
          concrete:true,roofscopex:true,roofinggutter:true,gutter:true,siding:true,paint:true,insulation:true,roofing:true
        },
        four_images : {
          concrete:false,roofscopex:false,roofinggutter:false,gutter:true,siding:false,paint:false,insulation:false,roofing:true
        }
      }

    }
            

  //insert menu buttons status in line editor page
  public menu_insert = {
    LINE: false,
    ARC: false,
    TRIANGLE: false,
    TEXT: false,
    CIRCLE: false,
    RECTANGLE: false,
    ELLIPSE: false,
  };
  //modify menu buttons status in line editor page
  public menu_modify = {
    MOVE: false,
    STRETCH: false,
    ROTATE: false,
    OFFSET: false,
    MIRROR: false,
    EXTEND: false,
    FILLET: false,
    TRIM: false,
    EDIT_TEXT: false,
    VIEW_PORT: false,
    POLYGON_BREAK: false
  };

  public bg_last_value = {
    BACKGROUND_VALUE: 0,
  }

  //input length is provided by user and scale length is calculated in measure and align page
  public input_scaleLength = {
    inputLength: 1, 
    scaleLength: 1, //feet
  };

  //footer buttons in line editor page
  public footer_lineEditor = {
    RADIAL: false,
    MID_POINT: false,
    END_POINT: false,
    INTERSECTION: false,
    PERPENDICULAR: false,
    NEAREST: false,
    GRID: false,
  };

  //scope queue page
  public scope_queue = {
    GRID_ROW_SELECTED: false,
    GRID_TAB: "",
    SEARCH: "",
    GRID_ROW_SELECTED_ID: 0
  }

  //project information page
  public project_information = {
    ORDER_ID: "",
    PROJECT_NAME: "",
    PROJECT_ADDRESS: "",
    CUSTOMER_NOTES: "",
    QC_NOTES: "",
    SHIPPING: "",
    SCOPE_TYPE: "",
    PRODUCT_TYPE: "",
    MAP_LINK: "",
    IS_MULTI_STRUCTURE: false, //is multi structure checkbox status in project information page
    BUILDINGS: [], //{ Name: "Structure 1", Count: 1, Id: buildingsGroup.uuid }
    DUE_DATE: null,
    SITE_IMAGE: null,
  };

  //imagery (image acquisition) page
  public imagery = {
    TOP_IMAGE_URL: "assets/images/browseImage.jpg", //top image selected in image acquisition page
    NORTH_IMAGE_URL: "assets/images/browseImage.jpg",
    SOUTH_IMAGE_URL: "assets/images/browseImage.jpg",
    EAST_IMAGE_URL: "assets/images/browseImage.jpg",
    WEST_IMAGE_URL: "assets/images/browseImage.jpg",
  };


  //area editor (assign planes) page
  public area_editor = {
    DRAGGING_POLYGON: false,
    POLYGON_ID: new Array(),
    POLYGON_DATA: new Array(),
    SCREENSHOT: null,
    SCREENSHOT_POSITION: null,
    LAST_SELECTED_LABEL_SHAPE:"",
    LAST_SELECT_LABEL_INDEX: "",
    LAST_SELECT_SHAPE_PARENT_INDEX: "",
    LAST_SELECT_SHAPE_INDEX: "",
  }

  //report page
  public report = {
    SUBMIT_ERROR: "",  //Building name with zero plane (zero area)\
    REPORT_CHECK_ZERO: "",
    REPORT_CHECK_NEGTIVE: "",
    PDF: null,
    POLYGON_EXTIST_MESSAGES: null,
    POLYGON_NAGETIVE_MESSAGES: null,
    POLYGON_NAGETIVE_ALPHABETS: null,
  }


  public new_project = {
    DUE_DATE: "",
    NAME: "",
    ADDRESS: "",
    DESCRIPTION: "",
    STATUS: "",
    REVISION: 0,
    GOOGLE_IMAGE: "",
    CLAIM_NUMBER: 1,
    JOB_NUMBER: 1,
    CREATED_ON: "",
    UPDATED_ON: "",
    USERS_REF: 1,
    PRODUCT_TYPE: "",
    ID: 0,
    FILENAME: "",
    Type: "",
  };

  public newProjectObject = {
    SCD_OBJ: null,
    OpenProjectSCD_OBJ: null
  }

  public currentSCDProjectObject = {
    SCD_DATA: null,
  }

  public signIn_user = {
    userName : null
  }

  public saveAndSubmit = {
    isSaveAndSubmit: false,
    ignoreSpinnerAutoSave: true
  }

  add(message: string) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }

  sendMessage(message: string) {
    this.subject.next(message);
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
