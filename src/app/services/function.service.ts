import { Injectable } from '@angular/core';
import { Vector3, Line3 } from 'three';
import { EngineService } from '../engine/engine.service';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class FunctionService {

  private linePoint:any = [ { mousePosition:null, selectedObject:null } ];

  constructor( private MS:MessageService ) {
  }

  //calculate ratio of measurement line length to input given by user
  get_input_scale_ratio( input_scaleLength:any ):number {

    //static
    const inputLength:number = input_scaleLength.inputLength;
    const scaleLength:number = input_scaleLength.scaleLength;
    const ratio:number = scaleLength / inputLength;

    return ratio;
  }

  // angle in degrees
  get_angle_between_points(x1:number, y1:number, x2:number, y2:number):number {
    return Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI
  }

  get_angle_radial(angleDeg:number, RADIAL_DEGREES:number):number {

    if( angleDeg < 0 ) angleDeg = angleDeg + 360;

    let radial_degree_number = Math.ceil( angleDeg / ( RADIAL_DEGREES / 2 ) );

    let total_angles = Math.ceil( 360 / RADIAL_DEGREES );

    let angleToRotate = 360 / total_angles;

    let sign:number = Math.sign( radial_degree_number );

      if( radial_degree_number % 2 == 0 ) {
        return angleToRotate * radial_degree_number / 2;
      }
      else {
        return angleToRotate * ( radial_degree_number - 1 ) / 2;
      }
            
  }
    
  //get a new point from a current point, angle and distance
  get_point_from_angle_distance(x:number, y:number, angle:number, distance:number):Vector3 {
    let point:Vector3 = new Vector3();

    point.x = Math.cos(angle * Math.PI / 180) * distance + x;
    point.y = Math.sin(angle * Math.PI / 180) * distance + y;
    point.z = 0;

    return point;
}
get_angle_between_two_lines( Line1X, Line1Y, centerX, centerY, Line2X, Line2Y ):number {
    var dAx = centerX - Line1X;
    var dAy = centerY - Line1Y;
    var dBx = centerX - Line2X;
    var dBy = centerY - Line2Y;
    var angle = Math.atan2(dAx * dBy - dAy * dBx, dAx * dBx + dAy * dBy);
    if(angle < 0) {angle = angle * -1;}
    var degree_angle = angle * (180 / Math.PI);
    return degree_angle;
  }

  get_arc_length(radius:number, angle:number ):number {
    let length = ( 2 * 22/7 * radius ) * ( angle / 360 );
    return length;
  }

  degrees_to_radians(degrees:number):number{
    var pi = Math.PI;
    return degrees * (pi/180);
  }

  radians_to_degrees(radians:number):number{
      var pi = Math.PI;
      return radians * (180/pi); 
  }

  get_formatted_feet(decimal: number): string {
    //remove 0 if after decimal
    let val: string = (+decimal).toFixed(1).replace(/\.0+$/, "");
    return val;
  }

  get_mid_point( x1:number, y1:number, x2:number, y2:number ):Vector3 {
    const midPoint:Vector3 = new Vector3( (x1 + x2) / 2, (y1 + y2) / 2, 0 );
    return midPoint;
  }

  get_position_parallel_to_line( x1:number, y1:number, x2:number, y2:number ):Vector3 {

    //https://stackoverflow.com/questions/57811875/how-to-build-a-perpendicular-in-three-js
    //You can find the normal of a line by subtracting its start point from its end point 
    //(thus you'll get its direction), 
    //then rotate the resulted vector at 90 degrees (Math.PI * 0.5), 
    //then normalize it, and this is it, you've got the normal.

    var lineVertices = [
      new Vector3(x1, y1, 0),
      new Vector3(x2, y2, 0)
    ];

    var midPoint = new Vector3()
    .subVectors(lineVertices[1], lineVertices[0])
    .multiplyScalar(0.5)
    .add(lineVertices[0]);

    var normal = new Vector3()
    .subVectors(lineVertices[1], lineVertices[0])
    .applyAxisAngle(new Vector3(0, 0, 1), Math.PI * 0.5)
    .normalize();
    return normal.clone().setLength(2).add(midPoint);
 }

  //line legth or distance between 2 points
  get_distance_between_points( x1:number, y1:number, x2:number, y2:number ):number {
    const distanceBetweenPoints:number = Math.sqrt( Math.pow((x1-x2), 2) + Math.pow((y1-y2), 2) );
    return distanceBetweenPoints;    
  }

  //get closest point to line point
  get_closest_point_to_line( lineX1:number, lineY1:number, lineX2:number, lineY2:number, pointX:number, pointY:number ):Vector3 {
    let line:Line3 = new Line3( new Vector3( lineX1, lineY1, 0 ), new Vector3( lineX2, lineY2, 0 ) );
    const nearestPoint:Vector3 = line.closestPointToPoint( new Vector3( pointX, pointY, 0 ), true, new Vector3 );
    return nearestPoint;
  }


  // Function to check if two straight 
  // lines are orthogonal / perpendicular or not
  get_perpendicular_point(
    Line1X1: number,
    Line1Y1: number,
    Line1X2: number,
    Line1Y2: number,
    Line2X1: number,
    Line2Y1: number,
    Line2X2: number,
    Line2Y2: number
  ): Vector3 {
    let x1: number,
      y1: number,
      x2: number,
      y2: number,
      x3: number,
      y3: number,
      x4: number,
      y4: number;
    x1 = Line1X1;
    y1 = Line1Y1;
    x2 = Line1X2;
    y2 = Line1Y2;
    x3 = Line2X1;
    y3 = Line2Y1;
    x4 = Line2X2;
    y4 = Line2Y2;

    const angle1: number = this.get_angle_between_points(x1, y1, x2, y2);
    const angle2: number = this.get_angle_between_points(x3, y3, x4, y4);

    const diff: number = parseFloat(Math.abs(angle1 - angle2).toFixed(1));
    const range: number = 0.5;

    //if mouse is within point's range which is perpendicular
    if (
      (diff >= 90 - range && diff <= 90 + range) ||
      (diff >= 270 - range && diff <= 270 + range)
    ) {
      //get perpendicular point
      const px = x2 - x1,
        py = y2 - y1,
        dAB = px * px + py * py;
      var u = ((x3 - x1) * px + (y3 - y1) * py) / dAB;
      var x = x1 + u * px,
        y = y1 + u * py;
      //this is point on line which is perpendicular to drawing line first point
      const perpendicularPoint: Vector3 = new Vector3(x, y, 0);
      return perpendicularPoint;
    } else return null;
  }

  //get circle center point by given 3 points of circle
  get_circle_center( Ax:number, Ay:number, Bx:number, By:number, Cx:number, Cy:number ) {
    var yDelta_a = By - Ay;
    var xDelta_a = Bx - Ax;
    var yDelta_b = Cy - By;
    var xDelta_b = Cx - Bx;

    let centerX:number;
    let centerY:number;

    if( yDelta_a == 0 ) yDelta_a = 0.0001;
    if( xDelta_a == 0 ) xDelta_a = 0.0001;
    if( yDelta_b == 0 ) yDelta_b = 0.0001;
    if( xDelta_b == 0 ) xDelta_b = 0.0001;

    let aSlope:number = yDelta_a / xDelta_a;
    let bSlope:number = yDelta_b / xDelta_b;

    centerX = (aSlope*bSlope*(Ay - Cy) + bSlope*(Ax + Bx) - aSlope*(Bx+Cx) )/(2* (bSlope-aSlope) );
    centerY = -1*(centerX - (Ax+Bx)/2)/aSlope +  (Ay+By)/2;
    return new Vector3(centerX, centerY, 0);
  }

  //get angle of point on circle
  get_circle_point_angle( centerX:number, centerY:number, pointX:number, pointY:number ) {
    return Math.atan2(pointY - centerY, pointX - centerX); 
  }

  //check a point is on what side on line is
  if_point_is_above_line( x1:number, y1:number, x2:number, y2:number, Px:number, Py:number ):boolean {
    let v1:any = {x:x2-x1, y:y2-y1};  
    let v2:any = {x:Px-x1, y:Py-y1}; 
    let cross_product:number = v1.x*v2.y - v1.y*v2.x;
    if( cross_product > 0 )
       return false;
    else if( cross_product < 0 )
        return true;
    else
      return true;
  }

  //check if a point is on line
  if_point_is_on_line ( x1:number, y1:number, x2:number, y2:number, Px:number, Py:number ) {

    var pointA:Vector3 = new Vector3(x1,y1,0);
    var pointB:Vector3 = new Vector3(x2,y2,0);

    var pointToCheck:Vector3 = this.get_closest_point_to_line( x1, y1, x2, y2, Px, Py );
    var c = new Vector3();   
    c.crossVectors(pointA.clone().sub(pointToCheck), pointB.clone().sub(pointToCheck));
    return !c.length(); 

  }

  //check if a point is on line
  if_point_is_between_two_points ( x1:number, y1:number, x2:number, y2:number, Px:number, Py:number ):boolean {

    x1 = Math.round( x1 );
    y1 = Math.round( y1 );
    x2 = Math.round( x2 );
    y2 = Math.round( y2 );
    Px = Math.round( Px );
    Py = Math.round( Py );

    if ( ( ( Px >= x1 && Px <= x2 ) || ( Px >= x2 && Px <= x1 ) )
      && ( ( Py >= y1 && Py <= y2 ) || ( Py >= y2 && Py <= y1 ) ) ) {
      return true;
    }
    else {
      return false;
    }

  }

  is_on_line( x1:number, y1:number, x2:number, y2:number, Px:number, Py:number  ) {
    var f = function(somex) { return (y2 - y1) / (x2 - x1) * (somex - x1) + y1; };
    return Math.abs(f(Px) - Py) < 1e-6 // tolerance, rounding errors
        && Px >= x1 && Px <= x2;      // are they also on this segment?
  }

  is_line_edge( x1:number, y1:number, x2:number, y2:number, Px:number, Py:number  ) {

    var diffX1:number;
    var diffY1:number;

    diffX1 = Math.abs( x1 - Px );
    diffY1 = Math.abs( y1 - Py );

    var diffX2:number;
    var diffY2:number;

    diffX2 = Math.abs( x2 - Px );
    diffY2 = Math.abs( y2 - Py );

    let tolerance:number = 0.5;

    if( diffX1 < tolerance && diffY1 < tolerance ) {
      return 1;
    }
    else if(  diffX2 < tolerance && diffY2 < tolerance ) {
      return 2;
    }
    else
      return -1;

  }
  
  get_length_or_area_with_slope_factor( lengthOrArea:number, rise:number ):number {
    let slopeFactor:number = this.get_slope_factor( rise );
    lengthOrArea = lengthOrArea * slopeFactor;
    return lengthOrArea;
  }

  getGridSnapLineLength(x1: number, y1: number, x2: number, y2: number) {
    //distance between 2 points
    let distanceBetweenPoints: number = this.get_distance_between_points(
      x1,
      y1,
      x2,
      y2
    );
    let gridSnapLength: number = this.MS.settings.GRID_SNAP_Length;

    const inputScaleRatio: number = this.get_input_scale_ratio(
      this.MS.input_scaleLength
    );

    distanceBetweenPoints = parseFloat(distanceBetweenPoints.toFixed(1));

    distanceBetweenPoints = distanceBetweenPoints / inputScaleRatio;

    gridSnapLength = parseFloat(gridSnapLength.toFixed(1));
    gridSnapLength = gridSnapLength / 12;

    //closest divible. this is the length of line to be drawn according to grid snap length
    let gridSnapLineLength: number =
      distanceBetweenPoints - (distanceBetweenPoints % gridSnapLength);

    return gridSnapLineLength * inputScaleRatio;
  }

  //show line point if midpoint or endpoint is enabled in line editor's footer
  showLinePointSquare(e:any, object:any, engServ:EngineService) {

    if ( this.linePoint[0].selectedObject ) {
        if( this.linePoint[0].selectedObject.name == "Line" ) {
            //hide line point
            engServ.linePointSquare.visible = false;
            this.linePoint = [ { mousePosition:null, selectedObject:null } ];
        }
    }
    this.linePoint = engServ.get3DPosition_Intersects( e.clientX, e.clientY );
    if( this.linePoint[0].selectedObject ) {
        if( this.linePoint[0].selectedObject.name == 'Line') {
            //if mouse is at intersection of 2 lines
            //if mouse is at perpendicular or nearest to line
            //if mouse is at end point or mid point
            let linePointSquarePosition:any //Vector3
            if( linePointSquarePosition = this.getLinePointSquarePosition( this.linePoint, object ) ) {
                //show line point square
                engServ.linePointSquare.position.copy( linePointSquarePosition );
                engServ.linePointSquare.visible = true;
            }
                
        }
    }
  }

  //if mouse is at intersection of 2 lines
    //if mouse is at perpendicular or nearest to line
    //if mouse is at end point or mid point
    getLinePointSquarePosition( linePoint:any[], object:any ):Vector3 {

      const selectedObject = linePoint[0].selectedObject;

      const selectedObjectX1:number = selectedObject.geometry.attributes.position.array[0];
      const selectedObjectY1:number = selectedObject.geometry.attributes.position.array[1];
      const selectedObjectX2:number = selectedObject.geometry.attributes.position.array[3];
      const selectedObjectY2:number = selectedObject.geometry.attributes.position.array[4];

      const mousePosX:number = Math.round( linePoint[0].mousePosition.x );
      const mousePosY:number = Math.round( linePoint[0].mousePosition.y );

      let tolerance:number = 1;

      if( this.MS.footer_lineEditor.NEAREST ) {

          const nearestPoint:Vector3 = this.get_closest_point_to_line( selectedObjectX1, selectedObjectY1, selectedObjectX2, selectedObjectY2, 
              linePoint[0].mousePosition.x, linePoint[0].mousePosition.y );
          return nearestPoint;

      }

      if( this.MS.footer_lineEditor.END_POINT ) {

          if( Math.abs( mousePosX - Math.round( selectedObjectX1 ) ) < tolerance && Math.abs( mousePosY - Math.round( selectedObjectY1 ) ) < tolerance ) {
             return new Vector3( selectedObjectX1, selectedObjectY1, 0 );
          }
          else if( Math.abs( mousePosX - Math.round( selectedObjectX2 ) ) < tolerance && Math.abs( mousePosY - Math.round( selectedObjectY2 ) ) < tolerance ) {
              return new Vector3( selectedObjectX2, selectedObjectY2, 0 );
          }            

      }

      if( this.MS.footer_lineEditor.MID_POINT ) {

          const midPoint:Vector3 = this.get_mid_point( selectedObjectX1, selectedObjectY1, selectedObjectX2, selectedObjectY2 );
           
          if( Math.abs( mousePosX - Math.round( midPoint.x ) ) < tolerance && Math.abs( mousePosY - Math.round( midPoint.y ) ) < tolerance ) {
              return new Vector3( midPoint.x, midPoint.y, 0 );
          }

      }

      if( this.MS.footer_lineEditor.PERPENDICULAR ) {
          
          const shapeArray = object.geometry.attributes.position.array;
          const shapeX1:number = shapeArray[0];
          const shapeY1:number = shapeArray[1];

          const perpendicularPoint = this.get_perpendicular_point( selectedObjectX1, selectedObjectY1, selectedObjectX2, selectedObjectY2,
              shapeX1, shapeY1, linePoint[0].mousePosition.x, linePoint[0].mousePosition.y );
    
          if( perpendicularPoint ) {
              return perpendicularPoint;
          } 

      }

      //intersection button is enabled in line editor page's footer
      //and 2 or more than 2 lines are intersection at mouse position
      if( this.MS.footer_lineEditor.INTERSECTION && linePoint.length > 1 ) {

          const selectedObject1 = linePoint[1].selectedObject;
 
          const selectedObject1X1:number = selectedObject1.geometry.attributes.position.array[0];
          const selectedObject1Y1:number = selectedObject1.geometry.attributes.position.array[1];
          const selectedObject1X2:number = selectedObject1.geometry.attributes.position.array[3];
          const selectedObject1Y2:number = selectedObject1.geometry.attributes.position.array[4];

          let intersection:any = this.get_line_intersection( selectedObjectX1, selectedObjectY1, selectedObjectX2, selectedObjectY2, selectedObject1X1, selectedObject1Y1, selectedObject1X2, selectedObject1Y2 )
          
          if( intersection.x && intersection.y ) {
            return new Vector3( intersection.x, intersection.y, 0 );
          }      
  
      }

      else 
          return null;
          
    }

  //https://stackoverflow.com/questions/22699811/three-js-get-position-of-rotated-object
  set_world_position( object:any ) {
      
    if( object.name != "Line" )
      return;

    var point1:Vector3 = new Vector3( object.geometry.attributes.position.array[0], object.geometry.attributes.position.array[1], 0 );
    var point2:Vector3 = new Vector3( object.geometry.attributes.position.array[3], object.geometry.attributes.position.array[4], 0 );
    point1.applyMatrix4(object.matrixWorld);
    point2.applyMatrix4(object.matrixWorld);

    object.geometry.attributes.position.array[0] = point1.x;
    object.geometry.attributes.position.array[1] = point1.y;
    object.geometry.attributes.position.array[2] = 0;
    object.geometry.attributes.position.array[3] = point2.x;
    object.geometry.attributes.position.array[4] = point2.y;
    object.geometry.attributes.position.array[5] = 0;
    object.geometry.attributes.position.needsUpdate = true;
    object.position.set(0,0,0);
    object.rotation.set(0,0,0)
  }

    // if the lines intersect, the result contains the x and y of the intersection 
    //(treating the lines as infinite) and booleans for whether line segment 1 
    //or line segment 2 contain the point
    get_line_intersection(line1StartX:number, line1StartY:number, line1EndX:number, line1EndY:number, line2StartX:number, line2StartY:number, line2EndX:number, line2EndY:number):any {
      
      var denominator, a, b, numerator1, numerator2, result = {
          x: null,
          y: null,
          onLine1: false,
          onLine2: false
      };
      denominator = ((line2EndY - line2StartY) * (line1EndX - line1StartX)) - ((line2EndX - line2StartX) * (line1EndY - line1StartY));
      if (denominator == 0) {
          return result;
      }
      a = line1StartY - line2StartY;
      b = line1StartX - line2StartX;
      numerator1 = ((line2EndX - line2StartX) * a) - ((line2EndY - line2StartY) * b);
      numerator2 = ((line1EndX - line1StartX) * a) - ((line1EndY - line1StartY) * b);
      a = numerator1 / denominator;
      b = numerator2 / denominator;

      // if we cast these lines infinitely in both directions, they intersect here:
      result.x = line1StartX + (a * (line1EndX - line1StartX));
      result.y = line1StartY + (a * (line1EndY - line1StartY));

      // if line1 is a segment and line2 is infinite, they intersect if:
      if (a > 0 && a < 1) {
          result.onLine1 = true;
      }
      // if line2 is a segment and line1 is infinite, they intersect if:
      if (b > 0 && b < 1) {
          result.onLine2 = true;
      }
      // if line1 and line2 are segments, they intersect if both of the above are true
      return result;
  }

  get_line_type_selected():any {
    let LINE_TYPES_REF_SELECTED:number = this.MS.menu_line_editor.LINE_TYPES_REF_SELECTED;
    let LINE_TYPES = this.MS.LINE_TYPES;
    let lineType:any = LINE_TYPES.find( x => x.LINE_TYPES_REF == LINE_TYPES_REF_SELECTED );
    return lineType;
  }

  get_line_type( LINE_TYPES_REF:number ):any {
    let LINE_TYPES = this.MS.LINE_TYPES;
    let lineType:any = LINE_TYPES.find( x => x.LINE_TYPES_REF == LINE_TYPES_REF );
    return lineType;
  }

  get_random_color():number {
    var randomColor = "000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
    randomColor = '0x'+randomColor;
    return parseInt( randomColor );
  }

  //remove an element from an array
  //https://love2dev.com/blog/javascript-remove-from-array/
  remove_from_array(arr, value) { return arr.filter(function(ele){ return ele != value; });}

  //remove an element from an array by index
  remove_from_array_by_index(arr, index) {
    let newArray= [...arr];
    newArray.splice(index,1);
    return newArray;
  }
  
  //id point inside a polygon
  is_point_inside_polygon(point, vs) {
    // ray-casting algorithm based on
    // https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html/pnpoly.html
    
    var x = point[0], y = point[1];
    
    var inside = false;
    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
        var xi = vs[i][0], yi = vs[i][1];
        var xj = vs[j][0], yj = vs[j][1];
        
        var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }
    
    return inside;
  }

  get_slope_factor( rise:number ):number {
    const run:number = 12;
    let c:number = Math.sqrt( ( rise * rise ) + ( run * run ) );
    let slopeFactor:number = c / 12;
    return slopeFactor;
  }

  get_threshold(camera:any) {
    let zoom:number = camera.zoom;
    return 1 / zoom * 10;
  }

  get_line_point_scale(camera:any) {
    let zoom:number = camera.zoom;
    return 1 / zoom * 3;
        
  }

  check_numbers_equal_nearly(a:number,b:number) {
    let diff:number = Math.abs( a - b );
    if( diff < 1 ) {
        return true;
    }
    else {
      return false;
    }
  }


}
