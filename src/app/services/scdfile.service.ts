import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { Subscription } from "rxjs";
import { MessageService } from "src/app/services/message.service";
import { Router } from "@angular/router";
import { HttpErrorResponse } from '@angular/common/http';

import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { ToastrService } from 'ngx-toastr';

import {
  HttpErrorHandler,
  HandleError,
} from "src/app/api/http-error-handler.service";

const httpOptions = {
  headers: new HttpHeaders({
    Bcad: "bcad"
  })
};

const httpUserSettingOptions = {
  headers: new HttpHeaders({
    Bcad: "bcad",
    "Access-Control-Allow-Origin": "*"
  }),
};

@Injectable({
  providedIn: "root",
})
export class ScdFileService {
  basicDataApiUrl = "https://qa-tracescope.myscopetech.com/Projects/Concrete"; // URL to web api
  basiApiUrl = "https://qa-tracescope.myscopetech.com/"; // URL to web api
  private handleError: HandleError;
  subscription: Subscription;
  apiEndpoint = environment.apiEndPoint;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler,
    private MS: MessageService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.handleError = httpErrorHandler.createHandleError("ScdFileService");

    this.subscription = this.MS.getMessage().subscribe((message) => {
      if (message == "SAVE_PROJECT_JSON") {
        
        this.addBasicProjectData(this.MS.save_project_json).subscribe((res) => {
          let orderID = res.replace("saved", "");
          orderID = orderID.replace("new", "");
          orderID = orderID.replace(" ", "");
          localStorage.setItem("orderID", orderID.trim());
          this.MS.project_information.ORDER_ID = orderID.trim();
          this.MS.sendMessage("SET_ORDER_ID");
          this.toastr.success('Project has been saved!', "Success!");
          localStorage.removeItem("projectSavedInfo_" + this.MS.save_project_json.Id);
        },
        (error: HttpErrorResponse) => {
          localStorage.setItem("projectSavedInfo_" + this.MS.save_project_json.Id, JSON.stringify(this.MS.save_project_json));
          this.toastr.warning('Project not saved!', "Warning!");
        });
      }
      
    
    });
    
  }

  addBasicProjectData(saveprojectObject: any): Observable<any> {
    let body = JSON.stringify(saveprojectObject);
    return this.http.post<any>(this.basicDataApiUrl, body, httpOptions).pipe();
  }

  saveAndSubmit( saveAndSubmitObject: any, orderID: number ): Observable<any> {
    let url = environment.apiEndPoint + "Projects/" + orderID;
    let body = JSON.stringify( saveAndSubmitObject );
    return this.http.post<any>(url, body, httpOptions).pipe();
  }

  savePDF( saveAndSubmitObject: any): Observable<any> {

    let body = this.MS.report.PDF;

    const paramData = {
      ProjectsRef: this.MS.project_information.ORDER_ID,
      Name: saveAndSubmitObject.Name,
      Size: 333,
      Data: body,
      DwgSize: 333,
      DwgData: body,
      DxfSize: 333,
      DxfData: body,
      ProductType: saveAndSubmitObject.ProductType
    }

    console.log("paramData > ", paramData);

    let url = environment.apiEndPoint + "Pdf/" + this.MS.project_information.ORDER_ID;

    return this.http.post<any>(url, paramData, httpOptions).pipe();
  }

  addNewProject(apiUrl,saveprojectObject: any): Observable<any> {
    let body = JSON.stringify(saveprojectObject);
    return this.http.post<any>(this.basiApiUrl+apiUrl, body, httpOptions).pipe();
  }

  getAllLineType(type="roofing") {
    let url = environment.apiEndPoint+ "Linetypes/"+ type;
    this.http.get(url, httpOptions).subscribe((data: any) => {
      console.log(data);    
    });
  }

  post(apiUrl, params: any): Observable<any> {
    let body = JSON.stringify(params);
    return this.http.post<any>(this.basiApiUrl + apiUrl, body, httpUserSettingOptions).pipe();
  }

  postUserSetting(apiUrl, params: any): Observable<any> {
    let body = JSON.stringify(params);
    console.log(body);
      return this.http.post<any>(this.basiApiUrl+apiUrl, body, httpOptions).pipe();
  }
  

  public getAll(url) {
    let api = this.apiEndpoint + url;
    return this.http.get(api, httpOptions);
  }

  loadProjects() {
    let url = environment.apiEndPoint+ "Projects/498039";
    this.http.get(url, httpOptions).subscribe((data: any) => {
      console.log("Project Data > ", data);
      this.router.navigate(["project"]);
    });
  }

  getLatestVersion(productType, orderID) {
    let url = environment.apiEndPoint+ "Projects/"+ orderID;
    this.http.get(url, httpOptions).subscribe((data: any) => {
      console.log("VersionUpdate > ", data);
    });
  }

  signInVersions(credUserPass): Observable<any> {
    let url = environment.apiEndPoint+ "Versions";
    let body = JSON.stringify(credUserPass);
    return this.http.post<any>(url, body, httpOptions).pipe();
  }

  getUserSettings() {
    
    this.getAll("usersettings").subscribe((data: any) => {

      let apiData  = JSON.parse(data);
    
      if(typeof apiData.GRID_SNAP_Length !="undefined") {
        this.MS.settings.GRID_SNAP_Length = apiData.GRID_SNAP_Length;
      }

      if(typeof apiData.RADIAL_DEGREES !="undefined") {
        this.MS.settings.RADIAL_DEGREES = apiData.RADIAL_DEGREES;
      }

      if(typeof apiData.BACKGROUND_COLOR !="undefined") {
        this.MS.settings.BACKGROUND_COLOR = apiData.BACKGROUND_COLOR;
        this.MS.color.EXTENT = apiData.BACKGROUND_COLOR;
      }
        
      if(typeof apiData.SELECTED_INDEX_BACKGROUND_COLOR  !="undefined" ) {
        this.MS.settings.SELECTED_INDEX_BACKGROUND_COLOR = apiData.SELECTED_INDEX_BACKGROUND_COLOR;
      }  
        
      if(typeof apiData.CROSSHAIR_SIZE !="undefined") {
        this.MS.settings.CROSSHAIR_SIZE = apiData.CROSSHAIR_SIZE;
      }  
        
      if(typeof apiData.CROSSHAIR_COLOR  !="undefined") {
        this.MS.settings.CROSSHAIR_COLOR = apiData.CROSSHAIR_COLOR;
      }

      if(typeof apiData.AUTO_SAVE  !="undefined" ) {
        this.MS.settings.AUTO_SAVE = apiData.AUTO_SAVE;
      }

      if(typeof apiData.DIMENSIONS_STANDARD  !="undefined" ) {
        this.MS.settings.DIMENSIONS_STANDARD = apiData.DIMENSIONS_STANDARD;
      }

      if(typeof apiData.DIMENSIONS_METRIC  !="undefined" ) {
        this.MS.settings.DIMENSIONS_METRIC = apiData.DIMENSIONS_METRIC;
      }

      if(typeof apiData.OBJECT_SNAPS  !="undefined" ) {
        this.MS.settings.OBJECT_SNAPS = apiData.OBJECT_SNAPS;
      }


console.log("this.MS.settings > ", this.MS.settings);

    });
  
  }

  saveUserSettings(saveSettingsObject) {
    this.postUserSetting("usersettings",  {user_settings: saveSettingsObject }).subscribe((data: any) => {
      console.log(data);
    });
  }

  

}
