import { EngineService } from '../engine/engine.service';
import { MessageService } from '../services/message.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { SubmitErrorComponent } from '../pages/project/dialog/submit-error/submit-error.component';
import { ReportPageErrorComponent } from '../pages/project/dialog/report-page-error/report-page-error.component';
export class Submit {
    
    private obj: any;
    bsModalRef: BsModalRef;
    subscription: Subscription;
    polygonExistMessage = new Array();
    polygonNagetiveMessage = new Array();

    constructor(private engServ: EngineService, private MS: MessageService, private modalService: BsModalService) {

        this.subscription = this.MS.getMessage().subscribe((message) => {
            if (message == "REPORT_SUBMIT") {

                this.submit();
            }
            
            if(message == "CHECK_PLANES") {
                this.checkNegativeAndZeroPlaneStructure();
            }
        });

    }

    submit() {

        this.obj = this.MS.save_project_json;

        this.MS.report.SUBMIT_ERROR = this.checkNegativeArea();

        if(this.MS.report.SUBMIT_ERROR != null) {
            this.bsModalRef = this.modalService.show( SubmitErrorComponent );
            return;
        }

        this.MS.report.SUBMIT_ERROR = this.checkZeroPlane();

        if(this.MS.report.SUBMIT_ERROR != null) {
            this.bsModalRef = this.modalService.show( SubmitErrorComponent );
            return;
        }
        
        if( this.MS.report.SUBMIT_ERROR == null ) {
            this.MS.sendMessage("GENERATE_REPORT_PDF");
        }  
    

    }

    reportCheckZeroPlane() {

        this.checkNegativeAndZeroPlaneStructure();

        this.obj = this.MS.save_project_json;

        this.MS.report.REPORT_CHECK_NEGTIVE = this.checkNegativeArea();

        if(this.obj != null) {
            this.MS.report.REPORT_CHECK_ZERO = this.checkZeroPlane();
        } 
        else {
            this.MS.report.REPORT_CHECK_ZERO = "Plane's value is Zero";
        }




        if(this.MS.report.REPORT_CHECK_ZERO != null || this.MS.report.REPORT_CHECK_NEGTIVE != null) {
            this.bsModalRef = this.modalService.show( ReportPageErrorComponent );
            return;
        }
    }

    checkNegativeAndZeroPlaneStructure():any {
        this.polygonExistMessage = [];
        this.polygonNagetiveMessage = [];
        let POLYGON_DATA = this.MS.area_editor.POLYGON_DATA;
        let structure = this.MS.menu_line_editor.STRUCTURE_COUNT;
        let buildings = this.MS.project_information.BUILDINGS;

        for (let i = 0; i < buildings.length; i++) {
            const isCheckPolygon:boolean = this.checkPolygonExist(i);
            const isNagativeAreaFound: boolean = this.checkStructureNegativeArea(i);

            if(isCheckPolygon == false) {
                this.polygonExistMessage.push("Polygon does not exist for " + buildings[i].Name);
                break;
            }
            else if(isNagativeAreaFound == true) {
                this.checkNagativeAreaOfStructure();
                this.polygonNagetiveMessage.push("Negative Area found in " + buildings[i].Name);
                break;
            }

        }

        if(this.polygonExistMessage.length > 0 || this.polygonNagetiveMessage.length > 0) {
            
            this.MS.report.POLYGON_NAGETIVE_MESSAGES = this.polygonNagetiveMessage;
            this.MS.report.POLYGON_EXTIST_MESSAGES = this.polygonExistMessage;;
            
            this.bsModalRef = this.modalService.show( ReportPageErrorComponent );
            return;
        }

    }

    checkPolygonExist(selectedBuildingGroup):any {
        let POLYGON_DATA = this.MS.area_editor.POLYGON_DATA;
        for (let i = 0; i < POLYGON_DATA.length; i++) {
            let matchSelectedGroup:number = POLYGON_DATA[i].selectedBuildingGroup;
            if(matchSelectedGroup == selectedBuildingGroup) {
                return true;
            }
        }
        return false;
    }

    checkStructureNegativeArea(selectedBuildingGroup):any {
        let POLYGON_DATA = this.MS.area_editor.POLYGON_DATA;
        for (let i = 0; i < POLYGON_DATA.length; i++) {
            let totalSlopeArea:number = POLYGON_DATA[i].totalSlopeArea;
            
            let matchSelectedGroup:number = POLYGON_DATA[i].selectedBuildingGroup;
            if( totalSlopeArea < 0 ) {
                if(matchSelectedGroup == selectedBuildingGroup) {
                    return true;
                }
            }
        }
        return false;
    }

    checkNagativeAreaOfStructure():any {
        let nagetiveAlpha  = [];
        let POLYGON_DATA = this.MS.area_editor.POLYGON_DATA;
        let buildings = this.MS.project_information.BUILDINGS;

        for (let i = 0; i < POLYGON_DATA.length; i++) {
            let totalSlopeArea:number = POLYGON_DATA[i].totalSlopeArea;
            let matchSelectedGroup:number = POLYGON_DATA[i].selectedBuildingGroup;
           
            if( totalSlopeArea < 0 ) {
                nagetiveAlpha.push(buildings[matchSelectedGroup].Name + " - "+ POLYGON_DATA[i].alphabet);
            }
        }

        this.MS.report.POLYGON_NAGETIVE_ALPHABETS = nagetiveAlpha;
    }

    checkNegativeArea():any {
        let POLYGON_DATA = this.MS.area_editor.POLYGON_DATA;

        for (let i = 0; i < POLYGON_DATA.length; i++) {
            let totalSlopeArea:number = POLYGON_DATA[i].totalSlopeArea;
            if( totalSlopeArea < 0 ) {
                return "Negative Area";
            }
        }
        return null;
    }

    checkZeroPlane():any {

        let buildings: any = this.obj.Buildings;

        //Buildings loop
        for (let i = 0; i < buildings.length; i++) {

            let building:any = buildings[i];
            if( building.Areas.length == 0 ) {
                return "Plane's value is Zero";
            }

        }  

        return null;

    }
}
