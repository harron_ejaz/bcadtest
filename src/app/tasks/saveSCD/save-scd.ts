import { Subscription } from "rxjs";
import { EngineService } from "src/app/engine/engine.service";
import { MessageService } from "src/app/services/message.service";
import { ProjectData } from "./project-data";
import { ProjectEngine } from "./project-engine";

export class SaveSCD {

  subscription: Subscription;

  private obj: any;
  private projectData: ProjectData;
  private projectEngine: ProjectEngine;

  constructor(private engServ: EngineService, private MS: MessageService) {

    this.projectData = new ProjectData(this.engServ, this.MS);
    this.projectEngine = new ProjectEngine(this.engServ, this.MS);

    this.subscription = this.MS.getMessage().subscribe((message) => {
      if (message == "SAVE_SCD") {
        this.saveProject();
      }
    });
  }

  //save .scd file
  public saveProject() {
    this.obj = new Object();

    this.generateJSON();
    this.MS.save_project_json = this.obj;
    this.MS.sendMessage("SAVE_PROJECT_JSON");


    console.log("Save .scd");
  }

  //create project JSON object to save
  private generateJSON() {
    this.projectData.updateJSON(this.obj);
    this.projectEngine.updateJSON(this.obj);
  }

}
