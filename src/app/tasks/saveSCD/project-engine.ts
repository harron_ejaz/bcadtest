import { EngineService } from "src/app/engine/engine.service";
import { MessageService } from "src/app/services/message.service";
import { Vector3 } from 'three';

export class ProjectEngine {

  constructor(private engServ: EngineService, private MS: MessageService) { }

  updateJSON(obj: any) {

    this.saveSiteAlignmentLine( obj );
        
    this.saveSiteMeasurementLine( obj );


    obj.Buildings = new Array();

    let buildings: any = this.MS.project_information.BUILDINGS;

    //Buildings loop
    for (let i = 0; i < buildings.length; i++) {

      this.createGeometries(buildings[i]);

      this.createAreas(buildings[i]);

      this.createReports(buildings[i]);

      obj.Buildings.push(buildings[i]);

    }

  }

  saveSiteAlignmentLine( obj:any ) {
    let line:any = this.engServ.commonGroup.getObjectByName('Align');
    if( line ) {
      const lineArray = line.geometry.attributes.position.array;
      const lineX1:number = lineArray[0];
      const lineY1:number = lineArray[1];
      const lineX2:number = lineArray[3];
      const lineY2:number = lineArray[4];
      let start:string = lineX1 + "," + lineY1 * -1;
      let end:string = lineX2 + "," + lineY2 * -1;
      obj.SiteAlignmentLineStart = start;
      obj.SiteAlignmentLineEnd = end;
    }
    else {
      obj.SiteAlignmentLineStart = "0,0";
      obj.SiteAlignmentLineEnd = "0,0";
    }
  }

  saveSiteMeasurementLine( obj:any ) {
    obj.SiteMeasurementLength = ( this.MS.input_scaleLength.inputLength * 12 ).toString();

    let line:any = this.engServ.commonGroup.getObjectByName('Scale');
    if( line ) {
      const lineArray = line.geometry.attributes.position.array;
      const lineX1:number = lineArray[0];
      const lineY1:number = lineArray[1];
      const lineX2:number = lineArray[3];
      const lineY2:number = lineArray[4];
      let start:string = lineX1 + "," + lineY1 * -1;
      let end:string = lineX2 + "," + lineY2 * -1;
      obj.SiteMeasurementLineStart = start;
      obj.SiteMeasurementLineEnd = end;
    }
    else {
      obj.SiteMeasurementLineStart = "0,0";
      obj.SiteMeasurementLineEnd = "0,0";
    }
  }

  createGeometries(buildings: any) {

    buildings.Geometries = new Array();

    let objects: any = this.engServ.getSelectedGeometriesGroup().children;

    //Geometries loop
    for (let i = 0; i < objects.length; i++) {
      let object = objects[i];
      let geometries: any = new Object();

      geometries.Type = object.name;
      geometries.LineTypesRef = object.userData.LineTypesRef;
      geometries.Slope = object.userData.Rise;
      geometries.OtherData = null;
      geometries.ShowDimension = true;
      geometries.DimensionOffset = "0,0";
      geometries.TextSize = 3.6845463685755;
      geometries.Factor = 1;
      geometries.OverrideTextSize = false;
      geometries.EndInnerOuterSymbol = 12.053639105726;
      geometries.Id = object.geometry.uuid;

      if (object.name == "Line") {
        this.createLineGeometries(geometries, object);
      } else if (object[i].name == "Circle") {
        this.createCircleGeometries(geometries, object);
      }

      buildings.Geometries.push(geometries);
    }

  }

  createLineGeometries(geometries: any, object: any) {
    const lineArray = object.geometry.attributes.position.array;
    const lineX1:number = lineArray[0];
    const lineY1:number = lineArray[1];
    const lineX2:number = lineArray[3];
    const lineY2:number = lineArray[4];
    let start:string = lineX1 + "," + lineY1 * -1;
    let end:string = lineX2 + "," + lineY2 * -1

    geometries.Start = start;
    geometries.End = end;
  }

  createCircleGeometries(geometries: any, object: any) {
    geometries.Start = object.position.x + "," + object.position.y;
    geometries.End = "0,0";
  }

  createAreas(buildings: any) {
    buildings.Areas = new Array();
    let POLYGON_DATA = this.MS.area_editor.POLYGON_DATA;

    for (let i = 0; i < POLYGON_DATA.length; i++) {

      if( buildings.Id == POLYGON_DATA[i].buildingsGroupUUID ) {

        let area: any = new Object();

        area.Name = POLYGON_DATA[i].alphabet;
        area.Slope = POLYGON_DATA[i].rise;
        area.TotalArea = POLYGON_DATA[i].totalSlopeArea;
        area.High = POLYGON_DATA[i].isOpen;
        area.IceAndWaterThickness = 0;
        area.SplitAreas = null;
        area.Boundaries = new Array();

        buildings.Areas.push(area);

      }
      
    }
    
  }

  createReports(buildings: any) {
    buildings.Reports = new Array();
    let position = this.MS.area_editor.SCREENSHOT_POSITION;
    if (position != null) {
      
      let POLYGON_DATA = this.MS.area_editor.POLYGON_DATA;
      for ( let i = 0; i < POLYGON_DATA.length; i++ ) {
        if( buildings.Id == POLYGON_DATA[i].buildingsGroupUUID ) {
            let report: any = new Object();
            report.Name = "ScreenShot";
            report.Position = position.topLeft.x + ',' + position.topLeft.y + ',' + position.bottomRight.x + ',' + position.bottomRight.y;
            report.Id = null;
            buildings.Reports.push(report);
        }
      }
    }
  }

}
