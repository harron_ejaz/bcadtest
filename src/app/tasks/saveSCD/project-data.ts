import { EngineService } from "src/app/engine/engine.service";
import { MessageService } from "src/app/services/message.service";

export class ProjectData {

  constructor(private engServ: EngineService, private MS: MessageService) {}

  updateJSON(obj: any) {

    obj.Name = this.MS.project_information.PROJECT_NAME;
    obj.Address = this.MS.project_information.PROJECT_ADDRESS;
    obj.SiteAddress = this.MS.project_information.PROJECT_ADDRESS;
    obj.Description = this.MS.project_information.CUSTOMER_NOTES;
    obj.Notes = this.MS.project_information.QC_NOTES;

    if ( this.MS.saveAndSubmit.isSaveAndSubmit == true ) {
      obj.Status = "INSPECT";
    } else {
      obj.Status = "3";
    }
    
    obj.UsersRef = this.MS.currentSCDProjectObject.SCD_DATA.UsersRef;
    obj.IsCurrent = true;
    obj.MasterRevisionRef = this.MS.currentSCDProjectObject.SCD_DATA.MasterRevisionRef;
    obj.ProductType = this.MS.currentSCDProjectObject.SCD_DATA.ProductType;
    obj.GoogleImage = this.MS.project_information.SITE_IMAGE;
    obj.GoogleMap = this.MS.currentSCDProjectObject.SCD_DATA.GoogleMap;
    obj.DateOfLoss = this.MS.currentSCDProjectObject.SCD_DATA.DateOfLoss;
    obj.ClaimNumber = this.MS.currentSCDProjectObject.SCD_DATA.MasterRevisionRef;
    obj.JobNumber = this.MS.currentSCDProjectObject.SCD_DATA.MasterRevisionRef;
    obj.Pitch = this.MS.currentSCDProjectObject.SCD_DATA.MasterRevisionRef;
    obj.AlternativePitch = this.MS.currentSCDProjectObject.SCD_DATA.AlternativePitch;
    obj.CreatedOn = this.MS.currentSCDProjectObject.SCD_DATA.CreatedOn;
    obj.UpdatedOn = this.MS.currentSCDProjectObject.SCD_DATA.UpdatedOn;
    obj.DueDate = this.MS.currentSCDProjectObject.SCD_DATA.DueDate;
    obj.RecentRemodels = this.MS.currentSCDProjectObject.SCD_DATA.RecentRemodels;
    obj.ShippingMethod = this.MS.currentSCDProjectObject.SCD_DATA.ShippingMethod;
    obj.A50Squares = this.MS.currentSCDProjectObject.SCD_DATA.A50Squares;
    obj.PropertyType = this.MS.currentSCDProjectObject.SCD_DATA.Id;
    obj.ScopeErrorNotes = this.MS.currentSCDProjectObject.SCD_DATA.ScopeErrorNotes;
    obj.ScopeErrorCount = this.MS.currentSCDProjectObject.SCD_DATA.ScopeErrorCount;
    obj.ScopeErrorType = this.MS.currentSCDProjectObject.SCD_DATA.ScopeErrorType;
    obj.QCTimeStamp = this.MS.currentSCDProjectObject.SCD_DATA.QCTimeStamp;
    obj.QualityTechnician = this.MS.currentSCDProjectObject.SCD_DATA.QualityTechnician;
    obj.PropertySquares = this.MS.currentSCDProjectObject.SCD_DATA.PropertySquares;
    obj.PropertyPlanes = this.MS.currentSCDProjectObject.SCD_DATA.PropertyPlanes;
    obj.Initials = this.MS.currentSCDProjectObject.SCD_DATA.Initials;
    obj.TrackViaId = this.MS.currentSCDProjectObject.SCD_DATA.TrackViaId;
    obj.CopyrightText = this.MS.currentSCDProjectObject.SCD_DATA.CopyrightText;
    obj.IsMultiStructure = this.MS.currentSCDProjectObject.SCD_DATA.IsMultiStructure;
    obj.IsSingleOverheadImage = this.MS.currentSCDProjectObject.SCD_DATA.IsSingleOverheadImage;
    obj.SurfaceTypes = this.MS.currentSCDProjectObject.SCD_DATA.SurfaceTypes;
    obj.Properties = new Array();
    
    obj.Id = this.MS.currentSCDProjectObject.SCD_DATA.Id;
    obj.LastUpdateBy = this.MS.signIn_user.userName;

    if( this.MS.imagery.TOP_IMAGE_URL == "assets/images/browseImage.jpg" ) 
      obj.SiteImage = null;
    else
      obj.SiteImage = this.MS.imagery.TOP_IMAGE_URL.split(",")[1]; 

    if( this.MS.imagery.NORTH_IMAGE_URL == "assets/images/browseImage.jpg" ) 
      obj.NorthImage = null;
    else
      obj.NorthImage = this.MS.imagery.NORTH_IMAGE_URL.split(",")[1]; 

    if( this.MS.imagery.SOUTH_IMAGE_URL == "assets/images/browseImage.jpg" ) 
      obj.SouthImage = null;
    else
      obj.SouthImage = this.MS.imagery.SOUTH_IMAGE_URL.split(",")[1];  

    if( this.MS.imagery.EAST_IMAGE_URL == "assets/images/browseImage.jpg" ) 
      obj.EastImage = null;
    else
      obj.EastImage = this.MS.imagery.EAST_IMAGE_URL.split(",")[1]; 
      
    if( this.MS.imagery.WEST_IMAGE_URL == "assets/images/browseImage.jpg" ) 
      obj.WestImage = null;
    else
      obj.WestImage = this.MS.imagery.WEST_IMAGE_URL.split(",")[1];   
    
  }

}
