import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from 'src/app/services/message.service';

export class ProjectData {

    constructor(private engServ: EngineService, private MS: MessageService) {}

    loadJSON(obj: any) {  
        
        this.loadProjectInformation(obj);

        this.loadImagery(obj);  

        this.loadBuildings(obj)
        
        this.MS.sendMessage("START_TIMER");

    }

    //load building structures list and combos
    loadBuildings(obj: any) {
        let buildings: any = obj.Buildings;
        //scd data is going to load, so delete 1 bulding that was loaded automatically
        this.MS.sendMessage("LOAD_SCD_DELETE_LAST_BUILDING");

        //Buildings loop
        for (let i = 0; i < buildings.length; i++) {
            let building:any = buildings[i];

            this.MS.project_information.BUILDINGS.push( building );
        }  

        this.engServ.addBuildingsGroup( "load_scd" );
        this.engServ.showSelectedBuildingsGroup( this.MS.menu_line_editor.BUILDING_GROUP_SELECTED );
        
    }

    //load project information page
    loadProjectInformation(obj: any) {
        this.MS.project_information.PROJECT_NAME = obj.Name;
        this.MS.project_information.PROJECT_ADDRESS = obj.Address;
        this.MS.project_information.CUSTOMER_NOTES = obj.Description;
        this.MS.project_information.QC_NOTES = obj.Notes;
        this.MS.project_information.SHIPPING = obj.ShippingMethod;
        this.MS.project_information.SCOPE_TYPE = obj.scope_type_id;
        this.MS.project_information.PRODUCT_TYPE = obj.product_id;
        this.MS.project_information.ORDER_ID = obj.Id;
        this.MS.project_information.MAP_LINK = obj.GoogleImage;
        this.MS.project_information.DUE_DATE = obj.DueDate;
        this.MS.project_information.SITE_IMAGE = obj.GoogleImage;
    }

    //load imagery page (image acquisiiton)
    loadImagery(obj: any) {

        this.MS.menu_select_image.IS_SINGLE_OVERHEAD_IMAGE = obj.IsSingleOverheadImage;
        this.MS.menu_select_image.COPYRIGHT_TEXT = obj.CopyrightText;

   

        if( obj.SiteImage == null ) 
            this.MS.imagery.TOP_IMAGE_URL = "assets/images/browseImage.jpg";
        else
            this.MS.imagery.TOP_IMAGE_URL = "data:image/png;base64," + obj.SiteImage;

        if( obj.NorthImage == null ) 
            this.MS.imagery.NORTH_IMAGE_URL = "assets/images/browseImage.jpg";
        else
            this.MS.imagery.NORTH_IMAGE_URL = "data:image/png;base64," + obj.NorthImage;   
            
        if( obj.SouthImage == null ) 
            this.MS.imagery.SOUTH_IMAGE_URL = "assets/images/browseImage.jpg";
        else
            this.MS.imagery.SOUTH_IMAGE_URL = "data:image/png;base64," + obj.SouthImage;     
            
        if( obj.EastImage == null ) 
            this.MS.imagery.EAST_IMAGE_URL = "assets/images/browseImage.jpg";
        else
            this.MS.imagery.EAST_IMAGE_URL = "data:image/png;base64," + obj.EastImage;  

        if( obj.WestImage == null ) 
            this.MS.imagery.WEST_IMAGE_URL = "assets/images/browseImage.jpg";
        else
            this.MS.imagery.WEST_IMAGE_URL = "data:image/png;base64," + obj.WestImage; 

    }

}
