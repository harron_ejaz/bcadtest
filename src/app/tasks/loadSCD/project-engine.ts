import { EngineService } from 'src/app/engine/engine.service';
import { MessageService } from 'src/app/services/message.service';
import { BufferGeometry, Vector3, Line, LineDashedMaterial, LineBasicMaterial } from 'three';
import { Measure } from 'src/app/engine/tasks/measure';
import { Align } from 'src/app/engine/tasks/align';

export class ProjectEngine {

    constructor(private engServ: EngineService, private MS: MessageService, private measure:Measure, private align:Align) {}

    loadJSON(obj: any) {   

        this.loadSiteAlignmentLine( obj.SiteAlignmentLineStart, obj.SiteAlignmentLineEnd );
        
        this.loadSiteMeasurementLine( obj.SiteMeasurementLineStart, obj.SiteMeasurementLineEnd, obj.SiteMeasurementLength );
        
        let buildings: any = obj.Buildings;

        //Buildings loop
        for (let i = 0; i < buildings.length; i++) {

            let building:any = buildings[i];

            this.loadGeometries(building, i);

            this.loadAreas(building);

            this.loadReports(building);

        }  

        this.MS.sendMessage('REMOVE_ALL_DIMENSIONS');

    }

    loadSiteAlignmentLine( SiteAlignmentLineStart:any, SiteAlignmentLineEnd:any ) {
        
        let x1:number = parseFloat( SiteAlignmentLineStart.split(',')[0] );
        let y1:number = parseFloat( SiteAlignmentLineStart.split(',')[1] );

        let x2:number = parseFloat( SiteAlignmentLineEnd.split(',')[0] );
        let y2:number = parseFloat( SiteAlignmentLineEnd.split(',')[1] );

        let relative1 = this.engServ.get2DPosition( x1, y1 );
        let relative2 = this.engServ.get2DPosition( x2, y2 );

        this.align.loadAlignmentLineFromSCD( relative1 );
        this.align.loadAlignmentLineFromSCD( relative2 );
        
    }

    loadSiteMeasurementLine( SiteMeasurementLineStart:any, SiteMeasurementLineEnd:any, SiteMeasurementLength:number ) {
        
        this.MS.input_scaleLength.inputLength = parseFloat( ( SiteMeasurementLength / 12 ).toFixed(2) );

        let x1:number = parseFloat( SiteMeasurementLineStart.split(',')[0] );
        let y1:number = parseFloat( SiteMeasurementLineStart.split(',')[1] );

        let x2:number = parseFloat( SiteMeasurementLineEnd.split(',')[0] );
        let y2:number = parseFloat( SiteMeasurementLineEnd.split(',')[1] );

        let relative1 = this.engServ.get2DPosition( x1, y1 );
        let relative2 = this.engServ.get2DPosition( x2, y2 );

        this.measure.loadMeasurementLineFromSCD( relative1 );
        this.measure.loadMeasurementLineFromSCD( relative2 );
        
    }

    loadGeometries(building: any, buildingIndex:number) {

        let geometries:any = building.Geometries;

        //Geometries loop
        for (let i = 0; i < geometries.length; i++) {

            if(geometries[i].Type == "Line") {
                this.loadLineGeometries( geometries[i], buildingIndex );
            }
            else if(geometries[i].Type == "Circle") {
                this.loadCircleGeometries( geometries[i] );
            }
            
        }


    }

    loadLineGeometries( geometries: any, buildingIndex:number ) {

        let x1:number = parseFloat( geometries.Start.split(',')[0] );
        let y1:number = parseFloat( geometries.Start.split(',')[1] );

        let x2:number = parseFloat( geometries.End.split(',')[0] );
        let y2:number = parseFloat( geometries.End.split(',')[1] );

        let relative1 = this.engServ.get2DPosition( x1, y1 );
        let relative2 = this.engServ.get2DPosition( x2, y2 );

        if( relative1 == null || relative2 == null) {
            console.log("Line geometries points mismatch")
            return;
        }

        const points = [];
        points.push( new Vector3( relative1.x, relative1.y, 0 ) );
        points.push( new Vector3( relative2.x, relative2.y, 0 ) );
        const geometry = new BufferGeometry().setFromPoints( points );

        let line:Line = this.set_line_shape(geometry, geometries.LineTypesRef, geometries.Slope);
        line.name = 'Line';
        line.renderOrder = 1;
        line.uuid = geometries.Id;
        //when intersection is on, the lines which have been drawn already should get intersects
        line.userData.Drawn = true;  
        line.userData.Selected = false;
        //bounding sphere is set so that line can be selected using mouse
        line.geometry.computeBoundingSphere();
        //add to respective building group in scene
        let buildingsGroup:any = this.engServ.rootBuildingsGroup.children[  buildingIndex ];
        buildingsGroup.getObjectByName( "GeometriesGroup" ).add( line );
    }

    loadCircleGeometries( geometries: any ) {

    }

    loadAreas(building: any) {
        
    }
    
    loadReports(building: any) {
        
    }

    get_line_type_selected( LineTypesRef:number ):any {
        let LINE_TYPES = this.MS.LINE_TYPES;
        let lineType:any = LINE_TYPES.find( x => x.LINE_TYPES_REF == LineTypesRef );
        return lineType;
      }

    private get_line_material( LineTypesRef:number ):LineDashedMaterial {
        let lineType:any = this.get_line_type_selected( LineTypesRef );
        // material
        const material:LineDashedMaterial = new LineDashedMaterial(
            { color: lineType.COLOR, dashSize: 1, gapSize: lineType.SOLID, depthTest: false }
        );
        return material;
      }

    private set_line_shape(geometry:BufferGeometry, LineTypesRef:number, Rise:number) {
        let material:LineDashedMaterial = this.get_line_material( LineTypesRef );
        // line
        let line:Line = new Line(geometry, material);
        line.userData.LineTypesRef = LineTypesRef;
        line.userData.Rise = Rise;
        return line;
    }

    lineShape( geometry:BufferGeometry ):Line {
        // material
        let material = new LineBasicMaterial({
            color: this.MS.color.HIGHLIGHT_LINE
        }); 
        // line
        const line = new Line(geometry, material);
        return line;
    }

}
