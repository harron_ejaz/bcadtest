import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OpenProjectComponent } from 'src/app/pages/open-project/open-project.component';


const routes: Routes = [
  { 
    path: 'signin',
    loadChildren: () => import('./pages/signin/signin.module').then(m => m.SigninModule) 
  },
  { 
    path: 'open-project', 
    loadChildren: () => import('./pages/open-project/open-project.module').then(m => m.OpenProjectModule) 
  },
  { 
    path: 'project',
    loadChildren: () => import('./pages/project/project.module').then(m => m.ProjectModule)  
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }