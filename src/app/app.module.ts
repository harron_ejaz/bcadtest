import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { PagesModule } from "./pages/pages.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { HttpClientModule } from "@angular/common/http";

import {
  RequestCache,
  RequestCacheWithMap,
} from "src/app/api/request-cache.service";

import { ConfigComponent } from "src/app/api/config/config.component";
import { HttpErrorHandler } from "src/app/api/http-error-handler.service";
import { MessageService } from "src/app/services/message.service";
import { MessagesComponent } from "src/app/api/messages/messages.component";
import { PackageSearchComponent } from "src/app/api/package-search/package-search.component";

import { httpInterceptorProviders } from "src/app/api/http-interceptors/index";

import { ScdFileService } from "./services/scdfile.service";
import { AuthService } from "src/app/api/auth.service";
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { ToastrModule } from 'ngx-toastr';
import { TooltipModule  } from 'ngx-bootstrap/tooltip';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [
    AppComponent,
    ConfigComponent,
    MessagesComponent,
    PackageSearchComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    LoadingBarModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    TooltipModule.forRoot(),
    NgxMaskModule.forRoot(),
    NgxSpinnerModule
  ],
  providers: [
    AuthService,
    HttpErrorHandler,
    MessageService,
    { provide: RequestCache, useClass: RequestCacheWithMap },
    httpInterceptorProviders,
    ScdFileService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
